clear all

%3D PSF
mu = [0 0 0];

sigxy = 0.04145; % dimensions are in mm
sigz = 0.046;

Sigma = [sigxy^2 0 0; 0 sigxy^2 0; 0 0 sigz^2]; %covariance matrix

%crop PSF grid when e.g. 1e-15 ~ 0
zero_crop = 1e-10;

range_xy = norminv(zero_crop,0,sigxy); %max, min range of PSF grid in mm
range_xy = norminv(zero_crop,0,sigxy);
range_z = norminv(zero_crop,0,sigz);

%PSF grid spacing
spacingx = 0.025;
spacingy = 0.025;
spacingz = 0.025;

x1 = range_xy:spacingx:-range_xy;
x2 = range_xy:spacingy:-range_xy;
x3 = range_z:spacingz:-range_z;
[X1,X2,X3] = meshgrid(x1,x2,x3);

dimscale = (spacingx*spacingy*spacingz); %mm^3

%the elusive 3-D PSF, in all its glory
F = mvnpdf([X1(:) X2(:) X3(:)], mu, Sigma);
PSF = reshape(F,length(x1),length(x2),length(x3)).*dimscale; %The PSF gereated in mm domain must be rescaled to be normalized in pixel domain