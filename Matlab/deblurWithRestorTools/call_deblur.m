%This is called by an Amira Matlab module to perform deblurring

%startup

%DC = background;

%OriginalCT = double(field.data);
%OriginalCT = OriginalCT - DC; %add DC value to shift up air single to be the zero signal

%PSF = double(field1.data);

%note that one needs to define the following before running the code
%PSF
%OriginalCT
%num_its

H = psfMatrix(double(PSF),'reflexive');

options = IRset('MaxIter',num_its);
deblurredScanMRNSD = IRmrnsd(H, OriginalCT, options);


writeDicomStack('C:\Mike\SkullCapData\ID5685\MRNSDdeblurred\','im_mat','.dcm',deblurredScanMRNSD,alternateOriginalCTInfo);
%%
%deblurred = x + DC;

%result = field;
%result.name = [field.name '_deblurred'];
%result.data = deblurred;
%%


