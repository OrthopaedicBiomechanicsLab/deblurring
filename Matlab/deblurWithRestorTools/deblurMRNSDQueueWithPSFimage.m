index=1;
%imagePath{index} = 'C:\Mike\CTAngioScans\StrokeDetection\Data\rewrittenByAmira\';
imagePath{index} = 'C:\Mike\CTAngioAmir\ResampledRemovedBackground\'
sigma{index} = [0.52,0.52,0.62];
spacing{index} = [0.400391, 0.400391, 0.625];
decimalPlaces{index} = 6;

%note that by providing PSF it will overwrite
PSFpath{index} = 'C:\Mike\CTAngioAmir\PSF\'


index=1+index;
%imagePath{index} = 'C:\Mike\CTAngioScans\StrokeDetection\Data\rewrittenByAmira\';
imagePath{index} = 'C:\Mike\CTAngioAmir\resampled\'
sigma{index} = [0.52,0.52,0.62];
spacing{index} = [0.400391, 0.400391, 0.625];
decimalPlaces{index} = 6;

%note that by providing PSF it will overwrite
PSFpath{index} = 'C:\Mike\CTAngioAmir\PSF\'




num_its = 100;

i=1;

for i = 1:2
    newFilePath = [imagePath{i} 'MRNSDdeblurred\deblurredData.m'];

    [scan, scanInfo] = readDicomStack([imagePath{i} '*.dcm']);
    psfStack = readImageStack([PSFpath{i} '*.tif']);



    tic
    deblurredScan = deblurMRNSD(scan,sigma{i},spacing{i},decimalPlaces{i},num_its, psfStack);
    toc

    save(newFilePath);

    writeDicomStack([imagePath{i} 'MRNSDdeblurred\'],'im_mat','.dcm',deblurredScan,scanInfo);
    
end

%LetsCheck how well deblurring does with aniso PSF

index=1+index;
%imagePath{index} = 'C:\Mike\CTAngioScans\StrokeDetection\Data\rewrittenByAmira\';
imagePath{index} = 'C:\Mike\CTAngioAmir\backgroundFilledDicom\';
sigma{index} = [0.52,0.52,0.62];
spacing{index} = [0.400391, 0.400391, 0.625];
decimalPlaces{index} = 6;

%note that by providing PSF it will overwrite
PSFpath{index} = 'C:\Mike\CTAngioAmir\anisoPSF\anisoPSF.nii';

for i = index
    newFilePath = [imagePath{i} 'MRNSDdeblurred\deblurredData.m'];

    [scan, scanInfo] = readDicomStack([imagePath{i} '*.dcm']);
    psfStackniiStructure = load_nii(PSFpath{i});
    psfStack = psfStackniiStructure.img;


    tic
    deblurredScan = deblurMRNSD(scan,sigma{i},spacing{i},decimalPlaces{i},num_its, psfStack);
    toc

    save(newFilePath);

    writeDicomStack([imagePath{i} 'MRNSDdeblurred\'],'im_mat','.dcm',deblurredScan,scanInfo);
    
end