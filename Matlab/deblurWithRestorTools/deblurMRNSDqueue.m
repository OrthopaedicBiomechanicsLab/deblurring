%queing up the skull caps for deblurring and writing to file
% % num_its = 100;
% % 
% % PSF = vtk_read_volume('C:\Mike\SkullCapData\ID5685\Deblurred\GaussianImageSource Output_6x6x6.vtk');
% % scaledPSF=PSF/sum(sum(sum(PSF)));
% % [OriginalCT, OriginalCTInfo] = readDicomStack('C:\Mike\SkullCapData\ID5685\im*');
% % minOriginalCT = min(min(min(OriginalCT)));
% % OriginalCT=double(OriginalCT-minOriginalCT);
% % 
% % tic
% % call_deblur1;
% % toc
% % deblurredScanMRNSDScaledPSF=int16(deblurredScanMRNSDScaledPSF)+minOriginalCT;
% % save('C:\Mike\SkullCapData\ID5685\MRNSDdeblurred\deblurredDataID5685.m');
% % writeDicomStack('C:\Mike\SkullCapData\ID5685\MRNSDdeblurred\','im_mat','.dcm',deblurredScanMRNSDScaledPSF,OriginalCTInfo);


imagePath = {};
sigma = {};
spacing = {};
decimalPlaces={};
upSampledNiftiFile ={};

num_its = 100;

index = 1;
% imagePath{1} = 'Z:\Calavera\SkullCapData\CT_Skull_Full_3\';
% sigma{1} = [0.55,0.55,0.54];
% spacing{1} = [0.625, 0.625, 0.625];
% decimalPlaces{1} = 6;
% 
% index = 1+index;
% rootdir = '\\Mike-PC\Mike\CTAngioScans'
% imagePath{index} = [rootdir '\1.3.6.1.4.1.25403.9210260030413.5236.20150619021943.1\CTA_Only\'];
% sigma{index} = [0.54,0.54,0.61];
% spacing{index} = [0.555, 0.555, 0.625];
% decimalPlaces{index} = 6;
% 
% index = 1+index;
% imagePath{index} = [rootdir '\1.3.6.1.4.1.25403.9210260030413.5236.20150619022249.1\CTA_Only\'];
% sigma{index} = [0.53,0.53,0.55];
% spacing{index} = [0.488, 0.488, 0.625];
% decimalPlaces{index} = 6;
% 
% 
% index = 1+index;
imagePath{index} = 'Z:\Calavera\SkullCapData\CT_Skull_Full_1\';
sigma{index} = [0.44,0.44,0.57];
spacing{index} = [0.625, 0.625, 0.625];
decimalPlaces{index} = 6;


index = 1+index;
imagePath{index} = 'Z:\Calavera\SkullCapData\CT_Skull_Full_2\';
sigma{index} = [0.63,0.63,0.54];
spacing{index} = [0.625, 0.625, 0.625];
decimalPlaces{index} = 6;


index = 1+index;
imagePath{index} = 'Z:\Calavera\SkullCapData\CT_Skull_Full_5\';
sigma{index} = [0.40,0.40,0.53];
spacing{index} = [0.625, 0.625, 0.625];
decimalPlaces{index} = 6;
% index = 1+index;
% imagePath{index} = 'Z:\Calavera\SkullCapData\ID5616\';
% sigma{index} = [0.39,0.39,0.49];
% spacing{index} = [0.625, 0.625, 0.625];
% decimalPlaces{index} = 6;
% 
% index = 1+index;
% imagePath{index} = 'Z:\Calavera\SkullCapData\ID5631\';
% sigma{index} = [0.41,0.41,0.56];
% spacing{index} = [0.625, 0.625, 0.625];
% decimalPlaces{index} = 6;
% 
% index = 1+index;
% imagePath{index} = 'Z:\Calavera\SkullCapData\ID5681\';
% sigma{index} = [0.43,0.43,0.55];
% spacing{index} = [0.625, 0.625, 0.625];
% decimalPlaces{index} = 6;
% 
% index = 1+index;
% imagePath{index} = 'Z:\Calavera\SkullCapData\ID5727\';
% sigma{index} = [0.39,0.39,0.52];
% spacing{index} = [0.625, 0.625, 0.625];
% decimalPlaces{index} = 6;
% 
% index = 1+index;
% imagePath{index} = 'Z:\Calavera\SkullCapData\ID5738\';
% sigma{index} = [0.33,0.33,0.47];
% spacing{index} = [0.625, 0.625, 0.625];
% decimalPlaces{index} = 6;







for i = 1:index
    %regular resolution
    newFilePath = [imagePath{i} 'MRNSDdeblurred2\deblurredData.m'];
    if exist(newFilePath,'file') ~= 2
        PSF = gaussianImageSource(sigma{i}, spacing{i}, decimalPlaces{i});
        scaledPSF=PSF/sum(sum(sum(PSF)));

        niftyFlag = false; %imagePath{i}(end-2:end)=='nii';

        if niftyFlag
            niiStructure= load_nii(imagePath{i});
            OriginalCT =niiStructure.img;
            %niiStructure.img = null;
        else    
            [OriginalCT, OriginalCTInfo] = readDicomStack([imagePath{i} 'dicom\im*']);
        end


        %OriginalCT = padarray(OriginalCT,[20 20 20],-3024);

        minOriginalCT = min(min(min(OriginalCT)));

        OriginalCT=double(OriginalCT-minOriginalCT);


        [pathstr,name,ext] = fileparts(newFilePath);
        if exist(pathstr,'dir') ~= 7
            mkdir(pathstr);
        end
    
    
    
    
        tic
        call_deblur1;
        toc

        save(newFilePath);
        
        deblurredScanMRNSDScaledPSF=int16(deblurredScanMRNSDScaledPSF)+minOriginalCT;
        %depadding
        %deblurredScanMRNSDScaledPSF = deblurredScanMRNSDScaledPSF(21:end-20,21:end-20,21:end-20);

        

        
        if niftyFlag
            niiStructure.img = deblurredScanMRNSDScaledPSF;
            save_nii(niiStructure,[imagePath{i} '.deblurredMRNSD.nii']);  
        else    
            writeDicomStack([imagePath{i} 'MRNSDdeblurred2\'],'im_mat','.dcm',deblurredScanMRNSDScaledPSF,OriginalCTInfo);
        end    
    
    end
    
end