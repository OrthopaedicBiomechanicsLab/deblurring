%Michael Hardisty 3D Gaussian Image Source
function [gaussianPSF] = gaussianImageSource(sigma, spacing, decimalPlaces)
% Generate a Gaussian PSF suitable for deconvolution use
% Inputs:
%  sigma   : 3 x 1 sigma value that defines the gaussina psf 
%  spacing : 3 x 1, voxelSize of the PSF 
%  decimalPlaces : how accurate the PSF needs to be in terms of how close
%  to zero the gaussian components must get at the edges of the image.
%  Gauss->0 as x->infinity so more pixels means Gauss gets closer to zero.

dimensions = size(sigma,2);
sigmaMultSum = 1;
for dimIndex = 1 : dimensions
    sigmaMultSum = sigmaMultSum * sigma(dimIndex);
end
psfEdgePhysicalUnits = ones(dimensions,1);
planeSize= ones(dimensions,1);
meanLocation= ones(dimensions,1);

for dimIndex = 1 : dimensions
    psfEdgePhysicalUnits(dimIndex) = (log(2*pi()*10^(-decimalPlaces)*sigmaMultSum)*sigma(dimIndex)*sigma(dimIndex)*(-2))^(0.5);
    planeSize(dimIndex) =2*(cast(psfEdgePhysicalUnits(dimIndex)/spacing(dimIndex),'int16')+1);
    meanLocation(dimIndex)=spacing(dimIndex)*planeSize(dimIndex)/2+0.625/2;
end
%psfEdgePhysicalUnits = (LOG(2*PI()*10^(-decimalPlaces)*B$4*B$5*B$6)*B4*B4*-2)^(0.5);

%scale0 =1/((2*pi())^(3/2)*sigmaMultSum);

%should have implimented this as n-dimensional recursion.  I can't be
%bothered right now
gaussianPSF = ones(transpose(planeSize));
gaussianPSFSum = 0;
for xIndex = 1 : planeSize(1)
    for yIndex = 1 : planeSize(2)
        for zIndex = 1 : planeSize(3)
            gaussianPSF(xIndex,yIndex,zIndex) = exp(-((xIndex*spacing(1)-meanLocation(1))^2/(2*sigma(1)^2)+(yIndex*spacing(2)-meanLocation(2))^2/(2*sigma(2)^2)+(zIndex*spacing(3)-meanLocation(3))^2/(2*sigma(3)^2)));
            gaussianPSFSum = gaussianPSFSum + gaussianPSF(xIndex,yIndex,zIndex);
        end
    end
end

gaussianPSF = gaussianPSF/gaussianPSFSum;