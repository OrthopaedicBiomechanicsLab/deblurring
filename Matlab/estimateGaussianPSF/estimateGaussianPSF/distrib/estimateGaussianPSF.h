//
// MATLAB Compiler: 4.18.1 (R2013a)
// Date: Wed Nov 25 17:12:23 2015
// Arguments: "-B" "macro_default" "-W" "cpplib:estimateGaussianPSF" "-T"
// "link:lib" "-d"
// "C:\Mike\Git\deblurring\Matlab\estimateGaussianPSF\estimateGaussianPSF\src"
// "-w" "enable:specified_file_mismatch" "-w" "enable:repeated_file" "-w"
// "enable:switch_ignored" "-w" "enable:missing_lib_sentinel" "-w"
// "enable:demo_license" "-v"
// "C:\Mike\Git\deblurring\Matlab\estimateGaussianPSF\estimateGaussianPSF.m" 
//

#ifndef __estimateGaussianPSF_h
#define __estimateGaussianPSF_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#include "mclcppclass.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_estimateGaussianPSF
#define PUBLIC_estimateGaussianPSF_C_API __global
#else
#define PUBLIC_estimateGaussianPSF_C_API /* No import statement needed. */
#endif

#define LIB_estimateGaussianPSF_C_API PUBLIC_estimateGaussianPSF_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_estimateGaussianPSF
#define PUBLIC_estimateGaussianPSF_C_API __declspec(dllexport)
#else
#define PUBLIC_estimateGaussianPSF_C_API __declspec(dllimport)
#endif

#define LIB_estimateGaussianPSF_C_API PUBLIC_estimateGaussianPSF_C_API


#else

#define LIB_estimateGaussianPSF_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_estimateGaussianPSF_C_API 
#define LIB_estimateGaussianPSF_C_API /* No special import/export declaration */
#endif

extern LIB_estimateGaussianPSF_C_API 
bool MW_CALL_CONV estimateGaussianPSFInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_estimateGaussianPSF_C_API 
bool MW_CALL_CONV estimateGaussianPSFInitialize(void);

extern LIB_estimateGaussianPSF_C_API 
void MW_CALL_CONV estimateGaussianPSFTerminate(void);



extern LIB_estimateGaussianPSF_C_API 
void MW_CALL_CONV estimateGaussianPSFPrintStackTrace(void);

extern LIB_estimateGaussianPSF_C_API 
bool MW_CALL_CONV mlxEstimateGaussianPSF(int nlhs, mxArray *plhs[], int nrhs, mxArray 
                                         *prhs[]);


#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

/* On Windows, use __declspec to control the exported API */
#if defined(_MSC_VER) || defined(__BORLANDC__)

#ifdef EXPORTING_estimateGaussianPSF
#define PUBLIC_estimateGaussianPSF_CPP_API __declspec(dllexport)
#else
#define PUBLIC_estimateGaussianPSF_CPP_API __declspec(dllimport)
#endif

#define LIB_estimateGaussianPSF_CPP_API PUBLIC_estimateGaussianPSF_CPP_API

#else

#if !defined(LIB_estimateGaussianPSF_CPP_API)
#if defined(LIB_estimateGaussianPSF_C_API)
#define LIB_estimateGaussianPSF_CPP_API LIB_estimateGaussianPSF_C_API
#else
#define LIB_estimateGaussianPSF_CPP_API /* empty! */ 
#endif
#endif

#endif

extern LIB_estimateGaussianPSF_CPP_API void MW_CALL_CONV estimateGaussianPSF(int nargout, mwArray& sigma, mwArray& cortIntensity, mwArray& edgePositions, mwArray& profileValuesAtEdges, const mwArray& inPlaneFlag, const mwArray& profilesX, const mwArray& profilesIntensity, const mwArray& edges, const mwArray& corticalIntensityMin, const mwArray& corticalIntensityMax, const mwArray& trabecularIntensityMin, const mwArray& trabecularIntensityMax, const mwArray& minCortThickness, const mwArray& minTrabThickness, const mwArray& sigmaMax, const mwArray& sigmaMin, const mwArray& slackFactor, const mwArray& sliceThickness, const mwArray& pixelSpacing, const mwArray& profilePositionsBeginning, const mwArray& profilePositionsEnd, const mwArray& inPlaneSig);

#endif
#endif
