% The objective function to be minimized
% global variables:
% t = distance vector
% y = CT intensities
% n = # of boxcar functions
% y_0 = lower tail constant intensity
% y_f = higher tail constant intensity
% A_CT = area of CT profile


function f = objective(x,t, y, n, nn, e_1_i, e_2_i, inplanemode, inplanesig, angles)

%global t y n nn h e_1_i e_2_i inplanemode inplanesig angles
%definitely surprising global variables being passed.

k = 2; %vector index

for j = 1:nn %Profile incrementation
  
    if inplanemode == 1
        sig = x(1); %Gaussian width
    else
        sig = ((((cos(angles(:,j)))^2)*inplanesig^2)+(((sin(angles(:,j)))^2)*x(1)^2))^0.5 ; %calculate effective sigma based on profile angle
    end
    
    if n(:,j) == 3
        xsub = x(k:k+8);
    else
        xsub = x(k:k+4);
    end
    
    for i = 1:n(:,j) %Intensity values
        Y(i) = xsub(i);
    end
    
    for i = 1:(n(:,j) + 1) %Step locations
        X(i) = xsub(i+n(:,j));
    end
    
    y_0 = xsub(length(xsub)-1);
    y_f = xsub(length(xsub));
    
    h_0 = 0.5*y_0*((erf((t(:,j)-min(t(:,j))+10)/(sqrt(2)*sig)))-(erf((t(:,j)-X(1))/(sqrt(2)*sig)))) ;
    h_f = 0.5*y_f*((erf((t(:,j)-X(n(:,j)+1))/(sqrt(2)*sig)))-(erf((t(:,j)-max(t(:,j))-10)/(sqrt(2)*sig)))) ;
    h = h_0 + h_f;
    
    for i = 1:n(:,j)
        h = h + 0.5*Y(i)*((erf((t(:,j)-X(i))/(sqrt(2)*sig)))-(erf((t(:,j)-X(i+1))/(sqrt(2)*sig))));
    end
    
    if n(:,j) == 3  %advance K to next profile in x vector
        k = k + 9;
    else
        k = k + 5;
    end
    
    y_res(:,j) = trapz(t(e_1_i(:,j):e_2_i(:,j),j), abs(h(e_1_i(:,j):e_2_i(:,j)) - y(e_1_i(:,j):e_2_i(:,j),j))); %residual, entire plot
    
end

% assignin('base', 'sig_1', sig);
% assignin('base', 'sig_2', sig2);
% assignin('base', 'a_1', a1);
% assignin('base', 'a_2', a2);

f = mean(y_res); %sum of all profile residuals to be minimized