%test estimateGaussianPSF for the inplane case
load('CF5760.mat')

inPlaneFlag = 1;

profiles = [];
profilesX(1,:) = handles.profile1.probex;
profilesC(1,:) = handles.profile1.c;
profilesX(2,:) = handles.profile2.probex;
profilesC(2,:) = handles.profile2.c;
profilesX(3,:) = handles.profile3.probex;
profilesC(3,:) = handles.profile3.c;

minCortThickness = handles.min_cortthick;
minTrabThickness= handles.min_trabthick;


edges = [];
edges(1,1) = handles.profile1.edge1;
edges(2,1) = handles.profile1.edge2;
edges(1,2) = handles.profile2.edge1;
edges(2,2) = handles.profile2.edge2;
edges(1,3) = handles.profile3.edge1;
edges(2,3) = handles.profile3.edge2;


corticalIntensityMin =handles.cort_min;
corticalIntensityMax = handles.cort_max;
trabecularIntensityMin = handles.sigma_min;
trabecularIntensityMax = handles.sigma_max;

sigmaMax = handles.sigma_max;
sigmaMin = handles.sigma_min;
slackFactor = handles.slack_fac;



%optional parameters that I think are only necessary for out of plane
%estimate of sigma
sliceThickness = handles.metadata.SliceThickness;
pixelSpacing = handles.metadata.PixelSpacing;

profilePositions = [];
profilePositionsBeg(1,1) = handles.profile1.xi(1);
profilePositionsBeg(1,2) = handles.profile1.yi(1);
profilePositionsEnd(1,1) = handles.profile1.xi(2);
profilePositionsEnd(1,2) = handles.profile1.yi(2);

profilePositionsBeg(2,1) = handles.profile2.xi(1);
profilePositionsBeg(2,2) = handles.profile2.yi(1);
profilePositionsEnd(2,1) = handles.profile2.xi(2);
profilePositionsEnd(2,2) = handles.profile2.yi(2);

profilePositionsBeg(3,1) = handles.profile3.xi(1);
profilePositionsBeg(3,2) = handles.profile3.yi(1);
profilePositionsEnd(3,1) = handles.profile3.xi(2);
profilePositionsEnd(3,2) = handles.profile3.yi(2);



[ inPlaneSigma,  cort_thicknesses, edgePositions, profileValuesAtEdges] = estimateGaussianPSF( inPlaneFlag, profilesX, profilesC, edges, corticalIntensityMin, corticalIntensityMax, trabecularIntensityMin, trabecularIntensityMax, minCortThickness, minTrabThickness, sigmaMax,sigmaMin,slackFactor,sliceThickness,pixelSpacing,profilePositionsBeg,profilePositionsEnd)


inPlaneSigma

handles.thesig


% Make sure sigma is correct, or throw an exception
if handles.thesig ~= inPlaneSigma

    % 2) Construct an MException object to represent the error.
    err = MException('ResultChk:BadInput', ...
        'In Plane sigma value should match precalulcated sigma value for this test');
   
    % 4) Throw the exception to stop execution and display an error message.
    %throw(err)
end





%Test the out of plane calculations
load('CF5760_zx_zy.mat')

inPlaneFlag = 0;

profiles = [];
profilesX(1,:) = handles.profile1.probex;
profilesC(1,:) = handles.profile1.c;
profilesX(2,:) = handles.profile2.probex;
profilesC(2,:) = handles.profile2.c;
profilesX(3,:) = handles.profile3.probex;
profilesC(3,:) = handles.profile3.c;

minCortThickness = handles.min_cortthick;
minTrabThickness= handles.min_trabthick;


edges = [];
edges(1,1) = handles.profile1.edge1;
edges(2,1) = handles.profile1.edge2;
edges(1,2) = handles.profile2.edge1;
edges(2,2) = handles.profile2.edge2;
edges(1,3) = handles.profile3.edge1;
edges(2,3) = handles.profile3.edge2;


corticalIntensityMin =handles.cort_min;
corticalIntensityMax = handles.cort_max;
trabecularIntensityMin = handles.sigma_min;
trabecularIntensityMax = handles.sigma_max;

sigmaMax = handles.sigma_max;
sigmaMin = handles.sigma_min;
slackFactor = handles.slack_fac;



%optional parameters that I think are only necessary for out of plane
%estimate of sigma
sliceThickness = handles.metadata.SliceThickness;
pixelSpacing = handles.metadata.PixelSpacing;

%pretty sure that handles has the profile position information stored as
%pixel information, however its probably more covienient in slicer to pass
%info in physical units.  Therefore I will adjust the profile position
%information to make the z values bigger

profilePositions = [];
if strcmp(handles.profile1.orientation,'xy')==1
    profilePositionsBeg(1,1) = handles.profile1.xi(1);
    profilePositionsBeg(1,2) = handles.profile1.yi(1);
    profilePositionsBeg(1,3) = 0;
    profilePositionsEnd(1,1) = handles.profile1.xi(2);
    profilePositionsEnd(1,2) = handles.profile1.yi(2);
    profilePositionsEnd(1,3) = 0;
    
    profilePositionsBeg(2,1) = handles.profile2.xi(1);
    profilePositionsBeg(2,2) = handles.profile2.yi(1);
    profilePositionsBeg(2,3) = 0;
    profilePositionsEnd(2,1) = handles.profile2.xi(2);
    profilePositionsEnd(2,2) = handles.profile2.yi(2);
    profilePositionsEnd(2,3) = 0;
    
    profilePositionsBeg(3,1) = handles.profile3.xi(1);
    profilePositionsBeg(3,2) = handles.profile3.yi(1);
    profilePositionsBeg(3,3) = 0;
    profilePositionsEnd(3,1) = handles.profile3.xi(2);
    profilePositionsEnd(3,2) = handles.profile3.yi(2);
    profilePositionsEnd(3,3) = 0;
    
elseif strcmp(handles.profile1.orientation,'yz')==1
    profilePositionsBeg(1,1) = 0;
    profilePositionsBeg(1,2) = handles.profile1.xi(1);
    profilePositionsBeg(1,3) = handles.profile1.yi(1);
    profilePositionsEnd(1,1) = 0;
    profilePositionsEnd(1,2) = handles.profile1.xi(2);
    profilePositionsEnd(1,3) = handles.profile1.yi(2);
    
    profilePositionsBeg(2,1) = 0;
    profilePositionsBeg(2,2) = handles.profile2.xi(1);
    profilePositionsBeg(2,3) = handles.profile2.yi(1);
    profilePositionsEnd(2,1) = 0;
    profilePositionsEnd(2,2) = handles.profile2.xi(2);
    profilePositionsEnd(2,3) = handles.profile2.yi(2);
    
    profilePositionsBeg(3,1) = 0;
    profilePositionsBeg(3,2) = handles.profile3.xi(1);
    profilePositionsBeg(3,3) = handles.profile3.yi(1);
    profilePositionsEnd(3,1) = 0;
    profilePositionsEnd(3,2) = handles.profile3.xi(2);
    profilePositionsEnd(3,3) = handles.profile3.yi(2);
    
    
elseif strcmp(handles.profile1.orientation,'xz')==1
    profilePositionsBeg(1,1) = handles.profile1.xi(1);
    profilePositionsBeg(1,2) = 0;
    profilePositionsBeg(1,3) = handles.profile1.yi(1);
    profilePositionsEnd(1,1) = handles.profile1.xi(2);
    profilePositionsEnd(1,2) = 0;
    profilePositionsEnd(1,3) = handles.profile1.yi(2);
   
    profilePositionsBeg(2,1) = handles.profile2.xi(1);
    profilePositionsBeg(2,2) = 0;
    profilePositionsBeg(2,3) = handles.profile2.yi(1);
    profilePositionsEnd(2,1) = handles.profile2.xi(2);
    profilePositionsEnd(2,2) = 0;
    profilePositionsEnd(2,3) = handles.profile2.yi(2);
    
    profilePositionsBeg(3,1) = handles.profile3.xi(1);
    profilePositionsBeg(3,2) = 0;
    profilePositionsBeg(3,3) = handles.profile3.yi(1);
    profilePositionsEnd(3,1) = handles.profile3.xi(2);
    profilePositionsEnd(3,2) = 0;
    profilePositionsEnd(3,3) = handles.profile3.yi(2);
end

profilePositionsBeg(:,1)=pixelSpacing(1)*profilePositionsBeg(:,1);
profilePositionsBeg(:,2)=pixelSpacing(2)*profilePositionsBeg(:,2);
profilePositionsBeg(:,3)=sliceThickness*profilePositionsBeg(:,3);

profilePositionsEnd(:,1)=pixelSpacing(1)*profilePositionsEnd(:,1);
profilePositionsEnd(:,2)=pixelSpacing(2)*profilePositionsEnd(:,2);
profilePositionsEnd(:,3)=sliceThickness*profilePositionsEnd(:,3);

[ outOfPlaneSigma,  cort_thicknesses, edgePositions, profileValuesAtEdges] = estimateGaussianPSF( inPlaneFlag, profilesX,profilesC, edges, corticalIntensityMin, corticalIntensityMax, trabecularIntensityMin, trabecularIntensityMax, minCortThickness, minTrabThickness, sigmaMax,sigmaMin,slackFactor,sliceThickness,pixelSpacing,profilePositionsBeg,profilePositionsEnd, inPlaneSigma)

% Make sure sigma is correct, or throw an exception
handles.thesig

if handles.thesig ~= outOfPlaneSigma
    

    % 2) Construct an MException object to represent the error.
    err = MException('ResultChk:BadInput', ...
        'In Plane sigma value should match precalulcated sigma value for this test');
   
    % 4) Throw the exception to stop execution and display an error message.
    throw(err)
end

