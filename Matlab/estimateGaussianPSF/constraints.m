% non-linear constraint, matching areas
% global variables:
% t = distance vector
% y = CT intensities
% n = # of boxcar functions
% y_0 = lower tail constant intensity
% y_f = higher tail constant intensity
% A_CT = area of CT profile

function[c,ceq,gradc,gradceq] = constraints(x,n, nn, A_CT, t, y_b, e_1_i, e_2_i, min_cortthick, min_trabthick, inplanesig, angles, inplanemode)



k = 2;

c = [];
ceq = [];

for j = 1:nn  %profile incrementation
    
    if inplanemode == 1
        sig = x(1); %Gaussian width
    else
        sig = ((((cos(angles(:,j)))^2)*inplanesig^2)+(((sin(angles(:,j)))^2)*x(1)^2))^0.5 ; %calculate effective sigma based on profile angle
    end
    
    if n(:,j) == 3
        xsub = x(k:k+8);
    else
        xsub = x(k:k+4);
    end
    
    for i = 1:n(:,j) %Intensity values
        Y(i) = xsub(i);
    end
    
    for i = 1:(n(:,j) + 1) %Step locations
        X(i) = xsub(i+n(:,j));
    end
    
    y_0 = xsub(length(xsub)-1);
    y_f = xsub(length(xsub));
    
    if n(:,j) == 3
        c = [c ; X(1)-X(2) + min_cortthick ; X(2)-X(3) + min_trabthick ; X(3)-X(4) + min_cortthick]; %
    else
        c = [c ; X(1)-X(2) + min_cortthick];
    end
    
    h_0 = 0.5*y_0*((erf((t(:,j)-min(t(:,j))+10)/(sqrt(2)*sig)))-(erf((t(:,j)-X(1))/(sqrt(2)*sig))));%y_0 + (y_f - y_0)*heaviside(t-X(n+1));
    h_f = 0.5*y_f*((erf((t(:,j)-X(n(:,j)+1))/(sqrt(2)*sig)))-(erf((t(:,j)-max(t(:,j))-10)/(sqrt(2)*sig))));
    h = h_0 + h_f;
    
    for i = 1:n(:,j)
        h = h + 0.5*Y(i)*((erf((t(:,j)-X(i))/(sqrt(2)*sig)))-(erf((t(:,j)-X(i+1))/(sqrt(2)*sig))));
    end
    
    A = trapz(t(e_1_i(:,j):e_2_i(:,j),j),h(e_1_i(:,j):e_2_i(:,j))-y_b(:,j));
    
    if n(:,j) == 3  %advance K to next profile in x vector
        k = k + 9;
    else
        k = k + 5;
    end
    
    ceq = [ceq ; A - A_CT(:,j) ; ]; %Y(1)-Y(3)
    
    
end


%if nargout > 2
%    gradc = [[0;0;0;0;1;-1;0;0],[0;0;0;0;0;1;-1;0],[0;0;0;0;0;0;1;-1]];
%    gradceq = [[0; -x(5)+x(6);   -x(6)+x(7);   -x(7)+x(8);   -x(2);   x(2)-x(3);     x(3)-x(4);  x(4)], [0;1;0;-1;0;0;0;0]];
%end

