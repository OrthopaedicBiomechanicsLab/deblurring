function stop = outfun(x,optimValues,state,p,t, y, n, nn, inplanesig, angles, inplanemode)
stop = false;
% Check if user has requested to stop the optimization.

%global t y n nn inplanesig angles inplanemode

k = 2; %vector index

for j = 1:nn %Profile incrementation
           
    if inplanemode == 1
        sig = x(1); %Gaussian width
    else
        sig = ((((cos(angles(:,j)))^2)*inplanesig^2)+(((sin(angles(:,j)))^2)*x(1)^2))^0.5 ; %calculate effective sigma based on profile angle
    end
    
    if n(:,j) == 3
        xsub = x(k:k+8);
    else
        xsub = x(k:k+4);
    end
    
    for i = 1:n(:,j) %Intensity values
        Y(i) = xsub(i);
    end
    
    for i = 1:(n(:,j) + 1) %Step locations
        X(i) = xsub(i+n(:,j));
    end
    
    y_0 = xsub(length(xsub)-1);
    y_f = xsub(length(xsub));
    
    h_0 = 0.5*y_0*((erf((t(:,j)-min(t(:,j))+10)/(sqrt(2)*sig)))-(erf((t(:,j)-X(1))/(sqrt(2)*sig))));
    h_f = 0.5*y_f*((erf((t(:,j)-X(n(:,j)+1))/(sqrt(2)*sig)))-(erf((t(:,j)-max(t(:,j))-10)/(sqrt(2)*sig))));
    h = h_0 + h_f;
    
    for i = 1:n(:,j)
        h = h + 0.5*Y(i)*((erf((t(:,j)-X(i))/(sqrt(2)*sig)))-(erf((t(:,j)-X(i+1))/(sqrt(2)*sig))));
    end
    
    z(:,1,j) = t(:,j); %CT(:,1); %first column: x-values, second column: CT HU values of actual profile
    z(:,2,j) = y(:,j);
    z(:,3,j) = y_0*rectpuls(t(:,j)-((X(1)+min(t(:,j)))/2),(X(1)-min(t(:,j))))+y_f*rectpuls(t(:,j)-((max(t(:,j))+X(n(:,j)+1))/2),(max(t(:,j))-X(n(:,j)+1)));
    for i = 1:n(:,j)
        z(:,3,j) = z(:,3,j) + Y(i)*rectpuls(t(:,j)-((X(i+1)+X(i))/2),(X(i+1)-X(i))); %third column: HU values of double rectangle function
    end
    z(:,4,j) = h; %fourth column: HU values of blurred rectangle function
    
    z(:,5,j) = abs(z(:,4,j) - z(:,2,j)); %residual
    
    if n(:,j) == 3  %advance K to next profile in x vector
        k = k + 9;
    else
        k = k + 5;
    end
    
end

plot(z(:,1,p),z(:,2:5,p))
% xlabel('Profile Distance (mm)');
% ylabel('HU');
% legend('CT Profile','Rectangular Profile Estimate','Best Fit Profile','Residual')
