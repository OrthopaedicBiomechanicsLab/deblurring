function estimate(handles)
% hObject    handle to estimate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% t = distance vector, not to be confused with variable vector x
% y = CT intensities
% n = # of boxcar functions
% y_0 = lower tail constant intensity
% y_f = higher tail constant intensity
% A_CT = area of CT profile


clearvars -global t y n A_CT y_b e_1_i e_2_i nn h xsub min_cortthick min_trabthick angles

global t y n A_CT y_b e_1_i e_2_i nn min_cortthick min_trabthick inplanemode inplanesig angles

min_cortthick = handles.min_cortthick;
min_trabthick = handles.min_trabthick;

sig_max = handles.sigma_max;  %sigma bounds
sig_min = handles.sigma_min;
sig_ini = (sig_max+sig_min)/2;

cort_max = handles.cort_max;    %cortical intensity bounds
cort_min = handles.cort_min;
cort_ini = (cort_max + cort_min)/2;

trab_max = handles.trab_max;     %trabecular intensity bounds
trab_min = handles.trab_min;
trab_ini = (trab_max + trab_min)/2;

%start assembly of bounds vectors
x0 = [sig_ini];
lb = [sig_min];
ub = [sig_max];

%Do the slice and longitudinal profiles seperation here

if exist('handles.selectinslice', 'var') == 1
    inplanemode = 1; %global to track if the estimation is being done for in-plane
    nn = handles.inplaneprofiles;
    handles.profilesubset = nn;   %number of profiles
    j = 1;
    for i = 1:handles.numprofiles
        if strcmp(handles.(['profile',num2str(i)]).orientation,'xy') == 1
            t(:,j) = handles.(['profile',num2str(i)]).probex;
            y(:,j) = handles.(['profile',num2str(i)]).c;
            n(:,j) = handles.n(:,i);
            edge1(:,j) = handles.(['profile',num2str(i)]).edge1;
            edge2(:,j) = handles.(['profile',num2str(i)]).edge2;
            orderxy(j) = i; %saves the master set location of the profile of the xy subset
            j = j + 1;
        end
    end
end

if exist(handles.selectlongitudinal,'var') == 1
    inplanemode = 0;
    inplanesig = str2num(get(handles.sigxy,'String'));
    nn = handles.longitudinalprofiles;
    handles.profilesubset = nn;   %number of profiles
    j = 1;
    for i = 1:handles.numprofiles
        if strcmp(handles.(['profile',num2str(i)]).orientation,'xy') == 0
            t(:,j) = handles.(['profile',num2str(i)]).probex;
            y(:,j) = handles.(['profile',num2str(i)]).c;
            n(:,j) = handles.n(:,i);
            
            %pixel size ratio must be taken into account for angle
            %calculation
            y_scale = handles.metadata.SliceThickness/handles.PixelSpacing(1);
            angles(:,j) =  atan((y_scale*abs((handles.(['profile',num2str(i)]).yi(2)-handles.(['profile',num2str(i)]).yi(1)))/abs((handles.(['profile',num2str(i)]).xi(2)-handles.(['profile',num2str(i)]).xi(1))))); %calculate cortical surface angle
            %angles(:,j) = -0.52;
            
            edge1(:,j) = handles.(['profile',num2str(i)]).edge1;      
            edge2(:,j) = handles.(['profile',num2str(i)]).edge2;
            orderz(j) = i; %saves the master set location of the profile of the xy subset
            j = j + 1;
        end
    end
end

for i = 1:j-1 %make an amalgamated vector
    
    %Edge settings based on segmentation
    x_edge_1 = edge1(:,i); %t(round(length(t)*0.4));%t(find(handles.(['profile',num2str(i)]).segc,1,'first'),i);
    x_edge_2 = edge2(:,i); %t(find(handles.(['profile',num2str(i)]).segc,1,'last'), i);
    w = (x_edge_2 - x_edge_1)/2;
    
    
    t_min = (x_edge_1 + x_edge_2)/2 - handles.slack_fac*w; %set lb and up, extend of the profile
    t_max = (x_edge_1 + x_edge_2)/2 + handles.slack_fac*w;
    
    %leading and trailing y-values/intensities averaging, and choosing the
    %"baseline"
    if t_min <= 0
        t_min = 0;
    end
    last = t(length(t(:,i)),i);
    if t_max >= last;
        t_max = last;
    end
    e_1_i(:,i) = searchclosest(t(:,i),t_min);
    y_0_v = y(1:searchclosest(t(:,i),x_edge_1),i);
    y_0_ini = mean(y_0_v);
    e_2_i(:,i) = searchclosest(t(:,i),t_max);
    y_f_v = y(searchclosest(t(:,i),x_edge_2):end,i);
    y_f_ini = mean(y_f_v);
    
    if y_0_ini < y_f_ini %set baseline/reference y
        y_b(:,i) = y_0_ini;
    else
        y_b(:,i) = y_f_ini;
    end
    
    A_CT(:,i) = trapz(t(e_1_i(:,i):e_2_i(:,i),i),y(e_1_i(:,i):e_2_i(:,i),i)-y_b(:,i));
    
    if n(:,i) == 3
        t_div = (t_max-t_min)/n(:,i);
        x0 = [x0   cort_ini    trab_ini    cort_ini   t_min    t_min     t_min+t_div*2       t_max   y_0_ini   y_f_ini]   ;   %solution seed
        lb = [lb   cort_min    trab_min    cort_min   t_min    t_min     t_min      t_min     y_0_ini-10*std(y_0_v)   y_f_ini-10*std(y_0_v)   ];
        ub = [ub   cort_max    trab_max    cort_max   t_max    t_max     t_max      t_max     y_0_ini+10*std(y_0_v)  y_f_ini+10*std(y_0_v)  ];
    end
    
    if n(:,i) == 1
        x0 = [x0 cort_ini    t_min       t_max     y_0_ini   y_f_ini]   ;   %solution seed
        lb = [lb cort_min    t_min       t_min     y_0_ini-10*std(y_0_v)   y_f_ini-10*std(y_f_v)   ];
        ub = [ub cort_max    t_max       t_max     y_0_ini+10*std(y_0_v)   y_f_ini+10*std(y_f_v)   ];
    end
    
    A = [];
    b = [];
    
    Aeq = [];
    beq = [];
    
    
end

clear i

%a brute force way of making the cell array necessary for the plotting component of the optim function
c = {};
c(1) = {@(x,optimValues,state)outfun(x,optimValues,state,1)};
c(2) = {@(x,optimValues,state)outfun(x,optimValues,state,2)};
c(3) = {@(x,optimValues,state)outfun(x,optimValues,state,3)};
c(4) = {@(x,optimValues,state)outfun(x,optimValues,state,4)};
c(5) = {@(x,optimValues,state)outfun(x,optimValues,state,5)};
c(6) = {@(x,optimValues,state)outfun(x,optimValues,state,6)};
c(7) = {@(x,optimValues,state)outfun(x,optimValues,state,7)};
c(8) = {@(x,optimValues,state)outfun(x,optimValues,state,8)};
c(9) = {@(x,optimValues,state)outfun(x,optimValues,state,9)};
c(10) = {@(x,optimValues,state)outfun(x,optimValues,state,10)};
c(11) = {@(x,optimValues,state)outfun(x,optimValues,state,11)};
c(12) = {@(x,optimValues,state)outfun(x,optimValues,state,12)};
c(13) = {@(x,optimValues,state)outfun(x,optimValues,state,13)};
c(14) = {@(x,optimValues,state)outfun(x,optimValues,state,14)};
c(15) = {@(x,optimValues,state)outfun(x,optimValues,state,15)};

handles.plotarray = c(1:handles.profilesubset);

options = optimset('Algorithm','interior-point','display','on', 'GradConstr','off','DerivativeCheck','off','TolFun',1e-10*sum(A_CT),'PlotFcns', handles.plotarray, 'MaxFunEvals',40000,'UseParallel','always' )%
[points , fval] = fmincon(@objective,x0,A,b,Aeq,beq,lb,ub,@constraints,options);  %objective function value

if get(handles.selectinslice,'Value') == 1
    set(handles.sigxy,'String',num2str(points(1)));
    inplanesig = points(1);
end

if get(handles.selectlongitudinal,'Value') == 1
    inplanesig = str2num(get(handles.sigxy,'String'));
    set(handles.sigz,'String',num2str(points(1)));
end

assignin('base', 'result', points);

%%%%%%%%%%%%%%%%%% translate results
k = 2;

for j = 1:nn %Profile incrementation
    
    thesig = points(1);
    
    handles.thesig = thesig;
    
    if n(:,j) == 3
        xsub(:,j) = points(k:k+8);
    else
        xsub(:,j) = [points(k:k+4) 0 0 0 0];
    end
    
    for i = 1:n(:,j) %Intensity values
        inten(i,j) = xsub(i,j);
    end
    
    for i = 1:(n(:,j)) %Step locations
        thick(i,j) = xsub(i+n(:,j)+1,j) - xsub(i+n(:,j),j);
    end
    
    if n(:,j) == 3  %advance k to next profile in x vector
        k = k + 9;
    else
        k = k + 5;
    end
    
end

%order everything back into a matrix
if get(handles.selectinslice,'Value') == 1
    handles.xsub = [];
    handles.thick = [];
    handles.inten = [];
    for j = 1:length(orderxy)
        handles.xsub(:,orderxy(j))= xsub(:,j);
        handles.thick(:,orderxy(j)) = thick(:,j);
        handles.inten(:,orderxy(j)) = inten(:,j);
    end
end

if get(handles.selectlongitudinal,'Value') == 1
    for j = 1:length(orderz)
        handles.xsub(:,orderz(j))= xsub(:,j);
        handles.thick(:,orderz(j)) = thick(:,j);
        handles.inten(:,orderz(j)) = inten(:,j);
    end
end

set(handles.table, 'Data', [handles.inten handles.thick]);

