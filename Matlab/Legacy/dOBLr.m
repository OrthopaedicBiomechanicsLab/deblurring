function varargout = dOBLr(varargin)
% dOBLr MATLAB code for dOBLr.fig
%      dOBLr, by itself, creates a new dOBLr or raises the existing
%      singleton*.
%
%      H = dOBLr returns the handle to a new dOBLr or the handle to
%      the existing singleton*.
%
%      dOBLr('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in dOBLr.M with the given input arguments.
%
%      dOBLr('Property','Value',...) creates a new dOBLr or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dOBLr_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dOBLr_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dOBLr

% Last Modified by GUIDE v2.5 19-Nov-2012 13:19:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @dOBLr_OpeningFcn, ...
    'gui_OutputFcn',  @dOBLr_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dOBLr is made visible.
function dOBLr_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dOBLr (see VARARGIN)

%cd('E:\dOBLr')

% Choose default command line output for dOBLr
handles.output = hObject;

%default values
set(handles.winlow,'String','-500');
set(handles.winhi,'String','1500');
set(handles.sampling,'String','500');
set(handles.psfsize,'String','40');
handles.CTloadflag = 0;
handles.winlow = -500;
handles.winhi = 1500;
handles.numsamples = 500;
handles.donedeblur = 0;
handles.psf_extend = 40;

handles.lasthighest = 1;

set(handles.sigmamin,'String',0.2);
handles.sigma_min = 0.2;
set(handles.sigmamax,'String',2);
handles.sigma_max = 2;
set(handles.cortmin,'String',1100);
handles.cort_min = 1100;
set(handles.cortmax,'String',2200);
handles.cort_max = 2200;
set(handles.trabmin,'String',0);
handles.trab_min = 0;
set(handles.trabmax,'String',800);
handles.trab_max = 800;
set(handles.slackfac,'String',3.5);
handles.slack_fac = 3.5;
set(handles.mincortthick,'String',0.1);
handles.min_cortthick = 0.1;
set(handles.mintrabthick,'String',0.5);
handles.min_trabthick = 0.5;

% Update handles structure
guidata(hObject, handles);



% UIWAIT makes dOBLr wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = dOBLr_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadCT.
function loadCT_Callback(hObject, eventdata, handles)

[handles.filename, pathname, filterindex] = uigetfile( ...
    {  '*.dcm','DICOM files (*.dcm)';
    '*.*',  'All Files (*.*)'}, ...
    'Pick a file', ...
    'MultiSelect', 'on');

addpath(pathname)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% if loading a normal image series
handles.metadata = dicominfo(handles.filename{1}); %store DICOM meta data

for i = 1:length(handles.filename)
    handles.CT(:,:,i)=single(dicomread(handles.filename{i}) + handles.metadata.RescaleIntercept);
end

handles.PixelSpacing = handles.metadata.PixelSpacing; %within slice pixel sizing
if exist('handles.metadata.SpacingBetweenSlices','var')
    %otherwise assume already set correctly
    handles.metadata.SliceThickness = handles.metadata.SpacingBetweenSlices;
end
     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% if loading a volume
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% without a header.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% manual input of voxel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% size

% handles.metadata = dicominfo(handles.filename); %store DICOM meta data
% handles.CT = dicomread(handles.filename);
% 
% handles.metadata.SliceThickness = 0.5;
% handles.metadata.PixelSpacing = [0.43,0.43];
% handles.PixelSpacing = handles.metadata.PixelSpacing; %within slice pixel sizing

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


handles.CT(handles.CT <= -1000) = -1000;

V = handles.CT;

handles.numzslices = length(V(1,1,:)); %number of slices in z direction
handles.numxslices = length(V(1,:,1)); %number of slices in x direction - this is actuallly number of columns
handles.numyslices = length(V(:,1,1)); %number of slices in y direction - this is actuallly number of rows

handles.xycurrent = round(handles.numzslices/2); %current XY orientation plot position, initialized to halfpoint
handles.xzcurrent = round(handles.numyslices/2); %current XZ orientation plot position, initialized to halfpoint
handles.yzcurrent = round(handles.numxslices/2); %current YZ orientation plot position, initialized to halfpoint

%set slider range for first-time opening of image stack
set(handles.maxslicenum,'String',num2str(handles.numzslices)); %max label
set(handles.slider,'Min',1); %set slider min value
set(handles.slider,'Max',handles.numzslices); %set slider max value
set(handles.slicenumbox,'String',num2str(handles.xycurrent)); %set box label of slider current position
set(handles.slider,'Value',handles.xycurrent);%set slider current position to halfway
set(handles.slider,'SliderStep',[1/(handles.numzslices-1) 1/(handles.numzslices-1)]);



%print image size info
set(handles.imagesizeinfo,'String',['Image volume size (X*Y*Z): ' num2str(handles.numxslices) '*' num2str(handles.numyslices) '*' num2str(handles.numzslices)]);
%print image resolution info
set(handles.imageresinfo,'String',['Voxel size (X*Y*Z): ' num2str(handles.metadata.PixelSpacing(1)) '*' num2str(handles.metadata.PixelSpacing(2)) '*' num2str(handles.metadata.SliceThickness) ' mm']);

set(gcf,'CurrentAxes',handles.imageplot);    %set on to which axes to plot
imshow(handles.CT(:,:,str2double(get(handles.slicenumbox,'String'))),[handles.winlow handles.winhi]);

%initialize orientation buttons
set(handles.xy,'Value',1)
set(handles.xz,'Value',0)
set(handles.yz,'Value',0)

%initialize image set selection
set(handles.ctview,'Value',1)

guidata(hObject, handles);




% hObject    handle to loadCT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in Loadsegment.
function Loadsegment_Callback(hObject, eventdata, handles)
% hObject    handle to Loadsegment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname, filterindex] = uigetfile( ...
    {  '*.dcm','DICOM files (*.dcm)';
    '*.*',  'All Files (*.*)'}, ...
    'Pick a file', ...
    'MultiSelect', 'on');

for i = 1:length(filename)
    handles.segment(:,:,i)=dicomread(filename{i});
end


%[handles.filename, user_canceled] = imgetfile;  %get location of first image in set
%[handles.segment,info] = ReadData3D(handles.filename); %use this 3rd party module to readin all images

refreshview(hObject, eventdata, handles)

guidata(hObject, handles);


% --- Executes on button press in viewCT.
function viewCT_Callback(hObject, eventdata, handles)
% hObject    handle to viewCT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(gcf,'CurrentAxes',handles.imageplot);
imshow(handles.CT, [handles.winlow handles.winhi]);

% --- Executes on button press in viewsegment.
function viewsegment_Callback(hObject, eventdata, handles)
% hObject    handle to viewsegment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(gcf,'CurrentAxes',handles.imageplot);
imshow(handles.segment, [0 1]);

function winlow_Callback(hObject, eventdata, handles)
% hObject    handle to winlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.winlow = str2double(get(hObject,'String'));

refreshview(hObject, eventdata, handles);

guidata(hObject, handles)

% Hints: get(hObject,'String') returns contents of winlow as text
%        str2double(get(hObject,'String')) returns contents of winlow as a double


% --- Executes during object creation, after setting all properties.
function winlow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to winlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function winhi_Callback(hObject, eventdata, handles)
% hObject    handle to winhi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.winhi = str2double(get(hObject,'String'))

refreshview(hObject, eventdata, handles)

guidata(hObject, handles)

% Hints: get(hObject,'String') returns contents of winhi as text
%        str2double(get(hObject,'String')) returns contents of winhi as a double


% --- Executes during object creation, after setting all properties.
function winhi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to winhi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function numprofiles_Callback(hObject, eventdata, handles)
% hObject    handle to numprofiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.donedeblur = 0; %if user updates number of profiles, deblurring has to be redone

handles.numprofiles = str2double(get(hObject,'String'));
list = [1:handles.numprofiles];
numlist = num2str(list);
strlist = regexp(numlist, '\s*', 'split');
set(handles.activeprofile,'String',strlist);

for i = 1:handles.numprofiles
    handles.(['profile',num2str(i)]) = []
end

handles.lasthighest = 1;

% Hints: get(hObject,'String') returns contents of numprofiles as text
%        str2double(get(hObject,'String')) returns contents of numprofiles as a double

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function numprofiles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numprofiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in activeprofile.
function activeprofile_Callback(hObject, eventdata, handles)
% hObject    handle to activeprofile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns activeprofile contents as cell array
%        contents{get(hObject,'Value')} returns selected item from activeprofile

handles.currentprofilenum = get(hObject,'Value');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function activeprofile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to activeprofile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in extract_profile.
function extract_profile_Callback(hObject, eventdata, handles)
% hObject    handle to extract_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.donedeblur = 0; %whenever a new profile is extracted, deblurring has to be redone

i = handles.currentprofilenum; %index, string type
handles.n(:,i) = handles.type; %assigns and saves the profile type for the current extraction

if handles.currentprofilenum > handles.lasthighest
    handles.lasthighest = handles.currentprofilenum;
end

set(gcf,'CurrentAxes',handles.imageplot);    %set focus

%set(handles.ctview,'Value',1) %set to CT view mode and show

%[vector x cordinate, vector y cordinates, line vector, x end poind coordinates, y end point coordinates)
%user extract interactively by mouse click
[handles.(['profile',num2str(i)]).cx, handles.(['profile',num2str(i)]).cy, handles.(['profile',num2str(i)]).c, handles.(['profile',num2str(i)]).xi, handles.(['profile',num2str(i)]).yi] = improfile(handles.numsamples,'bicubic');


%register orientation
if get(handles.xy,'Value') == 1
    handles.(['profile',num2str(i)]).orientation = 'xy';
    %    tempsegset = handles.segment(:,:,handles.xycurrent);
else if get(handles.xz,'Value') == 1
        handles.(['profile',num2str(i)]).orientation = 'xz';
        %        tempsegset = rot90(squeeze(handles.segment(handles.xzcurrent,:,:)),1);
    else if get(handles.yz,'Value') == 1
            handles.(['profile',num2str(i)]).orientation = 'yz';
            %            tempsegset = rot90(squeeze(handles.segment(:,handles.yzcurrent,:)),1);
        end
    end
end

%take the same profile using same coordinates as above on the segmentation
%set

%%%%handles.(['profile',num2str(i)]).segc = improfile(tempsegset, handles.(['profile',num2str(i)]).xi, handles.(['profile',num2str(i)]).yi, handles.numsamples);

%register slice number
handles.(['profile',num2str(i)]).slicenum =  str2double(get(handles.slicenumbox,'String'));

%dimention calculation, properly scaling according to pixel pitch and slice
%thickness.
if strcmp(handles.(['profile',num2str(i)]).orientation,'xy') == 1
    scale1 = handles.PixelSpacing(1);
    scale2 = handles.PixelSpacing(2);
else if strcmp(handles.(['profile',num2str(i)]).orientation,'xz') == 1 || strcmp(handles.(['profile',num2str(i)]).orientation,'yz') == 1
        scale1 = handles.PixelSpacing(1);
        scale2 = handles.metadata.SliceThickness;
    end
end
%probex is actual distance for each sampled point, based on pixel size
handles.(['profile',num2str(i)]).probex = (((scale1.*(handles.(['profile',num2str(i)]).cx - handles.(['profile',num2str(i)]).cx(1))).^2 + (scale2.*(handles.(['profile',num2str(i)]).cy - handles.(['profile',num2str(i)]).cy(1))).^2).^(0.5));

set(gcf,'CurrentAxes',handles.profileplot);
[AX,H1,H2] = plotyy(handles.(['profile',num2str(i)]).probex, handles.(['profile',num2str(i)]).c, handles.(['profile',num2str(i)]).probex, handles.(['profile',num2str(i)]).c,'plot');
set(get(AX(1),'Ylabel'),'String','Intensity (HU)');
set(get(AX(2),'Ylabel'),'String','Segmentation (binary)');
ylimits = get(AX(1),'YLim');
yinc = (ylimits(2)-ylimits(1))/20;
set(AX(1),'YTick',[ylimits(1):yinc:ylimits(2)]);
set(AX(2),'YTick',[0,0.5,1]);
xlabel('Probe Length (mm)');
title('Intensity Profile');

set(gcf,'CurrentAxes',handles.imageplot);
refreshview(hObject, eventdata, handles)

hold on
plot(handles.(['profile',num2str(i)]).cx,handles.(['profile',num2str(i)]).cy);
hold off

%for reviewing profiles in Variable Editor
assignin('base', 'review', handles)

%count profile types
handles.inplaneprofiles = 0;
handles.longitudinalprofiles = 0;
for i = 1:handles.lasthighest
    if strcmp(handles.(['profile',num2str(i)]).orientation,'xy') == 1
        handles.inplaneprofiles = handles.inplaneprofiles + 1;
    else
        handles.longitudinalprofiles = handles.longitudinalprofiles + 1;
    end
end

%update count lables
set(handles.inplanecount,'String',num2str(handles.inplaneprofiles)); %max label
set(handles.longitudinalcount,'String',num2str(handles.longitudinalprofiles)); %max label

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in plot_profile.
function plot_profile_Callback(hObject, eventdata, handles)
% hObject    handle to plot_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

j = handles.currentprofilenum; %index, string type

%update image view to the orientation and slince number of the profile
if strcmp(handles.(['profile',num2str(j)]).orientation,'xy') == 1 %if selectected profiles orientation is...
    set(handles.xy,'Value',1); %set image desplay orientation button
    set(handles.xz,'Value',0);
    set(handles.yz,'Value',0);
    handles.xycurrent = handles.(['profile',num2str(j)]).slicenum; %set the current image plot slice
else if strcmp(handles.(['profile',num2str(j)]).orientation,'xz') == 1
        set(handles.xy,'Value',0);
        set(handles.xz,'Value',1);
        set(handles.yz,'Value',0);
        handles.xzcurrent = handles.(['profile',num2str(j)]).slicenum;
    else if strcmp(handles.(['profile',num2str(j)]).orientation,'yz') == 1
            set(handles.xy,'Value',0)
            set(handles.xz,'Value',0)
            set(handles.yz,'Value',1)
            handles.yzcurrent = handles.(['profile',num2str(j)]).slicenum;
        end
    end
end

refreshview(hObject, eventdata, handles); %update the image plot
hold on

refreshslide(hObject, eventdata, handles); %update the slider

%now we want to superimpose the profile extraction line on top of the image
%if deblurring is not yet done
if handles.donedeblur == 0
    
    handles.(['profile',num2str(j)]).probex = (((handles.PixelSpacing(1).*(handles.(['profile',num2str(j)]).cx - handles.(['profile',num2str(j)]).cx(1))).^2 + (handles.PixelSpacing(2).*(handles.(['profile',num2str(j)]).cy - handles.(['profile',num2str(j)]).cy(1))).^2).^(0.5));
    
    set(gcf,'CurrentAxes',handles.profileplot);
    [AX,H1,H2] = plotyy(handles.(['profile',num2str(j)]).probex,handles.(['profile',num2str(j)]).c,handles.(['profile',num2str(j)]).probex,handles.(['profile',num2str(j)]).c,'plot');
    set(get(AX(1),'Ylabel'),'String','Intensity (HU)');
    set(get(AX(2),'Ylabel'),'String','Segmentation (binary)');
    ylimits = get(AX(1),'YLim');
    yinc = (ylimits(2)-ylimits(1))/20;
    set(AX(1),'YTick',[ylimits(1):yinc:ylimits(2)]);
    set(AX(2),'YTick',[0,0.5,1]);
    xlabel('Probe Length (mm)');
    title('Intensity Profile');
    
    refreshview(hObject, eventdata, handles)
    hold on
    
    plot(handles.(['profile',num2str(j)]).cx,handles.(['profile',num2str(j)]).cy);
    
    hold off
    
end

if handles.donedeblur == 1
    
    t = handles.(['profile',num2str(j)]).probex;  %these are the x-coordinates of the profile
    y = handles.(['profile',num2str(j)]).c; %these are the intensity values of the profile
    
    if handles.n(:,j) == 1
        y_0 = handles.xsub(length(handles.xsub(:,j))-5,j); %the leading intensity
        y_f = handles.xsub(length(handles.xsub(:,j))-4,j);    %the trailing intensity
    else
        y_0 = handles.xsub(length(handles.xsub(:,j))-1,j); %the leading intensity
        y_f = handles.xsub(length(handles.xsub(:,j)),j);    %the trailing intensity
    end
    
    for i = 1:(handles.n(:,j) + 1) %Step locations
        X(i) = handles.xsub(i+handles.n(:,j),j);
    end
    
    Y = handles.inten(:,j);
    
    z(:,1) = t; %first column, x-values
    z(:,2) = y; %second columb, original CT values
    z(:,3) = y_0*rectpuls(t-((X(1)+min(t))/2),(X(1)-min(t)))+y_f*rectpuls(t-((max(t)+X(handles.n(:,j)+1))/2),(max(t)-X(handles.n(:,j)+1))); %third column, rectangular function estimate
    
    for i = 1:handles.n(:,j)
        z(:,3) = z(:,3) + Y(i)*rectpuls(t-((X(i+1)+X(i))/2),(X(i+1)-X(i))); %third column: HU values of double rectangle function
    end
    
    %Extract the deblur profile
    if strcmp(handles.(['profile',num2str(j)]).orientation,'xy') == 1
        deblur1 = handles.deblur1(:,:,handles.xycurrent);
    else if strcmp(handles.(['profile',num2str(j)]).orientation,'xz') == 1
            deblur1 = rot90(squeeze(handles.deblur1(handles.xzcurrent,:,:)),1);
        else if strcmp(handles.(['profile',num2str(j)]).orientation,'yz') == 1
                deblur1 = rot90(squeeze(handles.deblur1(:,handles.yzcurrent,:)),1);
            end
        end
    end
    
    %fourth column, the deblurred profile
    z(:,4) = improfile(deblur1, handles.(['profile',num2str(j)]).xi,handles.(['profile',num2str(j)]).yi,handles.numsamples,'bicubic');
    
    set(gcf,'CurrentAxes',handles.profileplot);
    plot(z(:,1),z(:,2:4))
    %axis([0 40 -1500 2000])
    % xlabel('Profile Distance (mm)');
    % ylabel('HU');
    % legend('CT Profile','Rectangular Profile Estimate','Deblurred')
    
    refreshview(hObject, eventdata, handles)
    hold on
    
    plot(handles.(['profile',num2str(j)]).cx,handles.(['profile',num2str(j)]).cy);
    
    hold off
    
end


function sampling_Callback(hObject, eventdata, handles)
% hObject    handle to sampling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.numsamples = str2double(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);

% Hints: get(hObject,'String') returns contents of sampling as text
%        str2double(get(hObject,'String')) returns contents of sampling as a double


% --- Executes during object creation, after setting all properties.
function sampling_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sampling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function imageplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Update handles structure
guidata(hObject, handles);

% Hint: place code in OpeningFcn to populate imageplot


% --- Executes during object creation, after setting all properties.
function profileplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to profileplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Update handles structure
guidata(hObject, handles);

% Hint: place code in OpeningFcn to populate profileplot


% --- Executes on button press in estimate.

%good candidate for a commandline function, lets move the code that
%calculates sigma for the psf to a new function
function estimate_Callback(hObject, eventdata, handles)
% hObject    handle to estimate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% t = distance vector, not to be confused with variable vector x
% y = CT intensities
% n = # of boxcar functions
% y_0 = lower tail constant intensity
% y_f = higher tail constant intensity
% A_CT = area of CT profile

assignin('base', 'review', handles);

clearvars -global t y n A_CT y_b e_1_i e_2_i nn h xsub min_cortthick min_trabthick angles

global t y n A_CT y_b e_1_i e_2_i nn min_cortthick min_trabthick inplanemode inplanesig angles

min_cortthick = handles.min_cortthick;
min_trabthick = handles.min_trabthick;

sig_max = handles.sigma_max;  %sigma bounds
sig_min = handles.sigma_min;
sig_ini = (sig_max+sig_min)/2;

cort_max = handles.cort_max;    %cortical intensity bounds
cort_min = handles.cort_min;
cort_ini = (cort_max + cort_min)/2;

trab_max = handles.trab_max;     %trabecular intensity bounds
trab_min = handles.trab_min;
trab_ini = (trab_max + trab_min)/2;

%start assembly of bounds vectors
x0 = [sig_ini];
lb = [sig_min];
ub = [sig_max];

%Do the slice and longitudinal profiles seperation here

if get(handles.selectinslice,'Value') == 1
    inplanemode = 1; %global to track if the estimation is being done for in-plane
    nn = handles.inplaneprofiles;
    handles.profilesubset = nn;   %number of profiles
    j = 1;
    for i = 1:handles.numprofiles
        if strcmp(handles.(['profile',num2str(i)]).orientation,'xy') == 1
            t(:,j) = handles.(['profile',num2str(i)]).probex;
            y(:,j) = handles.(['profile',num2str(i)]).c;
            n(:,j) = handles.n(:,i);
            edge1(:,j) = handles.(['profile',num2str(i)]).edge1;
            edge2(:,j) = handles.(['profile',num2str(i)]).edge2;
            orderxy(j) = i; %saves the master set location of the profile of the xy subset
            j = j + 1;
        end
    end
end

if get(handles.selectlongitudinal,'Value') == 1
    inplanemode = 0;
    inplanesig = str2num(get(handles.sigxy,'String'));
    nn = handles.longitudinalprofiles;
    handles.profilesubset = nn;   %number of profiles
    j = 1;
    for i = 1:handles.numprofiles
        if strcmp(handles.(['profile',num2str(i)]).orientation,'xy') == 0
            t(:,j) = handles.(['profile',num2str(i)]).probex;
            y(:,j) = handles.(['profile',num2str(i)]).c;
            n(:,j) = handles.n(:,i);
            
            %pixel size ratio must be taken into account for angle
            %calculation
            y_scale = handles.metadata.SliceThickness/handles.PixelSpacing(1);
            angles(:,j) =  atan((y_scale*abs((handles.(['profile',num2str(i)]).yi(2)-handles.(['profile',num2str(i)]).yi(1)))/abs((handles.(['profile',num2str(i)]).xi(2)-handles.(['profile',num2str(i)]).xi(1))))); %calculate cortical surface angle
            %angles(:,j) = -0.52;
            
            edge1(:,j) = handles.(['profile',num2str(i)]).edge1;      
            edge2(:,j) = handles.(['profile',num2str(i)]).edge2;
            orderz(j) = i; %saves the master set location of the profile of the xy subset
            j = j + 1;
        end
    end
end

for i = 1:j-1 %make an amalgamated vector
    
    %Edge settings based on segmentation
    x_edge_1 = edge1(:,i); %t(round(length(t)*0.4));%t(find(handles.(['profile',num2str(i)]).segc,1,'first'),i);
    x_edge_2 = edge2(:,i); %t(find(handles.(['profile',num2str(i)]).segc,1,'last'), i);
    w = (x_edge_2 - x_edge_1)/2;
    
    
    t_min = (x_edge_1 + x_edge_2)/2 - handles.slack_fac*w; %set lb and up, extend of the profile
    t_max = (x_edge_1 + x_edge_2)/2 + handles.slack_fac*w;
    
    %leading and trailing y-values/intensities averaging, and choosing the
    %"baseline"
    if t_min <= 0
        t_min = 0;
    end
    last = t(length(t(:,i)),i);
    if t_max >= last;
        t_max = last;
    end
    e_1_i(:,i) = searchclosest(t(:,i),t_min);
    y_0_v = y(1:searchclosest(t(:,i),x_edge_1),i);
    y_0_ini = mean(y_0_v);
    e_2_i(:,i) = searchclosest(t(:,i),t_max);
    y_f_v = y(searchclosest(t(:,i),x_edge_2):end,i);
    y_f_ini = mean(y_f_v);
    
    if y_0_ini < y_f_ini %set baseline/reference y
        y_b(:,i) = y_0_ini;
    else
        y_b(:,i) = y_f_ini;
    end
    
    A_CT(:,i) = trapz(t(e_1_i(:,i):e_2_i(:,i),i),y(e_1_i(:,i):e_2_i(:,i),i)-y_b(:,i));
    
    if n(:,i) == 3
        t_div = (t_max-t_min)/n(:,i);
        x0 = [x0   cort_ini    trab_ini    cort_ini   t_min    t_min     t_min+t_div*2       t_max   y_0_ini   y_f_ini]   ;   %solution seed
        lb = [lb   cort_min    trab_min    cort_min   t_min    t_min     t_min      t_min     y_0_ini-10*std(y_0_v)   y_f_ini-10*std(y_0_v)   ];
        ub = [ub   cort_max    trab_max    cort_max   t_max    t_max     t_max      t_max     y_0_ini+10*std(y_0_v)  y_f_ini+10*std(y_0_v)  ];
    end
    
    if n(:,i) == 1
        x0 = [x0 cort_ini    t_min       t_max     y_0_ini   y_f_ini]   ;   %solution seed
        lb = [lb cort_min    t_min       t_min     y_0_ini-10*std(y_0_v)   y_f_ini-10*std(y_f_v)   ];
        ub = [ub cort_max    t_max       t_max     y_0_ini+10*std(y_0_v)   y_f_ini+10*std(y_f_v)   ];
    end
    
    A = [];
    b = [];
    
    Aeq = [];
    beq = [];
    
    
end

clear i

%a brute force way of making the cell array necessary for the plotting component of the optim function
c = {};
c(1) = {@(x,optimValues,state)outfun(x,optimValues,state,1)};
c(2) = {@(x,optimValues,state)outfun(x,optimValues,state,2)};
c(3) = {@(x,optimValues,state)outfun(x,optimValues,state,3)};
c(4) = {@(x,optimValues,state)outfun(x,optimValues,state,4)};
c(5) = {@(x,optimValues,state)outfun(x,optimValues,state,5)};
c(6) = {@(x,optimValues,state)outfun(x,optimValues,state,6)};
c(7) = {@(x,optimValues,state)outfun(x,optimValues,state,7)};
c(8) = {@(x,optimValues,state)outfun(x,optimValues,state,8)};
c(9) = {@(x,optimValues,state)outfun(x,optimValues,state,9)};
c(10) = {@(x,optimValues,state)outfun(x,optimValues,state,10)};
c(11) = {@(x,optimValues,state)outfun(x,optimValues,state,11)};
c(12) = {@(x,optimValues,state)outfun(x,optimValues,state,12)};
c(13) = {@(x,optimValues,state)outfun(x,optimValues,state,13)};
c(14) = {@(x,optimValues,state)outfun(x,optimValues,state,14)};
c(15) = {@(x,optimValues,state)outfun(x,optimValues,state,15)};

handles.plotarray = c(1:handles.profilesubset);

options = optimset('Algorithm','interior-point','display','on', 'GradConstr','off','DerivativeCheck','off','TolFun',1e-10*sum(A_CT),'PlotFcns', handles.plotarray, 'MaxFunEvals',40000,'UseParallel','always' )%
[points , fval] = fmincon(@objective,x0,A,b,Aeq,beq,lb,ub,@constraints,options);  %objective function value

if get(handles.selectinslice,'Value') == 1
    set(handles.sigxy,'String',num2str(points(1)));
    inplanesig = points(1);
end

if get(handles.selectlongitudinal,'Value') == 1
    inplanesig = str2num(get(handles.sigxy,'String'));
    set(handles.sigz,'String',num2str(points(1)));
end

assignin('base', 'result', points);

%%%%%%%%%%%%%%%%%% translate results
k = 2;

for j = 1:nn %Profile incrementation
    
    thesig = points(1);
    
    handles.thesig = thesig;
    
    if n(:,j) == 3
        xsub(:,j) = points(k:k+8);
    else
        xsub(:,j) = [points(k:k+4) 0 0 0 0];
    end
    
    for i = 1:n(:,j) %Intensity values
        inten(i,j) = xsub(i,j);
    end
    
    for i = 1:(n(:,j)) %Step locations
        thick(i,j) = xsub(i+n(:,j)+1,j) - xsub(i+n(:,j),j);
    end
    
    if n(:,j) == 3  %advance k to next profile in x vector
        k = k + 9;
    else
        k = k + 5;
    end
    
end

%order everything back into a matrix
if get(handles.selectinslice,'Value') == 1
    handles.xsub = [];
    handles.thick = [];
    handles.inten = [];
    for j = 1:length(orderxy)
        handles.xsub(:,orderxy(j))= xsub(:,j);
        handles.thick(:,orderxy(j)) = thick(:,j);
        handles.inten(:,orderxy(j)) = inten(:,j);
    end
end

if get(handles.selectlongitudinal,'Value') == 1
    for j = 1:length(orderz)
        handles.xsub(:,orderz(j))= xsub(:,j);
        handles.thick(:,orderz(j)) = thick(:,j);
        handles.inten(:,orderz(j)) = inten(:,j);
    end
end

set(handles.table, 'Data', [handles.inten handles.thick]);

guidata(hObject, handles);



function sigxy_Callback(hObject, eventdata, handles)
% hObject    handle to sigxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sigxy as text
%        str2double(get(hObject,'String')) returns contents of sigxy as a double

guidata(hObject, handles)


% --- Executes during object creation, after setting all properties.
function sigxy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes when entered data in editable cell(s) in table.
function table_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to table (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in view_deblur1.
function view_deblur1_Callback(hObject, eventdata, handles)
% hObject    handle to view_deblur1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% for i = 1:nn
%     t(:,i) = handles.(['profile',num2str(i)]).probex;
%     y(:,i) = handles.(['profile',num2str(i)]).c;
% end
%
% shift = round(length(t)/2);
% PSF = (((2*pi*thesig^2)^-0.5)*exp(-1*(((-t(shift)+t).^2)/(2*thesig^2)))); %Normalized Gaussian kernel
set(gcf,'CurrentAxes',handles.imageplot);
imshow(handles.deblur1view, [handles.winlow handles.winhi]);

% --- Executes on button press in deblur.
function deblur_Callback(hObject, eventdata, handles)
% hObject    handle to deblur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


 

%3D PSF
mu = [0 0 0];

sigxy = str2num(get(handles.sigxy,'String')); % dimensions are in mm
sigz = str2num(get(handles.sigz,'String'));

if isempty(sigxy) ~= 1 && isempty(sigz) ~= 1

Sigma = [sigxy^2 0 0; 0 sigxy^2 0; 0 0 sigz^2]; %covariance matrix

range = handles.psf_extend; %max, min range of PSF grid in mm

% x1_0 = -range:handles.metadata.PixelSpacing(1)/4:range;
% x2_0 = -range:handles.metadata.PixelSpacing(2)/4:range;
% x3_0 = -range:handles.metadata.SliceThickness/4:range;
% [X1_0,X2_0,X3_0] = meshgrid(x1_0,x2_0,x3_0);

%PSF grid spacing
spacingx = handles.metadata.PixelSpacing(1);
spacingy = handles.metadata.PixelSpacing(2);
spacingz = handles.metadata.SliceThickness;

x1 = -range:spacingx:range;
x2 = -range:spacingy:range;
x3 = -range:spacingz:range;
[X1,X2,X3] = meshgrid(x1,x2,x3);

dimscale = (spacingx*spacingy*spacingz); %mm^3

%the elusive 3-D PSF, in all its glory
F = mvnpdf([X1(:) X2(:) X3(:)], mu, Sigma);
handles.PSF = reshape(F,length(x1),length(x2),length(x3)).*dimscale; %The PSF gereated in mm domain must be rescaled to be normalized in pixel domain

end

javaaddpath 'C:\Program Files (x86)\ImageJ\ij.jar'  %'C:\Program Files\MATLAB\R2012a\java\ij.jar'
javaaddpath 'C:\Program Files\MATLAB\R2012a\java\mij.jar'
MIJ.start('C:\Program Files (x86)\ImageJ')

MIJ.createImage(handles.PSF)
MIJ.createImage(handles.CT)

guidata(hObject, handles);



% --- Executes on button press in weight1.
function weight1_Callback(hObject, eventdata, handles)
% hObject    handle to weight1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% handles.weight1 = handles.CT(:,:,1);%edge(handles.CT(:,:,1), 'sobel',handles.thresh1);
% handles.weight2 = squeeze(handles.CT(:,1,:));%edge(squeeze(handles.CT(:,1,:)), 'sobel',handles.thresh1);
% handles.weight3 = squeeze(handles.CT(1,:,:));%edge(squeeze(handles.CT(1,:,:)), 'sobel',handles.thresh1);
%
% handles.weight1 = handles.weight1.*0; %handles.CT(:,:,1);%edge(handles.CT(:,:,1), 'sobel',handles.thresh1);
% handles.weight2 = handles.weight2.*0;  %squeeze(handles.CT(:,1,:));%edge(squeeze(handles.CT(:,1,:)), 'sobel',handles.thresh1);
% handles.weight3 = handles.weight3.*0; %squeeze(handles.CT(1,:,:));%edge(squeeze(handles.CT(1,:,:)), 'sobel',handles.thresh1);
%
% se = strel('disk',handles.dil1);
% se2 = strel('disk',3);

el = 6;
handles.tweight = handles.CT.*0;
handles.tweight(:,:,[1:el end-el:end]) = 1.0;
handles.tweight(:,[1:el end-el:end],:) = 1.0;
handles.tweight([1:el end-el:end],:,:) = 1.0;

% handles.weight1([1:3 end-[0:2]],:) = 1;
% handles.weight1(:,[1:3 end-[0:2]]) = 1;
% handles.weight2([1:3 end-[0:2]],:) = 1;
% handles.weight2(:,[1:3 end-[0:2]]) = 1;
% handles.weight3([1:3 end-[0:2]],:) = 1;
% handles.weight3(:,[1:3 end-[0:2]]) = 1;

% handles.weight1 = double(imdilate(handles.weight1,se));
% handles.weight2 = double(imdilate(handles.weight2,se));
% handles.weight3 = double(imdilate(handles.weight3,se));

%handles.weight1 = 1 - (handles.weight1);% + handles.weight2); %im2double(handles.segment).*255;
%handles.weight2 = 1 - (handles.weight2);
%handles.weight3 = 1 - (handles.weight3);

% for i = 1:handles.numzslices
%     handles.tweight1(:,:,i) = handles.weight1;
% end
%
% for i = 1:handles.numxyslices
%     handles.tweight2(:,i,:) = handles.weight2;
% end
%
% for i = 1:handles.numxyslices
%     handles.tweight3(i,:,:) = handles.weight3;
% end

handles.tweight = double(1 - (handles.tweight));

set(gcf,'CurrentAxes',handles.imageplot);    %set on to which axes to plot
imshow(squeeze(handles.tweight(75,:,:)));

guidata(hObject, handles);


% --- Executes on button press in deblur2.
function deblur2_Callback(hObject, eventdata, handles)
% hObject    handle to deblur2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in viewweight.
function viewweight_Callback(hObject, eventdata, handles)
% hObject    handle to viewweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

E = handles.CT;
I = 1-handles.weight1;

set(gcf,'CurrentAxes',handles.imageplot);
imshow(E, [handles.winlow handles.winhi]);

green = cat(3, zeros(size(E)), ones(size(E)), zeros(size(E)));
hold on
h = imshow(green);
hold off

set(h, 'AlphaData', I)


% green = cat(3, zeros(size(handles.CT)), ones(size(handles.CT)), zeros(size(handles.CT)));
%
% set(gcf,'CurrentAxes',handles.imageplot);
% imshow(handles.CT, [handles.winlow handles.winhi]);
%
% set(h, 'AlphaData', alpha_data);
% imshow(handles.weight1,[0 1]);




% --- Executes on button press in view_deblur2.
function view_deblur2_Callback(hObject, eventdata, handles)
% hObject    handle to view_deblur2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function totat_iterations_Callback(hObject, eventdata, handles)
% hObject    handle to totat_iterations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of totat_iterations as text
%        str2double(get(hObject,'String')) returns contents of totat_iterations as a double

handles.iter1 = str2double(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function totat_iterations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to totat_iterations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edgedilation_Callback(hObject, eventdata, handles)
% hObject    handle to edgedilation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edgedilation as text
%        str2double(get(hObject,'String')) returns contents of edgedilation as a double

handles.dil1 = str2double(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edgedilation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edgedilation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
handles.thresh1 = str2double(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ntype_Callback(hObject, eventdata, handles)
% hObject    handle to ntype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ntype as text
%        str2double(get(hObject,'String')) returns contents of ntype as a double

handles.type = str2double(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ntype_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ntype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

set(gcf,'CurrentAxes',handles.imageplot);    %set on to which axes to plot

if get(handles.xy,'Value') == 1   %XY orientation is selected
    handles.xycurrent = round(get(hObject,'Value')); %
    set(handles.slicenumbox,'String',num2str(handles.xycurrent)); %set box label of slider current position
    guidata(hObject, handles);
    refreshview(hObject, eventdata, handles)
end

if get(handles.xz,'Value') == 1   %XZ orientation is selected
    handles.xzcurrent = round(get(hObject,'Value')); %
    I = squeeze(handles.CT(handles.xzcurrent,:,:));
    set(handles.slicenumbox,'String',num2str(handles.xzcurrent)); %set box label of slider current position
    guidata(hObject, handles);
    refreshview(hObject, eventdata, handles)
end

if get(handles.yz,'Value') == 1   %YZ orientation is selected
    handles.yzcurrent = round(get(hObject,'Value')); %
    I = squeeze(handles.CT(:,handles.yzcurrent,:));
    set(handles.slicenumbox,'String',num2str(handles.yzcurrent)); %set box label of slider current position
    guidata(hObject, handles);
    refreshview(hObject, eventdata, handles)
end

%str2double(get(handles.slideplace,'String')

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function slicenumbox_Callback(hObject, eventdata, handles)
% hObject    handle to slicenumbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of slicenumbox as text
%        str2double(get(hObject,'String')) returns contents of slicenumbox as a double


% --- Executes during object creation, after setting all properties.
function slicenumbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slicenumbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in xy.
function xy_Callback(hObject, eventdata, handles)
% hObject    handle to xy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of xy

if get(hObject, 'Value')==1
    set(handles.xy,'Value',1)
    set(handles.xz,'Value',0)
    set(handles.yz,'Value',0)
end

%imshow(handles.CT(:,:,handles.xycurrent),[handles.winlow handles.winhi]);

refreshview(hObject, eventdata, handles)

%set slider
refreshslide(hObject, eventdata, handles)

guidata(hObject,handles);


% --- Executes on button press in xz.
function xz_Callback(hObject, eventdata, handles)
% hObject    handle to xz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of xz

if get(hObject, 'Value')==1
    set(handles.xy,'Value',0)
    set(handles.xz,'Value',1)
    set(handles.yz,'Value',0)
end

refreshview(hObject, eventdata, handles)

refreshslide(hObject, eventdata, handles)

guidata(hObject,handles);

% --- Executes on button press in yz.
function yz_Callback(hObject, eventdata, handles)
% hObject    handle to yz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of yz

if get(hObject, 'Value')==1
    set(handles.xy,'Value',0)
    set(handles.xz,'Value',0)
    set(handles.yz,'Value',1)
end


refreshview(hObject, eventdata, handles)

%set slider range
refreshslide(hObject, eventdata, handles)

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function maxslicenum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxslicenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ctview.
function ctview_Callback(hObject, eventdata, handles)
% hObject    handle to ctview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ctview

if get(hObject, 'Value')==1
    set(handles.ctview,'Value',1)
    set(handles.deblur1view,'Value',0)
end

refreshview(hObject, eventdata, handles)

guidata(hObject,handles);

% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton7


% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton8


% --- Executes on button press in deblur1view.
function deblur1view_Callback(hObject, eventdata, handles)
% hObject    handle to deblur1view (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of segview
if get(hObject, 'Value')==1
    set(handles.ctview,'Value',0)
    set(handles.deblur1view,'Value',1)
end

refreshview(hObject, eventdata, handles)

guidata(hObject,handles);


% --- Executes on button press in segview.
function segview_Callback(hObject, eventdata, handles)
% hObject    handle to segview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of segview
if get(hObject, 'Value')==1
    set(handles.ctview,'Value',0)
    set(handles.segview,'Value',1)
    set(handles.deblur1view,'Value',0)
end

refreshview(hObject, eventdata, handles)

guidata(hObject,handles);

function refreshview(hObject, eventdata, handles)
%this function updates the Image View
% - check image set
% - check orientation
% the show image

set(gcf,'CurrentAxes',handles.imageplot);    %set on to which axes to plot
hold off

% if Original CT view is selected
if get(handles.ctview,'Value') == 1
    if get(handles.xy,'Value') == 1   %XY orientation is selected
        I = handles.CT(:,:,handles.xycurrent);
        set(handles.slicenumbox,'String',num2str(handles.xycurrent)); %set box label of slider current position
        imshow(I,[handles.winlow handles.winhi]);
    end
    
    if get(handles.xz,'Value') == 1   %XZ orientation is selected
        I = rot90(squeeze(handles.CT(handles.xzcurrent,:,:)),1);
        set(handles.slicenumbox,'String',num2str(handles.xzcurrent)); %set box label of slider current position
        imshow(I,[handles.winlow handles.winhi]);
        daspect([handles.metadata.SliceThickness/handles.PixelSpacing(1) 1 1]);
        
    end
    
    if get(handles.yz,'Value') == 1   %YZ orientation is selected
        I = rot90(squeeze(handles.CT(:,handles.yzcurrent,:)),1);
        set(handles.slicenumbox,'String',num2str(handles.yzcurrent)); %set box label of slider current position
        imshow(I,[handles.winlow handles.winhi]);
        daspect([handles.metadata.SliceThickness/handles.PixelSpacing(1) 1 1]);
    end
end

% if deblur1view view is selected
if get(handles.deblur1view,'Value') == 1
    if get(handles.xy,'Value') == 1   %XY orientation is selected
        I = handles.deblur1(:,:,handles.xycurrent);
        set(handles.slicenumbox,'String',num2str(handles.xycurrent)); %set box label of slider current position
        imshow(I,[handles.winlow handles.winhi]);
    end
    
    if get(handles.xz,'Value') == 1   %XZ orientation is selected
        I = rot90(squeeze(handles.deblur1(handles.xzcurrent,:,:)),1);
        set(handles.slicenumbox,'String',num2str(handles.xzcurrent)); %set box label of slider current position
        imshow(I,[handles.winlow handles.winhi]);
        daspect([handles.metadata.SliceThickness/handles.PixelSpacing(1) 1 1]);
    end
    
    if get(handles.yz,'Value') == 1   %YZ orientation is selected
        I = rot90(squeeze(handles.deblur1(:,handles.yzcurrent,:)),1);
        set(handles.slicenumbox,'String',num2str(handles.yzcurrent)); %set box label of slider current position
        imshow(I,[handles.winlow handles.winhi]);
        daspect([handles.metadata.SliceThickness/handles.PixelSpacing(1) 1 1]);
    end
end

guidata(hObject,handles);



function refreshslide(hObject, eventdata, handles)
%This function updates the slider

set(handles.slider,'Min',1); %set slider min value

if get(handles.xy,'Value') == 1
    set(handles.slicenumbox,'String',num2str(handles.xycurrent));%set box label of slider current position
    set(handles.maxslicenum,'String',num2str(handles.numzslices)); %max label
    set(handles.slider,'Max',handles.numzslices); %set slider max value
    set(handles.slider,'Value',handles.xycurrent);%set slider to last viewed slice
    set(handles.slider,'SliderStep',[1/(handles.numzslices-1) 1/(handles.numzslices-1)]);
end

if get(handles.xz,'Value') == 1
    set(handles.slicenumbox,'String',num2str(handles.xzcurrent)); %set box label of slider current position
    set(handles.maxslicenum,'String',num2str(handles.numyslices)); %max label
    set(handles.slider,'Max',handles.numyslices); %set slider max value
    set(handles.slider,'Value',handles.xzcurrent);%set slider current position
    set(handles.slider,'SliderStep',[1/(handles.numyslices-1) 1/(handles.numyslices-1)]);
end

if get(handles.yz,'Value') == 1
    set(handles.slicenumbox,'String',num2str(handles.yzcurrent)); %set box label of slider current position
    set(handles.maxslicenum,'String',num2str(handles.numxslices)); %max label
    set(handles.slider,'Max',handles.numxslices); %set slider max value
    set(handles.slider,'Value',handles.yzcurrent);%set slider current position
    set(handles.slider,'SliderStep',[1/(handles.numxslices-1) 1/(handles.numxslices-1)]);
end

guidata(hObject,handles);



function sigz_Callback(hObject, eventdata, handles)
% hObject    handle to sigz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sigz as text
%        str2double(get(hObject,'String')) returns contents of sigz as a double


% --- Executes during object creation, after setting all properties.
function sigz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectinslice.
function selectinslice_Callback(hObject, eventdata, handles)
% hObject    handle to selectinslice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of selectinslice

if get(hObject, 'Value')==1
    set(handles.selectinslice,'Value',1);
    set(handles.selectlongitudinal,'Value',0);
end

handles.plotarray = {};

guidata(hObject,handles);



% --- Executes on button press in selectlongitudinal.
function selectlongitudinal_Callback(hObject, eventdata, handles)
% hObject    handle to selectlongitudinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of selectlongitudinal

if get(hObject, 'Value')==1
    set(handles.selectinslice,'Value',0);
    set(handles.selectlongitudinal,'Value',1);
end

handles.plotarray = {};

guidata(hObject,handles);


% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function cellarrayforplot(hObject,handles)
% a crude way of making a cell array with function statements that go into
% the optimization solver in order to plot all the profile fittings
c = {};
c(1) = {@(x,optimValues,state)outfun(x,optimValues,state,1)};
c(2) = {@(x,optimValues,state)outfun(x,optimValues,state,2)};
c(3) = {@(x,optimValues,state)outfun(x,optimValues,state,3)};
c(4) = {@(x,optimValues,state)outfun(x,optimValues,state,4)};
c(5) = {@(x,optimValues,state)outfun(x,optimValues,state,5)};
c(6) = {@(x,optimValues,state)outfun(x,optimValues,state,6)};
c(7) = {@(x,optimValues,state)outfun(x,optimValues,state,7)};
c(8) = {@(x,optimValues,state)outfun(x,optimValues,state,8)};
c(9) = {@(x,optimValues,state)outfun(x,optimValues,state,9)};
c(10) = {@(x,optimValues,state)outfun(x,optimValues,state,10)};
c(11) = {@(x,optimValues,state)outfun(x,optimValues,state,11)};
c(12) = {@(x,optimValues,state)outfun(x,optimValues,state,12)};
c(13) = {@(x,optimValues,state)outfun(x,optimValues,state,13)};
c(14) = {@(x,optimValues,state)outfun(x,optimValues,state,14)};
c(15) = {@(x,optimValues,state)outfun(x,optimValues,state,15)};

handles.plotarray = c(1:handles.profilesubset);

guidata(hObject,handles);



function sigmamin_Callback(hObject, eventdata, handles)
% hObject    handle to sigmamin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sigmamin as text
%        str2double(get(hObject,'String')) returns contents of sigmamin as a double

handles.sigma_min = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function sigmamin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigmamin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sigmamax_Callback(hObject, eventdata, handles)
% hObject    handle to sigmamax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sigmamax as text
%        str2double(get(hObject,'String')) returns contents of sigmamax as a double

handles.sigma_max = str2double(get(hObject,'String'));

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function sigmamax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigmamax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sigmainitial_Callback(hObject, eventdata, handles)
% hObject    handle to sigmainitial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sigmainitial as text
%        str2double(get(hObject,'String')) returns contents of sigmainitial as a double


% --- Executes during object creation, after setting all properties.
function sigmainitial_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigmainitial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cortmin_Callback(hObject, eventdata, handles)
% hObject    handle to cortmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cortmin as text
%        str2double(get(hObject,'String')) returns contents of cortmin as a double

handles.cort_min = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function cortmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cortmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cortmax_Callback(hObject, eventdata, handles)
% hObject    handle to cortmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cortmax as text
%        str2double(get(hObject,'String')) returns contents of cortmax as a double

handles.cort_max = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function cortmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cortmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cortini_Callback(hObject, eventdata, handles)
% hObject    handle to cortini (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cortini as text
%        str2double(get(hObject,'String')) returns contents of cortini as a double


% --- Executes during object creation, after setting all properties.
function cortini_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cortini (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function trabmin_Callback(hObject, eventdata, handles)
% hObject    handle to trabmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trabmin as text
%        str2double(get(hObject,'String')) returns contents of trabmin as a double

handles.trab_min = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function trabmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trabmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function trabmax_Callback(hObject, eventdata, handles)
% hObject    handle to trabmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trabmax as text
%        str2double(get(hObject,'String')) returns contents of trabmax as a double

handles.trab_max = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function trabmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trabmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function trabini_Callback(hObject, eventdata, handles)
% hObject    handle to trabini (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trabini as text
%        str2double(get(hObject,'String')) returns contents of trabini as a double


% --- Executes during object creation, after setting all properties.
function trabini_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trabini (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function slackfac_Callback(hObject, eventdata, handles)
% hObject    handle to slackfac (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of slackfac as text
%        str2double(get(hObject,'String')) returns contents of slackfac as a double

handles.slack_fac = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function slackfac_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slackfac (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function mincortthick_Callback(hObject, eventdata, handles)
% hObject    handle to mincortthick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mincortthick as text
%        str2double(get(hObject,'String')) returns contents of mincortthick as a double

handles.min_cortthick = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function mincortthick_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mincortthick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function mintrabthick_Callback(hObject, eventdata, handles)
% hObject    handle to mintrabthick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mintrabthick as text
%        str2double(get(hObject,'String')) returns contents of mintrabthick as a double

handles.min_trabthick = str2double(get(hObject,'String'));

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function mintrabthick_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mintrabthick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in weighton.
function weighton_Callback(hObject, eventdata, handles)
% hObject    handle to weighton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of weighton

if get(hObject,'Value')== 1
    handles.weight_on = 1;
else if get(hObject,'Value')== 0
        handles.weight_on = 0;
    end
end

refreshview(hObject, eventdata, handles);

guidata(hObject,handles);



function iteration_interval_Callback(hObject, eventdata, handles)
% hObject    handle to iteration_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of iteration_interval as text
%        str2double(get(hObject,'String')) returns contents of iteration_interval as a double

handles.iterinterval1 = str2double(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function iteration_interval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to iteration_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in continuedeblur.
function continuedeblur_Callback(hObject, eventdata, handles)
% hObject    handle to continuedeblur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%deblur it damn it
if handles.iter1 - handles.itersdone >= handles.iterinterval1
    A = deconvlucy(handles.tdeblur, handles.PSF, handles.iterinterval1+handles.itersdone, handles.damp, handles.tweight, [], handles.s);
else
    A = deconvlucy(handles.tdeblur, handles.PSF, handles.iter1, handles.damp, handles.tweight, [], handles.s);
end
%handles.tdeblur{1} contains I, the original image.
%handles.tdeblur{2} contains the result of the last iteration.
%handles.tdeblur{3} contains the result of the next-to-last iteration.
%handles.tdeblur{4} is an array generated by the iterative algorithm.

handles.itersdone = handles.itersdone + handles.iterinterval1;

set(handles.itertext,'String',[num2str(handles.itersdone) ' of ' num2str(handles.iter1) ' iterations done']); %update progress text

handles.tdeblur = A;

%rescale
if size(A{2}) ~= size(handles.CT)
    handles.deblur1 = resize(A{2},size(handles.CT),'cubic') - handles.dcshift;
else
    handles.deblur1 = A{2} - handles.dcshift;
end

if get(hObject, 'Value')==1
    set(handles.ctview,'Value',0)
    set(handles.segview,'Value',0)
    set(handles.deblur1view,'Value',1)
end

refreshview(hObject, eventdata, handles)

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function itertext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itertext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function dcshift_Callback(hObject, eventdata, handles)
% hObject    handle to dcshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dcshift as text
%        str2double(get(hObject,'String')) returns contents of dcshift as a double

handles.dcshift = str2double(get(hObject,'String')); %this DC shift is a crude way to get around deconvolution clipping negative values to zero

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function dcshift_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dcshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function supersampling_Callback(hObject, eventdata, handles)
% hObject    handle to supersampling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of supersampling as text
%        str2double(get(hObject,'String')) returns contents of supersampling as a double

handles.s = str2double(get(hObject,'String')); %#ok<ST2NM> %super sampling factor

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function supersampling_CreateFcn(hObject, eventdata, handles)
% hObject    handle to supersampling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectedge1.
function selectedge1_Callback(hObject, eventdata, handles)
% hObject    handle to selectedge1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

i = handles.currentprofilenum; %index

set(gcf,'CurrentAxes',handles.profileplot);
handles.(['profile',num2str(i)]).edge1 = getpts

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in selectedge2.
function selectedge2_Callback(hObject, eventdata, handles)
% hObject    handle to selectedge2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

i = handles.currentprofilenum; %index

set(gcf,'CurrentAxes',handles.profileplot);
handles.(['profile',num2str(i)]).edge2 = getpts

% Update handles structure
guidata(hObject, handles);



function edit29_Callback(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit29 as text
%        str2double(get(hObject,'String')) returns contents of edit29 as a double

handles.damp = single(str2double(get(hObject,'String'))); %#ok<ST2NM> %super sampling factor

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit29_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function psfsize_Callback(hObject, eventdata, handles)
% hObject    handle to psfsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of psfsize as text
%        str2double(get(hObject,'String')) returns contents of psfsize as a double

handles.psf_extend = str2double(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function psfsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to psfsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in import.
function import_Callback(hObject, eventdata, handles)
% hObject    handle to import (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

prompt = {'Enter image name:'};
dlg_title = 'Input for naming';
num_lines = 1;
def = {''};
name = inputdlg(prompt,dlg_title,num_lines,def);

handles.deblur1 = MIJ.getImage(name{1});

refreshview(hObject, eventdata, handles)

handles.donedeblur = 1;

assignin('base', 'review', handles);

%Update handles structure
guidata(hObject, handles);


% --- Executes on button press in write.
function write_Callback(hObject, eventdata, handles)
% hObject    handle to write (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file,path] = uiputfile('*.*','Save DICOM as') %get file name and location where to save
num_slices = length(squeeze(handles.deblur1(1,1,:))); %get number of slices that must be written
num_digits = numel(num2str([num_slices]));

cd(path)

if num_digits < 3
    for i = 1:9
        I = dicomread(handles.filename{i});
        info = dicominfo(handles.filename{i});
        info.SOPClassUID = info.MediaStorageSOPClassUID;
        dicomwrite(int16(handles.deblur1(:,:,i)),[file '0' num2str(i) '.dcm'],info,'CreateMode','Copy');%'WritePrivate',true
    end
    
    for i = 10:num_slices
        I = dicomread(handles.filename{i});
        info = dicominfo(handles.filename{i});
        info.SOPClassUID = info.MediaStorageSOPClassUID;
        dicomwrite(int16(handles.deblur1(:,:,i)),[file num2str(i) '.dcm'],info,'CreateMode','Copy');%'WritePrivate',true
    end
end

if num_digits == 3
    for i = 1:9
        I = dicomread(handles.filename{i});
        info = dicominfo(handles.filename{i});
        info.SOPClassUID = info.MediaStorageSOPClassUID;
        dicomwrite(int16(handles.deblur1(:,:,i)),[file '00' num2str(i) '.dcm'],info,'CreateMode','Copy');%'WritePrivate',true
    end
    
    for i = 10:99
        I = dicomread(handles.filename{i});
        info = dicominfo(handles.filename{i});
        info.SOPClassUID = info.MediaStorageSOPClassUID;
        dicomwrite(int16(handles.deblur1(:,:,i)),[file '0' num2str(i) '.dcm'],info,'CreateMode','Copy');%'WritePrivate',true
    end
    
    for i = 100:num_slices
        I = dicomread(handles.filename{i});
        info = dicominfo(handles.filename{i});
        info.SOPClassUID = info.MediaStorageSOPClassUID;
        dicomwrite(int16(handles.deblur1(:,:,i)),[file num2str(i) '.dcm'],info,'CreateMode','Copy');%'WritePrivate',true
    end
       
end

cd('C:\Users\SuperOBL\Documents\My Box Files\Deconv')


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[file,path] = uiputfile('*.*','Save data as'); %get file name and location where to save

cd(path)

save(file,'handles');

addpath(path)

cd('C:\Users\SuperOBL\Documents\My Box Files\Deconv')


% --- Executes on button press in load.
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clear handles

[file, path, filterindex] = uigetfile( ...
    {  '*.dcm','DICOM files (*.dcm)';
    '*.*',  'All Files (*.*)'}, ...
    'Pick a file', ...
    'MultiSelect', 'off');

cd(path)

load(file)

%set slider range for first-time opening of image stack
set(handles.maxslicenum,'String',num2str(handles.numzslices)); %max label
set(handles.slider,'Min',1); %set slider min value
set(handles.slider,'Max',handles.numzslices); %set slider max value
set(handles.slicenumbox,'String',num2str(handles.xycurrent)); %set box label of slider current position
set(handles.slider,'Value',handles.xycurrent);%set slider current position to halfway
set(handles.slider,'SliderStep',[1/(handles.numzslices-1) 1/(handles.numzslices-1)]);

%print image size info
set(handles.imagesizeinfo,'String',['Image volume size (X*Y*Z): ' num2str(handles.numxslices) '*' num2str(handles.numyslices) '*' num2str(handles.numzslices)]);
%print image resolution info
set(handles.imageresinfo,'String',['Voxel size (X*Y*Z): ' num2str(handles.metadata.PixelSpacing(1)) '*' num2str(handles.metadata.PixelSpacing(2)) '*' num2str(handles.metadata.SliceThickness) ' mm']);

set(gcf,'CurrentAxes',handles.imageplot);    %set on to which axes to plot
imshow(handles.CT(:,:,str2double(get(handles.slicenumbox,'String'))),[handles.winlow handles.winhi]);

%initialize orientation buttons
set(handles.xy,'Value',1)
set(handles.xz,'Value',0)
set(handles.yz,'Value',0)

%initialize image set selection
set(handles.ctview,'Value',1)

%Update handles structure
guidata(hObject, handles);
