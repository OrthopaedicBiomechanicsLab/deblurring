function [ NewFileName ] = changeFileNames( preceedingSearchText, searchTextToBeReplaced, proceedingSearchText, replacementText, subdirFlag)
%Change all file names matching searchText.  An asterisk is expected in
%searchText and that is the part that is changed.  A search for all files
%matching searchText is performed and all file names are changed, the part 
%   Detailed explanation goes here
    fileList = dir_matlab([preceedingSearchText searchTextToBeReplaced proceedingSearchText],subdirFlag);
    for file = transpose(fileList)
        stringLocation = strfind(file,searchTextToBeReplaced)
        newFileName = [file(1:stringLocation-1) replacementText file(stringLocation+length(searchTextToBeReplaced):end)]
        rename file newFileName
    end   
end

