function  writeDicomStack(dicomStackPath,fileNamePrefix,extensionString,imageStack,infoStack)
%function to write dicom volume as a series of 2d images to file.  Note
%dicom header information is in the format that is recovered from the
%native matlab dicomwrite and dicomread files.  It is convienient to use
%this function with readDicomStack

%dicomStackPath - String, the location where files will be written.  The
%     string should end in a '\'
%     ie C:\Mike\SkullCapData\ID5685\
%fileNamePrefix - a prefix for the file, ie 'im' for file name im0001.dcm
%extensionString - the string to be appended to file names at the end of
%     the file name.  It should begin with a '.' otherwise one wont be
%     added
%imageStack - the image stack in matlab.  This should be a 3d array in
%     matlab with each element in the array representing a voxel in the image
%     volume
%infoStack - a stack of dicom info informatin for each z location in
%     imageStack.  The array will be used to write the header information for
%     each 2d dicom image file.
%Note the dicom headers may contain a tag indicating file name.  This won't
%be updated by this function.  


imageStackSize = size(imageStack,3);
infoStackSize = size(infoStack,1);
assert(imageStackSize==infoStackSize);

%would be a good idea to check if fileNamePrefix ends with / or \ and add
%them if they don't before using the variable

fieldSize = 4; %it might be a good idea to let field size be determined by the imageStackSize


for sliceIndex = 1:1:imageStackSize
    dicomwrite(imageStack(:,:,sliceIndex),[dicomStackPath,fileNamePrefix,sprintf( ['%0',num2str(fieldSize),'d'],sliceIndex),extensionString],infoStack{sliceIndex},'ObjectType','CT Image Storage'); %'CreateMode', 'copy',
end