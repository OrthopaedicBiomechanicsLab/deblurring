function [stack, info_stack] = readDicomStack(dicomStackPath,sortBy)
%function reads all dicom files within a directory into a single matlab 3d
%array

%dicomStackPath - the location of the files.  Note the path needs to end
%     with a '\'.  ie C:\Mike\SkullCapData\ID5685\
%stack - the 3d image stack
%info_stack - a stack with dicom header info that corrisponds to the
%     slicers in the stack variable


fileListUnsorted = dir_matlab(dicomStackPath,false);

fileList = sort_nat(fileListUnsorted);
%fileListNewDir = new_dir(dicomStackPath);

%[pathstr,name,ext] = fileparts(dicomStackPath);

fileList_size = size(fileList,1);
%info_stack(fileList_size,1) = dicominfo([pathstr, '\', fileList(fileList_size).name]);
info_stack = cell(fileList_size,1);

tic 
steps = fileList_size;
h = waitbar(0,'Please wait, Reading Image Data and Info...');
for fileListIndex = 1:1:fileList_size
    info_stack{fileListIndex} = dicominfo(fileList{fileListIndex});
    stack(:,:,fileListIndex) = dicomread(fileList{fileListIndex});
    waitbar(fileListIndex/steps)
end

if exist('sortBy','var')
    [info_stack index] = sortCellStruct(info_stack, sortBy);
    stack = stack(:,:,index);
end

close(h) 
toc


%%
% steps = fileList_size;
% h = waitbar(0,'Please wait, Reading Image Data...');
% for i = 1:1:fileList_size
% 
%     
% 
%     waitbar(i/steps)
% end
% close(h)

