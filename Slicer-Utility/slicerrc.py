# Inspired from startup file found at http://www.na-mic.org/Wiki/index.php/AHM2012-Slicer-Python
# Normand Robert
#Modified by Michael Hardisty
# On linux/unix this is should be a hidden file (leading dot) in your home directory and be called ".slicwerrc.py"

def testcode():
  print "SlicerRC - TestCode setup..."
  import imp, sys, os
  mod = "TestCode"
  sourceFile="F:\Dropbox\Slicer Code\TestCode.py"
  fp = open(sourceFile, "r")
  globals()[mod] = imp.load_module(mod, fp, sourceFile, ('.py', 'r', imp.PY_SOURCE))
  fp.close()
  globals()['e'] = e = globals()[mod].TestCodeWidget()

def testcode_table():
  print "SlicerRC - TestCode_Table setup..."
  import imp, sys, os
  mod = "TestCode_Table"
  sourceFile="F:\Slicer\Code\Pedicle_Screw_Simulator\PedicleScrewSimulator\TestCode_Table.py"
  fp = open(sourceFile, "r")
  globals()[mod] = imp.load_module(mod, fp, sourceFile, ('.py', 'r', imp.PY_SOURCE))
  fp.close()
  globals()['e'] = e = globals()[mod].TestCode_TableWidget()

def rigidity():
  print "SlicerRC - Rigidity setup..."
  import imp, sys, os
  mod = "Rigidity"
  sourceFile="F:\Slicer\Code\Pedicle_Screw_Simulator\PedicleScrewSimulator\Rigidity.py"
  fp = open(sourceFile, "r")
  globals()[mod] = imp.load_module(mod, fp, sourceFile, ('.py', 'r', imp.PY_SOURCE))
  fp.close()
  globals()['e'] = e = globals()[mod].RigidityWidget()

def pedicle():
  print "SlicerRC - Pedicle setup..."
  import imp, sys, os
  mod = "Pedicle Screw Simulator 02"
  sourceFile="F:\Slicer\Code\Pedicle_Screw_Simulator\PedicleScrewSimulator\PedicleScrewSimulator_02.py"
  fp = open(sourceFile, "r")
  globals()[mod] = imp.load_module(mod, fp, sourceFile, ('.py', 'r', imp.PY_SOURCE))
  fp.close()
  globals()['e'] = e = globals()[mod].PedicleScrewSimulator_02Widget()

def endoscopy():
  print "SlicerRC - endoscopy setup..."
  import imp, sys, os
  endoPath = '%s/../Slicer/Modules/Scripted/Endoscopy' % slicer.app.slicerHome
  if not sys.path.__contains__(endoPath):
    sys.path.insert(0,endoPath)

  mod = "Endoscopy"
  sourceFile = endoPath + "/Endoscopy.py"
  fp = open(sourceFile, "r")
  globals()[mod] = imp.load_module(mod, fp, sourceFile, ('.py', 'r', imp.PY_SOURCE))
  fp.close()

  globals()['e'] = e = globals()[mod].EndoscopyWidget()

def CalaveraWorkFlow():
  print "CalaveraModelGenerationWorkFlow setup..."
  import imp, sys, os
  mod = "RulerDataProbe"
  sourceFile="C:\Users\Stewart\calavera-3d-slicer\CalaveraModelGenerationWorkFlow.py"
  fp = open(sourceFile, "r")
  globals()[mod] = imp.load_module(mod, fp, sourceFile, ('.py', 'r', imp.PY_SOURCE))
  fp.close()
  globals()['e'] = e = globals()[mod].RulerDataProbeWidget()


def setupMacros():
  """Set up hot keys for various development scenarios"""

  import qt
#  global endoscopy
  global pedicle
  global testcode
  global testcode_table
  global rigidity
  global RulerDataProbe
  macros = (("Shift+Ctrl+2", pedicle),("Shift+Ctrl+3", testcode),("Shift+Ctrl+4", rigidity),("Shift+Ctrl+5", testcode_table),("Shift+Ctrl+6", CalaveraWorkFlow))

  for keys,f in macros:
    k = qt.QKeySequence(keys)
    s = qt.QShortcut(k,mainWindow())
    s.connect('activated()', f)
    s.connect('activatedAmbiguously()', f)
    print "SlicerRC - '%s' -> '%s'" % (keys, f.__name__)

# Install macros
if mainWindow(verbose=False): setupMacros()
