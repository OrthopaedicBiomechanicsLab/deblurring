
rootScanDir = 'Z:/Calavera/SkullCapData/'

localDirList = ['ID5599','ID5611','ID5616','ID5631','ID5681','ID5738']
scanNameList = ['/102 PETROUS BONES_Deconv.nrrd','/PETROUS BONES_Deconv.nrrd','/PETROUS BONES_Deconv.nrrd','/PETROUS BONES_Deconv.nrrd','/PETROUS BONES_Deconv.nrrd','/PETROUS BONES_Deconv.nrrd']

for localDir, scanName in zip(localDirList, scanNameList):
  patientID = localDir[2:]
  volumeNodeStatus = slicer.util.loadNodeFromFile(rootScanDir + localDir + scanName, 'VolumeFile', {}, True)
  volumeNode = volumeNodeStatus[1]
  parameters = {}
  parameters["patientName"]=localDir
  parameters["patientID"]=patientID
  parameters["studyComments"]='RL deblurred CT of Skull'
  parameters["inputVolume"] = volumeNode.GetID()
  parameters["dicomDirectory"] = rootScanDir + localDir + '/RL' 
  
  slicer.cli.run(cDicom,None,parameters)