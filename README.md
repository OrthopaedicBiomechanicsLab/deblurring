# README #

This project incorporates a couple of tools for deblurring. This project incorporates measuring a Point Spread Function (PSF) and then using the measured PSF to deblur(deconvolve) CT images to improve the ability to see fine structure in the CT.  Primarily, this project is implemented as a number of plugins within [3D slicer](www.slicer.org).

All the development has been on the windows platform, Slicer is cross-platform so theoretically, the module should work wherever Slicer works.

See LICENSE.txt for license information

Dependencies:

- [3D Slicer](https://github.com/Slicer/Slicer)


### How do I get set up? ###

RELEASE

- Please install the  [custom version of slicer with the deblurring module](https://bitbucket.org/OrthopaedicBiomechanicsLab/deblurring/downloads/SlicerDeblurringOnlyCustom-v3.zip)  Embedded within the  packaged version of Slicer is the deblurring Guidelet, which includes tools for deblurring and estimating the PSF. 
- Unzip the file.
- Run Slicer-4.7.0-2017-01-18-win-amd64.exe to install the Slicer executable.
- Install custom deblurring extension
 - Copy entire lib directory within d254fdf-win-amd64-deblurring...zip file to $Slicer_Install_DIR$.  Confirm merging of lib directories when prompted.

DEVELOPMENT VERSION

- Install [3D Slicer](https://github.com/Slicer/Slicer)
- Clone [deblurring repository] (https://bitbucket.org/OrthopaedicBiomechanicsLab/deblurring), git clone https://hardisty@bitbucket.org/OrthopaedicBiomechanicsLab/deblurring.git

**Link Deblurring Modules to Slicer**

1. Open Slicer.exe from the super-build directory. 
2. Click on Edit --> Application Settings --> Modules. Then from the deblurring source directory, go to lib --> Slicer.47. Drag and drop the qt-scripted-modules into the "Additional module paths" field. 
3. Restart Slicer. All the deblurring modules should now be present in the Modules tab.


### How do I Deblur CT scans? ###

Have a look at the [SOP](https://orthopaedicbiomechanicslab.bitbucket.io) or [pdf](https://bitbucket.org/OrthopaedicBiomechanicsLab/deblurring/downloads/CAL-DeblurScansinSlicerStandardOperatingProcedure-131020-1128-9.pdf)


### Who do I talk to? ###

Michael Hardisty: m.hardisty@utoronto.ca, this code was developed at Sunnybrook Research Institute, so you might also want to speak with my collaborators at SRI, Cari Whyne cari.whyne@sunnybrook.ca or James Mainprize james@mainprize.ca
Amani Ibrahim: 9ai15@queensu.ca: Amani wrote much of the code in python while a Masters student at Queens University
