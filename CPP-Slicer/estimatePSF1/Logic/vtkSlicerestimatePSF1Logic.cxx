/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/
//Matlab estimate Gaussian includes
#include "estimateGaussianPSF.h"
// estimatePSF1 Logic includes
#include "vtkSlicerestimatePSF1Logic.h"

// MRML includes
#include <vtkMRMLAnnotationRulerNode.h>
#include <vtkMRMLAnnotationHierarchyNode.h>
#include <vtkMRMLestimatePSF1ParametersNode.h>
//#include "matlab.hpp"



#include <vtkMRMLScalarVolumeNode.h>
#include <vtkMRMLVolumeNode.h>
#include <vtkMRMLScene.h>

// VTK includes
#include <vtkIntArray.h>
#include <vtkImageData.h>
#include <vtkNew.h>
#include <vtkObjectFactory.h>
#include <vtkSmartPointer.h>
#include <vtkVersion.h>
#include <vtkLineSource.h>
#include <vtkProbeFilter.h>
#include <vtkPointData.h>
#include <vtkMatrix4x4.h>
// STD includes
#include <cassert>
#include <iostream>


#define WDBGVAR( var ) \
{ \
  std::ostringstream ods_stream_; \
  ods_stream_ << __FILE__ << "(" << __LINE__ << ") " << #var << " = [" << (var) << "]\n"; \
  ::OutputDebugString( ods_stream_.str().c_str() ); \
}

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkSlicerestimatePSF1Logic);

//----------------------------------------------------------------------------
vtkSlicerestimatePSF1Logic::vtkSlicerestimatePSF1Logic()
{
	bool ret = estimateGaussianPSFInitialize();
      if (!ret){
        std::cout << "Error initializing MATLAB Component Runtime\n";
        system("PAUSE");
        //return 0;
      }
}

//----------------------------------------------------------------------------
vtkSlicerestimatePSF1Logic::~vtkSlicerestimatePSF1Logic()
{
	estimateGaussianPSFTerminate();
}

//----------------------------------------------------------------------------
void vtkSlicerestimatePSF1Logic::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "vtkSlicerestimatePSF1Logic:             " << this->GetClassName() << "\n";
}

vtkSmartPointer<vtkLineSource> vtkSlicerestimatePSF1Logic::createLine(float p1[3],float p2[3],int numPoints) const
{
    //# Create the line along which you want to sample
	vtkSmartPointer<vtkLineSource> line = 
		vtkSmartPointer<vtkLineSource>::New();
    line->SetResolution(numPoints);
    line->SetPoint1(p1);
    line->SetPoint2(p2);
    line->Update();
    return line;
}

vtkSmartPointer<vtkDataArray> vtkSlicerestimatePSF1Logic::probeOverLine(vtkSmartPointer<vtkLineSource> line,vtkImageData* vtkdata) const
{
    //#Interpolate the data from the VTK-file on the created line.
    vtkImageData* data = vtkdata;
    //# vtkProbeFilter, the probe line is the input, and the underlying dataset is the source.
    vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();
    probe->SetInputConnection(line->GetOutputPort());
    probe->SetSourceData(data);
    probe->Update();
    //#get the data from the VTK-object (probe) to an numpy array
    vtkSmartPointer<vtkDataArray> t(probe->GetOutput()->GetPointData()->GetArray(0));
    //print  data
    
    return t;
}

int vtkSlicerestimatePSF1Logic::Apply(vtkMRMLestimatePSF1ParametersNode* pnode)
{

 vtkMRMLScene *scene = this->GetMRMLScene();

  vtkMRMLVolumeNode *inputVolume =
    vtkMRMLVolumeNode::SafeDownCast(scene->GetNodeByID(pnode->GetInputVolumeNodeID()));
  vtkMRMLAnnotationHierarchyNode *inputRulerList =
    vtkMRMLAnnotationHierarchyNode::SafeDownCast(scene->GetNodeByID(pnode->GetRulerListNodeID()));

  vtkImageData *inputVolumeImage = inputVolume->GetImageData();


  int numberOfPotentialRulers;
  numberOfPotentialRulers = inputRulerList->GetNumberOfChildrenNodes();

  if(!inputVolume || !inputRulerList)
    {
    std::cerr << "Failed to look up input volume and/or Ruler List Hierarchy Node!" << std::endl;
    return -1;
    }
  if(numberOfPotentialRulers==0)
    {
    std::cerr << "No Rulers in List!" << std::endl;
    return -1;
    }

    //This is where it all happens call the relevent matlab dll now
//7)  In your C/C++ code, initialize the MATLAB component:

  //Going to try moving this to the constructor, so that its only created once, maybe the graphs stay and it fixes the exception?
      /*bool ret = estimateGaussianPSFInitialize();
      if (!ret){
        std::cout << "Error initializing MATLAB Component Runtime\n";
        system("PAUSE");
        return 0;
      }*/
//8)  Create the input variables to the MATLAB function:

	  int nargout = 4; // 
	  
	  //need to populate these values
	  
	  
	  //need to get these values from the scan and other modules
	  int numPoint = 1000;

	  //need to get each ruler
	  //make each ruler a line
	  //get the intensity at each point along the line - this is what profiles is composed of
	  //generate the position along the line in mm
	  int numberOfProfiles = 0;
  
	  vtkCollection* rulerListCollection=0;
	  //inputRulerList->up;
	  //std::vector< vtkMRMLHierarchyNode *> rulerListNodes;
      //inputRulerList->GetAllChildrenNodes(allChildren);
	  //inputRulerList->GetAllChildren( rulerListCollection);
	  //Example Line CopiedvtkMRMLDisplayableHierarchyNode *dispHierarchyNode = vtkMRMLDisplayableHierarchyNode::SafeDownCast(allChildren[i]);

	 
	  vtkMRMLAnnotationRulerNode* currentRulerNode = 0;

	  //checking number of profiles
	  for (int rulerIndex = 0; rulerIndex < numberOfPotentialRulers; rulerIndex++)
		{
			
			currentRulerNode =
				vtkMRMLAnnotationRulerNode::SafeDownCast(inputRulerList->GetNthChildNode(rulerIndex)->GetAssociatedNode());
			if (!currentRulerNode==0)
			{
				numberOfProfiles++;
			}
		}
	   
	  mwArray profilesX(numberOfProfiles,numPoint, mxDOUBLE_CLASS);
	  mwArray profilesIntensity(numberOfProfiles,numPoint, mxDOUBLE_CLASS);
	  //profiles(numberOfProfiles,2,numPoint) = 1;

	  mwArray edges(2,numberOfProfiles, mxDOUBLE_CLASS);;
	  //edges(2,numberOfProfiles) = 1;

	  mwArray profilePositionsBeginning(numberOfProfiles,3, mxDOUBLE_CLASS);
	  mwArray profilePositionsEnd(numberOfProfiles,3, mxDOUBLE_CLASS);

	  int currentProfileIndex = 0;
	  //vtkMRMLAnnotationRulerNode* currentRulerNode =0;

	  for (int rulerIndex = 0; rulerIndex < numberOfPotentialRulers; rulerIndex++)
		{
			currentRulerNode =
				vtkMRMLAnnotationRulerNode::SafeDownCast(inputRulerList->GetNthChildNode(rulerIndex)->GetAssociatedNode());
			if (!currentRulerNode==0)
			{
//TRYING TO FIGURE OUT LWHY THE LINE POSITION ISN'T COMING THROUGH
				double linePosition1[3] = {0,0,0};
				currentRulerNode->GetPosition1(linePosition1);
				double linePosition2[3] = {0,0,0};
				currentRulerNode->GetPosition2(linePosition2);
//currentRulerNode->GetPosition1()

				//#transforming p1,p2 to the ijk space
				vtkSmartPointer<vtkMatrix4x4> rasToijkMatrix = vtkSmartPointer<vtkMatrix4x4>::New();// = vtkMatrix4x4() #= np.arange(16).reshape(4,4)
				inputVolume->GetRASToIJKMatrix(rasToijkMatrix);

				float fltLinePostion1[4] = {linePosition1[0],linePosition1[1],linePosition1[2],1};
				float fltLinePostion2[4] = {linePosition2[0],linePosition2[1],linePosition2[2],1};
				float linePosition1_ijk[4] = {0,0,0,1};
				float linePosition2_ijk[4] = {0,0,0,1};//might need to free this memory

				WDBGVAR(linePosition1[0]);
				WDBGVAR(linePosition1[1]);
				WDBGVAR(linePosition1[2]);
				WDBGVAR(linePosition1[3]);
				WDBGVAR(linePosition2[0]);
				WDBGVAR(linePosition2[1]);
				WDBGVAR(linePosition2[2]);
				WDBGVAR(linePosition2[3]);

				
				WDBGVAR(*rasToijkMatrix);

				rasToijkMatrix->MultiplyPoint(fltLinePostion1,linePosition1_ijk);
				rasToijkMatrix->MultiplyPoint(fltLinePostion2,linePosition2_ijk);


				WDBGVAR(linePosition1_ijk[0]);
				WDBGVAR(linePosition1_ijk[1]);
				WDBGVAR(linePosition1_ijk[2]);
				WDBGVAR(linePosition1_ijk[3]);
				WDBGVAR(linePosition2_ijk[0]);
				WDBGVAR(linePosition2_ijk[1]);
				WDBGVAR(linePosition2_ijk[2]);
				WDBGVAR(linePosition2_ijk[3]);
				
				//WDBGVAR(linePosition2_ijk);


				//p1_ijk = p1_ijk[0:3]
				//p2_ijk = p2_ijk[0:3]

				vtkSmartPointer<vtkLineSource> currentLine = createLine(linePosition1_ijk,linePosition2_ijk,numPoint);
				vtkSmartPointer<vtkDataArray> imageIntensityAlongLine = probeOverLine(currentLine,inputVolumeImage);
				
				
				double lineLength = pow(pow(linePosition1[0]-linePosition2[0],2)+pow(linePosition1[1]-linePosition2[1],2)+pow(linePosition1[2]-linePosition2[2],2),(0.5));
				
				double lineLengthIncrement = lineLength/(numPoint-1);
				for (int pointIndex = 0;pointIndex<numPoint;pointIndex++)
				{
					profilesX(currentProfileIndex+1,pointIndex+1) = lineLengthIncrement*pointIndex;
					double currentIntensityValue[1];
					imageIntensityAlongLine->GetTuple(pointIndex,currentIntensityValue); 
					profilesIntensity(currentProfileIndex+1,pointIndex+1) = currentIntensityValue[0];
				}

				edges(1,currentProfileIndex+1) = 0;
				
				edges(2,currentProfileIndex+1) = lineLength;
				//there is something wrong here in that my understanding is that lines ought to have 3 coordinates
				//I think that the profile position for out of plane sigma is either yz or xz but its unclear to me how this
				//should work
				profilePositionsBeginning(currentProfileIndex+1,1) = linePosition1[0];
				profilePositionsBeginning(currentProfileIndex+1,2) = linePosition1[1];
				profilePositionsBeginning(currentProfileIndex+1,3) = linePosition1[2];
				profilePositionsEnd(currentProfileIndex+1,1) = linePosition2[0];
				profilePositionsEnd(currentProfileIndex+1,2) = linePosition2[1];
				profilePositionsEnd(currentProfileIndex+1,3) = linePosition2[2];

				currentProfileIndex++;
			}
		}







	  mwArray sliceThickness(inputVolume->GetSpacing()[2]);
	  mwArray pixelSpacing(inputVolume->GetSpacing()[0]);

	  //From the UI
	  mwArray inPlaneFlag(!pnode->GetSigmaDirection());
	  mwArray corticalIntensityMin(pnode->GetCorticalIntensityMin()); 
	  mwArray corticalIntensityMax(pnode->GetCorticalIntensityMax());
	  mwArray trabecularIntensityMin(pnode->GetTrabecularIntensityMin());
	  mwArray trabecularIntensityMax(pnode->GetTrabecularIntensityMax());
	  mwArray minCortThickness(pnode->GetTrabecularIntensityMin());
	  mwArray minTrabThickness(pnode->GetTrabecularIntensityMax());
	  mwArray sigmaMax(pnode->GetSigmaMax());
	  mwArray sigmaMin(pnode->GetSigmaMin());
	  mwArray slackFactor(pnode->GetEdgeDetectionSlackFactor());
	  
	  
	  
	  mwArray inPlaneSig(pnode->GetInPlaneSigma());


//9)  Create the output variables from the MATLAB function:

      mwArray sigma;
	  mwArray cortIntensity;
	  mwArray edgePositions; 
	  mwArray profileValuesAtEdges;

	  

//10) Call your function:

	  WDBGVAR(sigma);
	  WDBGVAR(cortIntensity);
	  WDBGVAR(edgePositions);
	  WDBGVAR(profileValuesAtEdges);
	  WDBGVAR(inPlaneFlag);
	  WDBGVAR(profilesX);
	  WDBGVAR(profilesIntensity);
	  WDBGVAR(edges);
	  WDBGVAR(corticalIntensityMin);
	  WDBGVAR(corticalIntensityMax);
	  WDBGVAR(trabecularIntensityMin);
	  WDBGVAR(trabecularIntensityMax);
	  WDBGVAR(minCortThickness);
	  WDBGVAR(minTrabThickness);
	  WDBGVAR(sigmaMax);
	  WDBGVAR(sigmaMin);
	  WDBGVAR(slackFactor);
	  WDBGVAR(sliceThickness);
	  WDBGVAR(pixelSpacing);
	  WDBGVAR(profilePositionsBeginning);
	  WDBGVAR(profilePositionsEnd);
	  WDBGVAR(inPlaneSig);

      
	  estimateGaussianPSF(nargout, sigma, cortIntensity, edgePositions, profileValuesAtEdges, inPlaneFlag, profilesX, profilesIntensity,  edges,  corticalIntensityMin, corticalIntensityMax, trabecularIntensityMin, trabecularIntensityMax, minCortThickness, minTrabThickness, sigmaMax, sigmaMin, slackFactor,  sliceThickness, pixelSpacing, profilePositionsBeginning, profilePositionsEnd, inPlaneSig);


	  WDBGVAR(sigma);
	  WDBGVAR(cortIntensity);
	  WDBGVAR(edgePositions);
	  WDBGVAR(profileValuesAtEdges);
	  
	  

//11) Get the data out of the mwArrays:
//
//      int length;
//      mwLength.GetData(&length, 1);
//      double *xp = new double[length];
//      mwXP.GetData(xp, length);
//12) Terminate the use of the MATLAB component:

      //estimateGaussianPSFTerminate();

  //report the result
  //outputVolume->SetAndObserveTransformNodeID(NULL);
  //pnode->SetOutputVolumeNodeID(outputVoume->GetID());

  //this is probably not sufficient, I need some way of keeping track of multiple groups but this will do for the first version
	  if (pnode->GetSigmaDirection() ==0)
	  {
		  pnode->SetInPlaneSigma(sigma);
	  } else {
		  pnode->SetOutOfPlaneSigma(sigma);
	  }

  
  
  return 0;
}
//---------------------------------------------------------------------------
void vtkSlicerestimatePSF1Logic::SetMRMLSceneInternal(vtkMRMLScene * newScene)
{
  vtkNew<vtkIntArray> events;
  events->InsertNextValue(vtkMRMLScene::NodeAddedEvent);
  events->InsertNextValue(vtkMRMLScene::NodeRemovedEvent);
  events->InsertNextValue(vtkMRMLScene::EndBatchProcessEvent);
  this->SetAndObserveMRMLSceneEventsInternal(newScene, events.GetPointer());
}

//-----------------------------------------------------------------------------
void vtkSlicerestimatePSF1Logic::RegisterNodes()
{
  assert(this->GetMRMLScene() != 0);
}
//---------------------------------------------------------------------------
void vtkSlicerestimatePSF1Logic::UpdateFromMRMLScene()
{
  assert(this->GetMRMLScene() != 0);
}

//---------------------------------------------------------------------------
void vtkSlicerestimatePSF1Logic
::OnMRMLSceneNodeAdded(vtkMRMLNode* vtkNotUsed(node))
{
}

//---------------------------------------------------------------------------
void vtkSlicerestimatePSF1Logic
::OnMRMLSceneNodeRemoved(vtkMRMLNode* vtkNotUsed(node))
{
}
