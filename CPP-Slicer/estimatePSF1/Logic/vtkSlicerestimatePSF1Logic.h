/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// .NAME vtkSlicerestimatePSF1Logic - slicer logic class for volumes manipulation
// .SECTION Description
// This class manages the logic associated with reading, saving,
// and changing propertied of the volumes


#ifndef __vtkSlicerestimatePSF1Logic_h
#define __vtkSlicerestimatePSF1Logic_h

// Slicer includes
#include "vtkSlicerModuleLogic.h"

class vtkSlicerCLIModuleLogic;
class vtkSlicerVolumesLogic;
class vtkMRMLVolumeNode;
class vtkMRMLAnnotationRulerNode;
class vtkMRMLAnnotationHierachyNode;


// STD includes
#include <cstdlib>

#include "vtkSlicerestimatePSF1ModuleLogicExport.h"
#include "vtkLineSource.h"


// MRML includes
class vtkMRMLestimatePSF1ParametersNode;

/// \ingroup Slicer_QtModules_ExtensionTemplate
class VTK_SLICER_ESTIMATEPSF1_MODULE_LOGIC_EXPORT vtkSlicerestimatePSF1Logic :
  public vtkSlicerModuleLogic
{
public:

  static vtkSlicerestimatePSF1Logic *New();
  vtkTypeMacro(vtkSlicerestimatePSF1Logic, vtkSlicerModuleLogic);
  void PrintSelf(ostream& os, vtkIndent indent);
  virtual void RegisterNodes();
  int Apply(vtkMRMLestimatePSF1ParametersNode* pnode);
  vtkSmartPointer<vtkLineSource> createLine(float p1[3],float p2[3],int numPoints) const;
  vtkSmartPointer<vtkDataArray> probeOverLine(vtkSmartPointer<vtkLineSource> line,vtkImageData* vtkdata) const;
  //virtual void RegisterNodes();

protected:
  vtkSlicerestimatePSF1Logic();
  virtual ~vtkSlicerestimatePSF1Logic();

  virtual void SetMRMLSceneInternal(vtkMRMLScene* newScene);
  /// Register MRML Node classes to Scene. Gets called automatically when the MRMLScene is attached to this logic class.
  
  virtual void UpdateFromMRMLScene();
  virtual void OnMRMLSceneNodeAdded(vtkMRMLNode* node);
  virtual void OnMRMLSceneNodeRemoved(vtkMRMLNode* node);
private:

  vtkSlicerestimatePSF1Logic(const vtkSlicerestimatePSF1Logic&); // Not implemented
  void operator=(const vtkSlicerestimatePSF1Logic&); // Not implemented
};

#endif
