

  12   array->Resize(10);

typedef typename vtkSmartPointer<const vtkDenseArray<double>> SmartPointerArrayType;

bool estimateGaussianPSF( bool inPlaneFlag, 
						 SmartPointerArrayType profilesX, 
						 SmartPointerArrayType profilesIntensity, 
						 SmartPointerArrayType edges, 
						 long corticalIntensityMin, 
						 long corticalIntensityMax, 
						 long trabecularIntensityMin, 
						 long trabecularIntensityMax, 
						 double minCortThickness, 
						 double minTrabThickness, 
						 double sigmaMax,
						 double sigmaMin,
						 double slackFactor,
						 double sliceThickness =0.5, 
						 double pixelSpacing = 0.5059, 
						 const double* profilePositionsBeginning, 
						 const double* profilePositionsEnd, 
						 double inPlaneSig, 
						 double &sigma,  
						 double &cortIntensity, 
						 double &edgePositions, 
						 double &profileValuesAtEdges)
{

	//optional arguments
	//sliceThickness
	//pixelSpacing
	//profilePositions note that profilePositionsBeg and ProfilePositionsEnd
	//if ~exist('sliceThickness','var'), sliceThickness = 0.5; end %in mm
	//if ~exist('pixelSpacing','var'), pixelSpacing = 0.5059; end %in mm
	//if ~exist('inPlaneSig','var'), inPlaneSig = 0; end %in mm

	double angles = 0;
	//% handles    structure with handles and user data (see GUIDATA)

	/*% t = distance vector, not to be confused with variable vector x
	% y = CT intensities
	% n = # of boxcar functions
	% y_0 = lower tail constant intensity
	% y_f = higher tail constant intensity
	% A_CT = area of CT profile
	*/


	const double min_cortthick = minCortThickness; 
	const double min_trabthick = minTrabThickness;

	const double sig_max = sigmaMax;  %sigma bounds
	const double sig_min = sigmaMin;
	const double sig_ini = (sig_max+sig_min)/2;

	const double cort_max = corticalIntensityMax;    %cortical intensity bounds
	const double cort_min = corticalIntensityMin;
	const double cort_ini = (cort_max + cort_min)/2;

	const double trab_max = trabecularIntensityMax;     %trabecular intensity bounds
	const double trab_min = trabecularIntensityMin;
	const double trab_ini = (trab_max + trab_min)/2;

	const int NUMBEROFPROFILES = sizeof(


	//%start assembly of bounds vectors
	SmartPointerArrayType x0 = new double[

		//matlab border
	x0 = [sig_ini];
	lb = [sig_min];
	ub = [sig_max];

	 %probably this is always set to one, I guess if I want to add more layers I'd have to make this a parameter

	profileCount = size(profilesX,1);
	n = ones(1,profileCount); 
	%Do the slice and longitudinal profiles seperation here
	%inplanemode = inPlaneFlag;




	 for i = 1:profileCount
			%Assume that all profiles passed are to be used.  The filtering
			%will be done externally
			%if strcmp(handles.(['profile',num2str(i)]).orientation,'xy') == 1
			t(:,i) = profilesX(i,:);
			y(:,i) = profilesIntensity(i,:);
			%n(:,i) = n(:,i);
			edge1(i) = edges(1,i);
			edge2(i) = edges(2,i);
			%order(i) = i; %saves the master set location of the profile of the xy subset
			%j = j + 1;
	        
	 end

	if inPlaneFlag ~= 1
	     
		%inplanesig = str2num(get(handles.sigxy,'String'));
	    
		%handles.profilesubset = nn;   %number of profiles
		for i = 1:profileCount
	        
	                 
				%pixel size ratio must be taken into account for angle
				%calculation
				y_scale = sliceThickness/pixelSpacing(1);
				%angles(:,i) =  atan((y_scale*abs((handles.(['profile',num2str(i)]).yi(2)-handles.(['profile',num2str(i)]).yi(1)))/abs((handles.(['profile',num2str(i)]).xi(2)-handles.(['profile',num2str(i)]).xi(1))))); %calculate cortical surface angle
				%//# this andgle is the 90-angle between the profile and the z
				%direction, so dot product between z and profile direction
				%should be useful
				tempAngle = acos((profilePositionsEnd(i,3)-profilePositionsBeginning(i,3))/((profilePositionsEnd(i,1)-profilePositionsBeginning(i,1))^2+(profilePositionsEnd(i,2)-profilePositionsBeginning(i,2))^2+(profilePositionsEnd(i,3)-profilePositionsBeginning(i,3))^2)^0.5);
				if and(tempAngle >= pi/2,  tempAngle <pi)
					tempAngle = pi-tempAngle;
				elseif and(tempAngle >=pi,tempAngle <3*pi/2)
					 tempAngle = tempAngle-pi;
				elseif and(tempAngle >=3*pi/2,tempAngle <2*pi)
					tempAngle = 2*pi-tempAngle;
				end
	            
				if and(tempAngle <= 0,  tempAngle >-pi/2)
					tempAngle = -tempAngle;
				elseif and(tempAngle <= -pi/2,tempAngle >-pi)
					 tempAngle = pi+tempAngle;
				elseif and(tempAngle <= -pi,tempAngle >-3*pi/2)
					tempAngle = -pi-tempAngle;
				elseif and(tempAngle >= -2*pi,tempAngle <-3*pi/2)
					tempAngle = 2*pi+tempAngle;
				end
				angles(:,i)=pi/2-tempAngle;
				%angles(:,i) =  atan((y_scale  *  abs(profilePositionsEnd(i,2)-profilePositionsBeginning(i,2)))  /  abs(profilePositionsEnd(i,2,1)-profilePositionsBeginning(i,1,1)) ); %calculate cortical surface angle
				%so for each profile 
				%angles(:,j) = -0.52;
	            
	           
	            
	        
		end
	end

	for i = 1:profileCount %make an amalgamated vector
	    
		%Edge settings based on segmentation
		x_edge_1 = edge1(i); %t(round(length(t)*0.4));%t(find(handles.(['profile',num2str(i)]).segc,1,'first'),i);
		x_edge_2 = edge2(i); %t(find(handles.(['profile',num2str(i)]).segc,1,'last'), i);
		w = (x_edge_2 - x_edge_1)/2;
	    
	    
		t_min = (x_edge_1 + x_edge_2)/2 - slackFactor*w; %set lb and up, extend of the profile
		t_max = (x_edge_1 + x_edge_2)/2 + slackFactor*w;
	    
		%leading and trailing y-values/intensities averaging, and choosing the
		%"baseline"
		if t_min <= 0
			t_min = 0;
		end
		last = t(length(t(:,i)),i);
		if t_max >= last;
			t_max = last;
		end
		e_1_i(:,i) = searchclosest(t(:,i),t_min);
		y_0_v = y(1:searchclosest(t(:,i),x_edge_1),i);
		y_0_ini = mean(y_0_v);
		e_2_i(:,i) = searchclosest(t(:,i),t_max);
		y_f_v = y(searchclosest(t(:,i),x_edge_2):end,i);
		y_f_ini = mean(y_f_v);
	    
		if y_0_ini < y_f_ini %set baseline/reference y
			y_b(:,i) = y_0_ini;
		else
			y_b(:,i) = y_f_ini;
		end
	    
		A_CT(:,i) = trapz(t(e_1_i(:,i):e_2_i(:,i),i),y(e_1_i(:,i):e_2_i(:,i),i)-y_b(:,i));
	    
		%note that this doesn't happen currently as I haven't allowed more than
		%1 layer
		if n(i) == 3
			t_div = (t_max-t_min)/n(:,i);
			x0 = [x0   cort_ini    trab_ini    cort_ini   t_min    t_min     t_min+t_div*2       t_max   y_0_ini   y_f_ini]   ;   %solution seed
			lb = [lb   cort_min    trab_min    cort_min   t_min    t_min     t_min      t_min     y_0_ini-10*std(y_0_v)   y_f_ini-10*std(y_0_v)   ];
			ub = [ub   cort_max    trab_max    cort_max   t_max    t_max     t_max      t_max     y_0_ini+10*std(y_0_v)  y_f_ini+10*std(y_0_v)  ];
		end
	    
		if n(i) == 1
			x0 = [x0 cort_ini    t_min       t_max     y_0_ini   y_f_ini]   ;   %solution seed
			lb = [lb cort_min    t_min       t_min     y_0_ini-10*std(y_0_v)   y_f_ini-10*std(y_f_v)   ];
			ub = [ub cort_max    t_max       t_max     y_0_ini+10*std(y_0_v)   y_f_ini+10*std(y_f_v)   ];
		end
	    
		A = [];
		b = [];
	    
		Aeq = [];
		beq = [];
	    
	    
	end

	clear i

	%a brute force way of making the cell array necessary for the plotting component of the optim function
	%outfun(x,optimValues,state,p,t, y, n, nn, inplanesig, angles, inplanemode)
	c = {};
	c(1) = {@(x,optimValues,state)outfun(x,optimValues,state,1,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(2) = {@(x,optimValues,state)outfun(x,optimValues,state,2,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(3) = {@(x,optimValues,state)outfun(x,optimValues,state,3,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(4) = {@(x,optimValues,state)outfun(x,optimValues,state,4,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(5) = {@(x,optimValues,state)outfun(x,optimValues,state,5,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(6) = {@(x,optimValues,state)outfun(x,optimValues,state,6,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(7) = {@(x,optimValues,state)outfun(x,optimValues,state,7,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(8) = {@(x,optimValues,state)outfun(x,optimValues,state,8,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(9) = {@(x,optimValues,state)outfun(x,optimValues,state,9,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(10) = {@(x,optimValues,state)outfun(x,optimValues,state,10,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(11) = {@(x,optimValues,state)outfun(x,optimValues,state,11,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(12) = {@(x,optimValues,state)outfun(x,optimValues,state,12,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(13) = {@(x,optimValues,state)outfun(x,optimValues,state,13,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(14) = {@(x,optimValues,state)outfun(x,optimValues,state,14,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};
	c(15) = {@(x,optimValues,state)outfun(x,optimValues,state,15,t, y, n, profileCount, inPlaneSig, angles, inPlaneFlag)};

	plotarray = c(1:profileCount);

	%creating functions that take only one argument
	% non-linear constraint, matching areas
	% global variables:
	% t = distance vector
	% y = CT intensities
	% n = # of boxcar functions
	% y_0 = lower tail constant intensity
	% y_f = higher tail constant intensity
	% A_CT = area of CT profile

	%constraints(x,n, nn, A_CT, t, y_b, e_1_i, e_2_i, h, xsub, min_cortthick, min_trabthick, inplanesig, angles, inplanemode)

	global objectiveX
	objectiveX = @(x)objective(x, t, y, n, profileCount, e_1_i, e_2_i, inPlaneFlag, inPlaneSig, angles);

	global constraintsX
	constraintsX = @(x)constraints(x, n, profileCount, A_CT, t, y_b, e_1_i, e_2_i, min_cortthick, min_trabthick, inPlaneSig, angles, inPlaneFlag);

	%options = optimset('Algorithm','interior-point','display','on', 'GradConstr','off','DerivativeCheck','off','TolFun',1e-10*sum(A_CT),'PlotFcns', plotarray, 'MaxFunEvals',40000,'UseParallel','always' )%
	%0.5445
	options = optimset('Algorithm','sqp','display','on', 'GradConstr','off','DerivativeCheck','off','TolFun',1e-10*sum(A_CT),'PlotFcns', plotarray, 'MaxFunEvals',40000,'UseParallel','always' )%
	%0.5505
	%[xResult , fval] = fmincon(objectiveX,x0,A,b,Aeq,beq,lb,ub,constraintsX,options);  %objective function value

	%opt.algorithm = NLOPT_LN_NEWUOA %0.5342
	opt.algorithm = NLOPT_LN_COBYLA %0.5456
	opt.verbose = 1;
	opt.upper_bounds = ub
	opt.lower_bounds = lb
	opt.min_objective = @objectiveXgrad
	opt.fc = {@(x)constraintsXGrad(x,1),@(x)constraintsXGrad(x,2),@(x)constraintsXGrad(x,3)}
	%opt.fc = { (@(x) myconstraint(x,2,0)), (@(x) myconstraint(x,-1,1)) }
	%opt.fc_tol = [1e-8, 1e-8];
	opt.xtol_rel = 1e-5


	[xResult, fval, retcode] = nlopt_optimize(opt, x0)

	sigma = xResult(1);
	cortIntensity = xResult(2);
	edgePositions(1) = xResult(3);
	edgePositions(2) = xResult(4);
	profileValuesAtEdges(1) = xResult(5);
	profileValuesAtEdges(2) = xResult(6);

	 %points = [x cort_intensity    t_minPosition       t_maxPosition     y_@minPosition   y_@maxPosition]


}