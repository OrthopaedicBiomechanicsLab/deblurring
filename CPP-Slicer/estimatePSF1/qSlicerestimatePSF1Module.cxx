/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.


  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// Qt includes
#include <QtPlugin>

// estimatePSF1 Logic includes
#include <vtkSlicerestimatePSF1Logic.h>

// estimatePSF1 includes
#include "qSlicerestimatePSF1Module.h"
#include "qSlicerestimatePSF1ModuleWidget.h"



//-----------------------------------------------------------------------------
Q_EXPORT_PLUGIN2(qSlicerestimatePSF1Module, qSlicerestimatePSF1Module);


//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerestimatePSF1ModulePrivate

{
public:
  qSlicerestimatePSF1ModulePrivate();
};

//-----------------------------------------------------------------------------
// qSlicerestimatePSF1ModulePrivate methods

//-----------------------------------------------------------------------------
qSlicerestimatePSF1ModulePrivate::qSlicerestimatePSF1ModulePrivate()
{
}

//-----------------------------------------------------------------------------
// qSlicerestimatePSF1Module methods

//-----------------------------------------------------------------------------
qSlicerestimatePSF1Module::qSlicerestimatePSF1Module(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerestimatePSF1ModulePrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerestimatePSF1Module::~qSlicerestimatePSF1Module()
{
}



//-----------------------------------------------------------------------------
QString qSlicerestimatePSF1Module::helpText() const
{
  return "This is a loadable module that can be bundled in an extension";
}



//-----------------------------------------------------------------------------
QString qSlicerestimatePSF1Module::acknowledgementText() const
{
  return "This work was partially funded by NIH grant NXNNXXNNNNNN-NNXN";
}

//-----------------------------------------------------------------------------
QStringList qSlicerestimatePSF1Module::contributors() const
{
  QStringList moduleContributors;
  moduleContributors << QString("John Doe (AnyWare Corp.)");
  return moduleContributors;
}

//-----------------------------------------------------------------------------
QIcon qSlicerestimatePSF1Module::icon() const
{
  return QIcon(":/Icons/estimatePSF1.png");
}

//-----------------------------------------------------------------------------
QStringList qSlicerestimatePSF1Module::categories() const
{
  return QStringList() << "Examples";
}

//-----------------------------------------------------------------------------
QStringList qSlicerestimatePSF1Module::dependencies() const
{
  return QStringList() << "Annotations";
}

//-----------------------------------------------------------------------------
void qSlicerestimatePSF1Module::setup()
{
  this->Superclass::setup();
//consider including some tests and any setup required
  vtkSlicerestimatePSF1Logic* estimatePSF1Logic =
    vtkSlicerestimatePSF1Logic::SafeDownCast(this->logic());
}

//-----------------------------------------------------------------------------
qSlicerAbstractModuleRepresentation* qSlicerestimatePSF1Module
::createWidgetRepresentation()
{
  return new qSlicerestimatePSF1ModuleWidget;
}

//-----------------------------------------------------------------------------
vtkMRMLAbstractLogic* qSlicerestimatePSF1Module::createLogic()
{
  return vtkSlicerestimatePSF1Logic::New();
}
