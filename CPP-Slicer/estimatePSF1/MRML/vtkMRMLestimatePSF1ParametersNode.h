/*=auto=========================================================================

  Portions (c) Copyright 2005 Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Program:   3D Slicer
  Module:    $RCSfile: vtkMRMLVolumeRenderingParametersNode.h,v $
  Date:      $Date: 2006/03/19 17:12:29 $
  Version:   $Revision: 1.3 $

=========================================================================auto=*/
// .NAME vtkMRMLVolumeRenderingParametersNode - MRML node for storing a slice through RAS space
// .SECTION Description
// This node stores the information about the currently selected volume
//
//

#ifndef __vtkMRMLestimatePSF1ParametersNode_h
#define __vtkMRMLestimatePSF1ParametersNode_h

#include "vtkMRML.h"
#include "vtkMRMLScene.h"
#include "vtkMRMLNode.h"
#include "vtkSlicerestimatePSF1ModuleMRMLExport.h"

class vtkMRMLAnnotationRulerListNode;
class vtkMRMLVolumeNode;

/// \ingroup Slicer_QtModules_estimatePSF1
class VTK_SLICER_ESTIMATEPSF1_MODULE_MRML_EXPORT vtkMRMLestimatePSF1ParametersNode : public vtkMRMLNode
{
  public:

  static vtkMRMLestimatePSF1ParametersNode *New();
  vtkTypeMacro(vtkMRMLestimatePSF1ParametersNode,vtkMRMLNode);
  void PrintSelf(ostream& os, vtkIndent indent);

  virtual vtkMRMLNode* CreateNodeInstance();

  // Description:
  // Set node attributes
  virtual void ReadXMLAttributes( const char** atts);

  // Description:
  // Write this node's information to a MRML file in XML format.
  virtual void WriteXML(ostream& of, int indent);

  // Description:
  // Copy the node's attributes to this object
  virtual void Copy(vtkMRMLNode *node);

  // Description:
  // Get node XML tag name (like Volume, Model)
  virtual const char* GetNodeTagName() {return "estimatePSF1Parameters";};

  // Description:
  vtkSetStringMacro(InputVolumeNodeID);
  vtkGetStringMacro (InputVolumeNodeID);
  //vtkSetStringMacro(OutputVolumeNodeID);
  //vtkGetStringMacro (OutputVolumeNodeID);
  vtkSetStringMacro(RulerListNodeID);
  vtkGetStringMacro (RulerListNodeID);

  //vtkSetMacro(IsotropicResampling,bool);
  //vtkGetMacro(IsotropicResampling,bool);
  //vtkBooleanMacro(IsotropicResampling,bool);

  vtkSetMacro(SigmaDirection,int);
  vtkGetMacro(SigmaDirection,int);

  vtkSetMacro(InPlaneSigma, double);
  vtkGetMacro(InPlaneSigma, double);

  vtkSetMacro(OutOfPlaneSigma, double);
  vtkGetMacro(OutOfPlaneSigma, double);

  vtkSetMacro(RulerListVisibility,bool);
  vtkGetMacro(RulerListVisibility,bool);
  vtkBooleanMacro(RulerListVisibility,bool);

  vtkSetMacro(SigmaMin, double);
  vtkGetMacro(SigmaMin, double);

  vtkSetMacro(SigmaMax, double);
  vtkGetMacro(SigmaMax, double);
  
  vtkSetMacro(CorticalIntensityMin, double);
  vtkGetMacro(CorticalIntensityMin, double);
  
  vtkSetMacro(CorticalIntensityMax, double);
  vtkGetMacro(CorticalIntensityMax, double);

  vtkSetMacro(TrabecularIntensityMin, double);
  vtkGetMacro(TrabecularIntensityMin, double);

  vtkSetMacro(TrabecularIntensityMax, double);
  vtkGetMacro(TrabecularIntensityMax, double);

  vtkSetMacro(EdgeDetectionSlackFactor, double);
  vtkGetMacro(EdgeDetectionSlackFactor, double);

  vtkSetMacro(CorticalThicknessMin, double);
  vtkGetMacro(CorticalThicknessMin, double);

  vtkSetMacro(TrabecularThicknessMin, double);
  vtkGetMacro(TrabecularThicknessMin, double);

  //vtkSetMacro(VoxelBased,bool);
  //vtkGetMacro(VoxelBased,bool);
  //vtkBooleanMacro(VoxelBased,bool);

  //vtkSetMacro(InterpolationMode, int);
  //vtkGetMacro(InterpolationMode, int);

  //vtkSetMacro(SpacingScalingConst, double);
  //vtkGetMacro(SpacingScalingConst, double);

protected:
  vtkMRMLestimatePSF1ParametersNode();
  ~vtkMRMLestimatePSF1ParametersNode();

  vtkMRMLestimatePSF1ParametersNode(const vtkMRMLestimatePSF1ParametersNode&);
  void operator=(const vtkMRMLestimatePSF1ParametersNode&);

  char *InputVolumeNodeID;
  //char *OutputVolumeNodeID;
  char *RulerListNodeID;

  bool RulerListVisibility;
  int SigmaDirection;
  double InPlaneSigma;
  double OutOfPlaneSigma;
  double SigmaMin;
  double SigmaMax;
  double CorticalIntensityMin;
  double CorticalIntensityMax;
  double TrabecularIntensityMin;
  double TrabecularIntensityMax;
  double EdgeDetectionSlackFactor;
  double CorticalThicknessMin;
  double TrabecularThicknessMin;
  //bool VoxelBased;
  //int InterpolationMode;
  //bool IsotropicResampling;
  //double SpacingScalingConst;
};

#endif

