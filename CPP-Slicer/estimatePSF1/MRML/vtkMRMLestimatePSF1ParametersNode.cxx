/*=auto=========================================================================

Portions (c) Copyright 2005 Brigham and Women\"s Hospital (BWH) All Rights Reserved.

See COPYRIGHT.txt
or http://www.slicer.org/copyright/copyright.txt for details.

Program:   3D Slicer
Module:    $RCSfile: vtkMRMLestimatePSF1ParametersNode.cxx,v $
Date:      $Date: 2006/03/17 15:10:10 $
Version:   $Revision: 1.2 $

=========================================================================auto=*/

// VTK includes
#include <vtkCommand.h>
#include <vtkObjectFactory.h>

// MRML includes
#include "vtkMRMLVolumeNode.h"

// CropModuleMRML includes
#include "vtkMRMLestimatePSF1ParametersNode.h"

// AnnotationModuleMRML includes
#include "vtkMRMLAnnotationRulerNode.h"
#include "vtkMRMLAnnotationHierarchyNode.h"

// STD includes

//----------------------------------------------------------------------------
vtkMRMLNodeNewMacro(vtkMRMLestimatePSF1ParametersNode);

//----------------------------------------------------------------------------
vtkMRMLestimatePSF1ParametersNode::vtkMRMLestimatePSF1ParametersNode()
{
  this->HideFromEditors = 1;

  this->InputVolumeNodeID = NULL;
  //this->OutputVolumeNodeID = NULL;
  this->RulerListNodeID = NULL;

  this->RulerListVisibility = false;
  SigmaDirection = 0;
  InPlaneSigma = 0;
  OutOfPlaneSigma = 0;
  SigmaMin = 0.2;
  SigmaMax = 2;
  CorticalIntensityMin = 1100;
  CorticalIntensityMax = 2200;
  TrabecularIntensityMin = 0;
  TrabecularIntensityMax = 800;
  EdgeDetectionSlackFactor = 1;
  CorticalThicknessMin = 0.1;
  TrabecularThicknessMin = 0.5;
  /*this->InterpolationMode = 2;

  this->VoxelBased = false;

  this->SpacingScalingConst = 1.;*/
}

//----------------------------------------------------------------------------
vtkMRMLestimatePSF1ParametersNode::~vtkMRMLestimatePSF1ParametersNode()
{
  if (this->InputVolumeNodeID)
    {
    this->SetInputVolumeNodeID(NULL);
    }

  /*if (this->OutputVolumeNodeID)
    {
    this->SetOutputVolumeNodeID(NULL);
    }*/

  if (this->RulerListNodeID)
    {
    this->SetRulerListNodeID(NULL);
    }
}

//----------------------------------------------------------------------------
void vtkMRMLestimatePSF1ParametersNode::ReadXMLAttributes(const char** atts)
{
  std::cerr << "Reading estimatePSF1 param node!" << std::endl;
  Superclass::ReadXMLAttributes(atts);

  const char* attName;
  const char* attValue;
  while (*atts != NULL)
  {
    attName = *(atts++);
    attValue = *(atts++);
    if (!strcmp(attName, "inputVolumeNodeID"))
    {
      this->SetInputVolumeNodeID(attValue);
      continue;
    }
    /*if (!strcmp(attName, "outputVolumeNodeID"))
    {
      this->SetOutputVolumeNodeID(attValue);
      continue;
    }*/
    if (!strcmp(attName, "RulerListNodeID"))
    {
      this->SetRulerListNodeID(attValue);
      continue;
    }
    if (!strcmp(attName,"RulerListVisibility"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->RulerListVisibility;
      continue;
    }
	if (!strcmp(attName,"SigmaDirection"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->SigmaDirection;
      continue;
    }
	if (!strcmp(attName,"OutOfPlaneSigma"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->OutOfPlaneSigma;
      continue;
    }
	if (!strcmp(attName,"RulerListVisibility"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->RulerListVisibility;
      continue;
    }
	if (!strcmp(attName,"SigmaMin"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->SigmaMin;
      continue;
    }
	if (!strcmp(attName,"SigmaMax"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->SigmaMax;
      continue;
    }
	if (!strcmp(attName,"CorticalIntensityMin"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->CorticalIntensityMin;
      continue;
    }
	if (!strcmp(attName,"InPlaneSigma"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->InPlaneSigma;
      continue;
    }
	if (!strcmp(attName,"CorticalIntensityMax"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->CorticalIntensityMax;
      continue;
    }
	if (!strcmp(attName,"TrabecularIntensityMin"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->TrabecularIntensityMin;
      continue;
    }
	if (!strcmp(attName,"TrabecularIntensityMax"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->TrabecularIntensityMax;
      continue;
    }
	if (!strcmp(attName,"EdgeDetectionSlackFactor"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->EdgeDetectionSlackFactor;
      continue;
    }
	if (!strcmp(attName,"CorticalThicknessMin"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->CorticalThicknessMin;
      continue;
    }
	if (!strcmp(attName,"TrabecularThicknessMin"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->TrabecularThicknessMin;
      continue;
    }
    /*if (!strcmp(attName,"inter4polationMode"))
    {
      std::stringstream ss;
      ss << attValue;
      ss >> this->InterpolationMode;
      continue;
    }*/
  }

  this->WriteXML(std::cout,1);
}

//----------------------------------------------------------------------------
void vtkMRMLestimatePSF1ParametersNode::WriteXML(ostream& of, int nIndent)
{
  Superclass::WriteXML(of, nIndent);

  vtkIndent indent(nIndent);

  of << indent << " inputVolumeNodeID=\"" << (this->InputVolumeNodeID ? this->InputVolumeNodeID : "NULL") << "\"";
  //of << indent << " outputVolumeNodeID=\"" << (this->OutputVolumeNodeID ? this->OutputVolumeNodeID : "NULL") << "\"";
  of << indent << " RulerListVisibility=\""<< this->RulerListVisibility << "\"";
  of << indent << " RulerListNodeID=\"" << (this->RulerListNodeID ? this->RulerListNodeID : "NULL") << "\"";
  //of << indent << " interpolationMode=\"" << this->InterpolationMode << "\"";
}

//----------------------------------------------------------------------------
// Copy the node\"s attributes to this object.
// Does NOT copy: ID, FilePrefix, Name, SliceID
void vtkMRMLestimatePSF1ParametersNode::Copy(vtkMRMLNode *anode)
{
  Superclass::Copy(anode);
  vtkMRMLestimatePSF1ParametersNode *node = vtkMRMLestimatePSF1ParametersNode::SafeDownCast(anode);
  this->DisableModifiedEventOn();

  this->SetInputVolumeNodeID(node->GetInputVolumeNodeID());
  //this->SetOutputVolumeNodeID(node->GetOutputVolumeNodeID());
  this->SetRulerListNodeID(node->GetRulerListNodeID());
  //this->SetInterpolationMode(node->GetInterpolationMode());
  this->SetRulerListVisibility(node->GetRulerListVisibility());

  this->DisableModifiedEventOff();
  this->InvokePendingModifiedEvent();
}

//----------------------------------------------------------------------------
void vtkMRMLestimatePSF1ParametersNode::PrintSelf(ostream& os, vtkIndent indent)
{
  Superclass::PrintSelf(os,indent);

  os << "InputVolumeNodeID: " << ( (this->InputVolumeNodeID) ? this->InputVolumeNodeID : "None" ) << "\n";
  //os << "OutputVolumeNodeID: " << ( (this->OutputVolumeNodeID) ? this->OutputVolumeNodeID : "None" ) << "\n";
  os << "RulerListNodeID: " << ( (this->RulerListNodeID) ? this->RulerListNodeID : "None" ) << "\n";
  os << "RulerListVisibility: " << this->RulerListVisibility << "\n";
  //os << "InterpolationMode: " << this->InterpolationMode << "\n";
  //os << "IsotropicResampling: " << this->IsotropicResampling << "\n";
}

// End
