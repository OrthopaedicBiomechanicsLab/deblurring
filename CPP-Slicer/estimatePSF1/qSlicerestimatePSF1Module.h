#ifndef __qSlicerestimatePSF1Module_h
#define __qSlicerestimatePSF1Module_h
// SlicerQt includes
#include "qSlicerLoadableModule.h"
#include "qSlicerestimatePSF1ModuleExport.h"
class qSlicerestimatePSF1ModulePrivate;

/// \ingroup Slicer_QtModules_ExtensionTemplate
class Q_SLICER_QTMODULES_ESTIMATEPSF1_EXPORT qSlicerestimatePSF1Module : 
public qSlicerLoadableModule
{
  Q_OBJECT
  Q_INTERFACES(qSlicerLoadableModule);
public:
  typedef qSlicerLoadableModule Superclass;
  explicit qSlicerestimatePSF1Module(QObject *parent=0);
  virtual ~qSlicerestimatePSF1Module();
  qSlicerGetTitleMacro(QTMODULE_TITLE);
  virtual QIcon icon()const;
  virtual QStringList categories()const;
  virtual QString helpText()const;
  virtual QString acknowledgementText()const;
  virtual QStringList contributors()const;
  virtual QStringList dependencies() const;
protected:
  /// Initialize the module. Register the volumes reader/writer
  virtual void setup();
  /// Create and return the widget representation associated to this module
  virtual qSlicerAbstractModuleRepresentation * createWidgetRepresentation();

  /// Create and return the logic associated to this module
  virtual vtkMRMLAbstractLogic* createLogic();
protected:
  QScopedPointer<qSlicerestimatePSF1ModulePrivate> d_ptr;
private:
  Q_DECLARE_PRIVATE(qSlicerestimatePSF1Module);
  Q_DISABLE_COPY(qSlicerestimatePSF1Module);
};

#endif
