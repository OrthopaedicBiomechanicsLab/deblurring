/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

// Qt includes
#include <QDebug>

// CTK includes
#include <ctkFlowLayout.h>

// SlicerQt includes
#include "qSlicerestimatePSF1Module.h"
#include "qSlicerestimatePSF1ModuleWidget.h"
#include "ui_qSlicerestimatePSF1ModuleWidget.h"

// CTK includes
#include <ctkFlowLayout.h>

#include "vtkCollection.h"
#include <vtkLineSource.h>

#include "vtkSlicerestimatePSF1Logic.h"

#include <qSlicerAbstractCoreModule.h>

// qMRML includes
#include <qMRMLNodeFactory.h>

// MRMLAnnotation includes

#include <vtkMRMLAnnotationRulerNode.h>
#include <vtkMRMLAnnotationHierarchyNode.h>

// MRMLLogic includes
#include <vtkMRMLApplicationLogic.h>


#include <vtkMRMLestimatePSF1ParametersNode.h>

//-----------------------------------------------------------------------------
/// \ingroup Slicer_QtModules_ExtensionTemplate
class qSlicerestimatePSF1ModuleWidgetPrivate: public Ui_qSlicerestimatePSF1ModuleWidget
{
  Q_DECLARE_PUBLIC(qSlicerestimatePSF1ModuleWidget);
protected:
  qSlicerestimatePSF1ModuleWidget* const q_ptr;
public:
  qSlicerestimatePSF1ModuleWidgetPrivate(qSlicerestimatePSF1ModuleWidget& object);
  ~qSlicerestimatePSF1ModuleWidgetPrivate();

  vtkSlicerestimatePSF1Logic* logic() const;
};

//-----------------------------------------------------------------------------
// qSlicerestimatePSF1ModuleWidgetPrivate methods

//-----------------------------------------------------------------------------
qSlicerestimatePSF1ModuleWidgetPrivate::qSlicerestimatePSF1ModuleWidgetPrivate(qSlicerestimatePSF1ModuleWidget& object) : q_ptr(&object)
{
}
qSlicerestimatePSF1ModuleWidgetPrivate::~qSlicerestimatePSF1ModuleWidgetPrivate()
{
}
vtkSlicerestimatePSF1Logic* qSlicerestimatePSF1ModuleWidgetPrivate::logic() const
{
  Q_Q(const qSlicerestimatePSF1ModuleWidget);
  return vtkSlicerestimatePSF1Logic::SafeDownCast(q->logic());
}
//-----------------------------------------------------------------------------
// qSlicerestimatePSF1ModuleWidget methods

//-----------------------------------------------------------------------------
qSlicerestimatePSF1ModuleWidget::qSlicerestimatePSF1ModuleWidget(QWidget* _parent)
  : Superclass( _parent )
  , d_ptr( new qSlicerestimatePSF1ModuleWidgetPrivate(*this) )
{
	this->parametersNode = NULL;
}

//-----------------------------------------------------------------------------
qSlicerestimatePSF1ModuleWidget::~qSlicerestimatePSF1ModuleWidget()
{
}

//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::setup()
{
  Q_D(qSlicerestimatePSF1ModuleWidget);
  d->setupUi(this);
  this->Superclass::setup();

  //connectiing signals and slots
  connect(d->InputRulerListComboBox->nodeFactory(), SIGNAL(nodeInitialized(vtkMRMLNode*)),
          this, SLOT(initializeNode(vtkMRMLNode*)));

  connect(d->EstimateButton, SIGNAL(clicked()),
          this, SLOT(onApply()) );
  connect(d->InputVolumeComboBox, SIGNAL(currentNodeChanged(vtkMRMLNode*)),
          this, SLOT(onInputVolumeChanged()));
  connect(d->InputRulerListComboBox, SIGNAL(currentNodeChanged(vtkMRMLNode*)),
          this, SLOT(onInputRulerListChanged()));
  //connect(d->VisibilityButton, SIGNAL(toggled(bool)),
  //        this, SLOT(onRulerListVisibilityChanged()));
}
//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::enter()
{
  this->onInputVolumeChanged();
  this->onInputRulerListChanged();

  this->Superclass::enter();
}
//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::setMRMLScene(vtkMRMLScene* scene)
{

  this->Superclass::setMRMLScene(scene);
  if(scene == NULL)
    {
    return;
    }

  this->initializeParameterNode(scene);

  this->updateWidget();
}
//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::initializeParameterNode(vtkMRMLScene* scene)
{
	#ifndef __commentOutVTK
  vtkCollection* parameterNodes = scene->GetNodesByClass("vtkMRMLestimatePSF1ParametersNode");

  if(parameterNodes->GetNumberOfItems() > 0)
    {
    this->parametersNode = vtkMRMLestimatePSF1ParametersNode::SafeDownCast(parameterNodes->GetItemAsObject(0));
    if(!this->parametersNode)
      {
      qCritical() << "FATAL ERROR: Cannot instantiate estimatePSF1ParameterNode";
      Q_ASSERT(this->parametersNode);
      }
    }
  else
    {
    qDebug() << "No estimatePSF1 parameter nodes found!";
    this->parametersNode = vtkMRMLestimatePSF1ParametersNode::New();
    scene->AddNode(this->parametersNode);
    this->parametersNode->Delete();
    }

  parameterNodes->Delete();
#endif
}
void qSlicerestimatePSF1ModuleWidget::initializeNode(vtkMRMLNode *n)
{
	#ifndef __commentOutVTK
  vtkMRMLScene* scene = qobject_cast<qMRMLNodeFactory*>(this->sender())->mrmlScene();
  vtkMRMLAnnotationRulerNode::SafeDownCast(n)->Initialize(scene);
#endif
}

//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::onApply(){

  Q_D(const qSlicerestimatePSF1ModuleWidget);

  #ifndef __commentOutVTKSlicerestimatePSF1Logic
  #ifndef __commentOutVTK
  vtkSlicerestimatePSF1Logic *logic = d->logic();
  #endif
#endif

  if(!this->parametersNode || !d->InputVolumeComboBox->currentNode() ||
     !d->InputRulerListComboBox->currentNode())
    return;

  //rather than only updating the volumeID and RulerListID, update everything
  this->updateParameters();
  //this->parametersNode->SetInputVolumeNodeID(d->InputVolumeComboBox->currentNode()->GetID());
  //this->parametersNode->SetRulerListNodeID(d->InputRulerListComboBox->currentNode()->GetID());

#ifndef __commentOutVTKSlicerestimatePSF1Logic
#ifndef __commentOutVTK
  if(!logic->Apply(this->parametersNode))
    {
    std::cerr << "Propagating to the selection node" << std::endl;
    vtkSlicerApplicationLogic *appLogic = this->module()->appLogic();
    vtkMRMLSelectionNode *selectionNode = appLogic->GetSelectionNode();
    appLogic->PropagateVolumeSelection();
    }
 #endif
#endif
  this->updateWidget();
}

//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::onInputVolumeChanged()
{
  Q_D(qSlicerestimatePSF1ModuleWidget);
  Q_ASSERT(d->InputVolumeComboBox);
  
  
  /*vtkMRMLNode* node = d->InputVolumeComboBox->currentNode();
  if(node)
    {
    if(d->VoxelBasedModeRadioButton->isChecked())
      {
      if(d->checkForVolumeParentTransform())
        {
        d->showUnsupportedTransVolumeVoxelCroppingDialog();
        d->InputVolumeComboBox->setCurrentNode(NULL);
        }
      else
        {
        d->performROIVoxelGridAlignment();
        }
      }
    }*/
}

//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::onInputRulerListChanged()
{
  Q_D(qSlicerestimatePSF1ModuleWidget);

#ifndef __commentOutVTK
  vtkMRMLAnnotationHierarchyNode* node =
    vtkMRMLAnnotationHierarchyNode::SafeDownCast(d->InputRulerListComboBox->currentNode());
#endif
  /*if(node)
    {
    if(d->VoxelBasedModeRadioButton->isChecked())
      d->performROIVoxelGridAlignment();
    }*/
}


//-----------------------------------------------------------------------------
/*void qSlicerestimatePSF1ModuleWidget::onRulerListVisibilityChanged()
{
  Q_D(qSlicerestimatePSF1ModuleWidget);
  if(!this->parametersNode)
    {
    return;
    }
  //this->parametersNode->SetRulerListVisibility(d->VisibilityButton->isChecked());
  
  #ifndef __commentOutVTK
  vtkMRMLAnnotationHierarchyNode * node =
    vtkMRMLAnnotationHierarchyNode ::SafeDownCast(d->InputRulerListComboBox->currentNode());
  if (node)
    {
		//Essentially, since I am wanting to connect to a hierchy node that contains a number of rulers, 
		//I get each ruler and change its visability accordingly.  I am certain there is a more elegent solution uting vtk iterators
		vtkCollection* rulerListCollection=0;
		node->GetAllChildren(rulerListCollection);
		int numberOfPotentialRulers;
		numberOfPotentialRulers = rulerListCollection->GetNumberOfItems();

		for (int rulerIndex = 0; rulerIndex < numberOfPotentialRulers; rulerIndex++)
		{
			vtkMRMLAnnotationRulerNode* currentRulerNode =
				vtkMRMLAnnotationRulerNode::SafeDownCast(rulerListCollection->GetItemAsObject(rulerIndex));
			if (currentRulerNode)
			{
				currentRulerNode->SetDisplayVisibility(d->VisibilityButton->isChecked());
			}
		}

		
    }
#endif
  
}

*/
//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::onEndCloseEvent()
{
  this->initializeParameterNode(this->mrmlScene());
}

//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::updateParameters()
{
  Q_D(qSlicerestimatePSF1ModuleWidget);
  if(!this->parametersNode)
    return;

  #ifndef __commentOutVTK
  vtkMRMLestimatePSF1ParametersNode *pNode = this->parametersNode;

  vtkMRMLNode *volumeNode = d->InputVolumeComboBox->currentNode();
  vtkMRMLNode *RulerListNode = d->InputRulerListComboBox->currentNode();

  if(volumeNode)
    pNode->SetInputVolumeNodeID(volumeNode->GetID());
  else
    pNode->SetInputVolumeNodeID(NULL);


  if(RulerListNode)
    pNode->SetRulerListNodeID(RulerListNode->GetID());
  else
    pNode->SetRulerListNodeID(NULL);

  //pNode->SetRulerListVisibility(d->VisibilityButton->isChecked());
  
  if(d->InSigmaDirectionRadioButton->isChecked())
    pNode->SetSigmaDirection(0);
  else if(d->OutSigmaDirectionRadioButton->isChecked())
    pNode->SetSigmaDirection(1);


  pNode->SetInPlaneSigma(d->InPlaneSigmaSpinBox->value());
  pNode->SetOutOfPlaneSigma(d->OutOfPlaneSigmaSpinBox->value());

  pNode->SetSigmaMin(d->SigmaMinSpinBox->value());
  pNode->SetSigmaMax(d->SigmaMaxSpinBox->value());
  
  pNode->SetCorticalIntensityMin(    d->CorticalIntensityMinSpinBox->value()     );
  pNode->SetCorticalIntensityMax(    d->CorticalIntensityMaxSpinBox->value()     );
  pNode->SetTrabecularIntensityMin(  d->TrabecularIntensityMinSpinBox->value()   );
  pNode->SetTrabecularIntensityMax(  d->TrabecularIntensityMaxSpinBox->value()   );
  pNode->SetEdgeDetectionSlackFactor(d->EdgeDetectionSlackFactorSpinBox->value() );
  pNode->SetCorticalThicknessMin(    d->CorticalThicknessMinSpinBox->value()     );
  pNode->SetTrabecularThicknessMin(  d->TrabecularThicknessMinSpinBox->value()   );


#endif
  /*if(d->NNRadioButton->isChecked())
    pNode->SetInterpolationMode(1);
  else if(d->LinearRadioButton->isChecked())
    pNode->SetInterpolationMode(2);
  else if(d->WSRadioButton->isChecked())
    pNode->SetInterpolationMode(3);
  else if(d->BSRadioButton->isChecked())
    pNode->SetInterpolationMode(4);

  if(d->IsotropicCheckbox->isChecked())
    pNode->SetIsotropicResampling(1);

  pNode->SetSpacingScalingConst(d->SpacingScalingSpinBox->value());*/
}

//-----------------------------------------------------------------------------
void qSlicerestimatePSF1ModuleWidget::updateWidget()
{
	//update parameters based on the parameter node
  Q_D(qSlicerestimatePSF1ModuleWidget);
  if(!this->parametersNode || !this->mrmlScene())
    {
    return;
    }
  #ifndef __commentOutVTK
  vtkMRMLestimatePSF1ParametersNode *parameterNode = this->parametersNode;

  vtkMRMLNode *volumeNode = this->mrmlScene()->GetNodeByID(parameterNode->GetInputVolumeNodeID());
  vtkMRMLNode *RulerListNode = this->mrmlScene()->GetNodeByID(parameterNode->GetRulerListNodeID());

  if(volumeNode)
    d->InputVolumeComboBox->setCurrentNode(volumeNode);
  if(RulerListNode)
    d->InputRulerListComboBox->setCurrentNode(RulerListNode);

  //d->VisibilityButton->setChecked(parameterNode->GetRulerListVisibility());
  /*switch(parameterNode->GetInterpolationMode())
    {
    case 1: d->NNRadioButton->setChecked(1); break;
    case 2: d->LinearRadioButton->setChecked(1); break;
    case 3: d->WSRadioButton->setChecked(1); break;
    case 4: d->BSRadioButton->setChecked(1); break;
    }*/
  //d->IsotropicCheckbox->setChecked(parameterNode->GetIsotropicResampling());

  //d->VisibilityButton->setChecked(parameterNode->GetRulerListVisibility());

  //making sure that data flows to and from the paramter node
  /*if(d->InSigmaDirectionRadioButton->isChecked())
    pNode->SetSigmaDirection(0);
  else if(d->OutSigmaDirectionRadioButton->isChecked())
    pNode->SetSigmaDirection(1);


  
  pNode->SetSigmaMin(d->SigmaMinSpinBox->value());
  pNode->SetSigmaMax(d->SigmaMaxSpinBox->value());
  
  pNode->SetCorticalIntensityMin(    d->CorticalIntensityMinSpinBox->value()     );
  pNode->SetCorticalIntensityMax(    d->CorticalIntensityMaxSpinBox->value()     );
  pNode->SetTrabecularIntensityMin(  d->TrabecularIntensityMinSpinBox->value()   );
  pNode->SetTrabecularIntensityMax(  d->TrabecularIntensityMaxSpinBox->value()   );
  pNode->SetEdgeDetectionSlackFactor(d->EdgeDetectionSlackFactorSpinBox->value() );
  pNode->SetCorticalThicknessMin(    d->CorticalThicknessMinSpinBox->value()     );
  pNode->SetTrabecularThicknessMin(  d->TrabecularThicknessMinSpinBox->value()   );*/
  
  d->InPlaneSigmaSpinBox->setValue(parameterNode->GetInPlaneSigma());
  d->OutOfPlaneSigmaSpinBox->setValue(parameterNode->GetOutOfPlaneSigma());
  


  //d->SpacingScalingSpinBox->setValue(parameterNode->GetSpacingScalingConst());
#endif
}