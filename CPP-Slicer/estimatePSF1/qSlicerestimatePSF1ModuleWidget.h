/*==============================================================================

  Program: 3D Slicer

  Portions (c) Copyright Brigham and Women's Hospital (BWH) All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

==============================================================================*/

#ifndef __qSlicerestimatePSF1ModuleWidget_h
#define __qSlicerestimatePSF1ModuleWidget_h

// SlicerQt includes
#include "qSlicerAbstractModuleWidget.h"

#include "qSlicerestimatePSF1ModuleExport.h"

class qSlicerestimatePSF1ModuleWidgetPrivate;
class vtkMRMLNode;
class vtkMRMLestimatePSF1ParametersNode;

/// \ingroup Slicer_QtModules_ExtensionTemplate
class Q_SLICER_QTMODULES_ESTIMATEPSF1_EXPORT qSlicerestimatePSF1ModuleWidget :
  public qSlicerAbstractModuleWidget
{
  Q_OBJECT

public:

  typedef qSlicerAbstractModuleWidget Superclass;
  qSlicerestimatePSF1ModuleWidget(QWidget *parent=0);
  virtual ~qSlicerestimatePSF1ModuleWidget();

public slots:


protected:
  QScopedPointer<qSlicerestimatePSF1ModuleWidgetPrivate> d_ptr;

  virtual void setup();
  virtual void enter();
  virtual void setMRMLScene(vtkMRMLScene*);
  void initializeParameterNode(vtkMRMLScene*);

protected slots:
  void initializeNode(vtkMRMLNode*);
  void onInputVolumeChanged();
  void onInputRulerListChanged();
  //void onRulerListVisibilityChanged();
  void onApply();
  void updateWidget();
  void updateParameters();
  void onEndCloseEvent();

private:
  Q_DECLARE_PRIVATE(qSlicerestimatePSF1ModuleWidget);
  Q_DISABLE_COPY(qSlicerestimatePSF1ModuleWidget);
  vtkMRMLestimatePSF1ParametersNode *parametersNode;
};

#endif
