import os
from __main__ import vtk, qt, ctk, slicer

from DeblurringGuideletLib import Guidelet, GuideletLoadable, GuideletLogic, GuideletWidget
# from Guidelet import Guidelet
import logging
import time


class DeblurringGuidelet(GuideletLoadable):
  """Uses GuideletLoadable class, available at:
  """

  def __init__(self, parent):
    GuideletLoadable.__init__(self, parent)
    self.parent.title = "Deblurring Guidelet"
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["YOUR NAME"]
    self.parent.helpText = """ SOME HELP AND A LINK TO YOUR WEBSITE """
    self.parent.acknowledgementText = """ THANKS TO ... """


class DeblurringGuideletWidget(GuideletWidget):
  """Uses GuideletWidget base class, available at:
  """

  def __init__(self, parent = None):
    GuideletWidget.__init__(self, parent)


  def setup(self):
    GuideletWidget.setup(self)

  def addLauncherWidgets(self):
    # GuideletWidget.addLauncherWidgets(self)
    return

  def createGuideletInstance(self):
    return DeblurringGuideletGuidelet(None, self.guideletLogic, self.selectedConfigurationName)


  def createGuideletLogic(self):
    return DeblurringGuideletLogic()

class DeblurringGuideletLogic(GuideletLogic):
  """Uses GuideletLogic base class, available at:
  """ #TODO add path


  def __init__(self, parent = None):
    GuideletLogic.__init__(self, parent)



class DeblurringGuideletGuidelet(Guidelet):

  def __init__(self, parent, logic, configurationName='Default'):

    Guidelet.__init__(self, parent, logic, configurationName)
    logging.debug('DeblurringGuideletGuidelet.__init__')

    moduleDirectoryPath = slicer.modules.deblurringguidelet.path.replace('DeblurringGuidelet.py', '')

    # Set up main frame.

    

    self.sliceletDockWidget.setObjectName('DeblurringGuideletPanel')
    self.sliceletDockWidget.setWindowTitle('Deblurring Plan Generation Workflow')
    self.mainWindow.setWindowTitle('DeblurringGuidelet')
    print "\n\nModule DIRECTORY PATH", moduleDirectoryPath
    self.mainWindow.windowIcon = qt.QIcon(moduleDirectoryPath + '/Resources/Icons/DeblurringIcon.png')

    self.setupScene()

    # self.navigationView = self.VIEW_ULTRASOUND_3D

    # Setting button open on startup.
    # self.calibrationCollapsibleButton.setProperty('collapsed', False)


  def setupScene(self): #applet specific
    print "setup scene"
    logging.debug('setupScene')
    self.sliceletDockWidget.show()
    self.showToolbars(True)
    self.showModulePanel(True)
    self.showMenuBar(True)
    slicer.util.mainWindow().showMaximized()

    # Save current state
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'true')



  def getViewNode(self, viewName):
    """
    Get the view node for the selected 3D view
    """
    viewNode = slicer.util.getNode(viewName)
    return viewNode


  def updateNavigationView(self):
    self.selectView(self.navigationView)

    # Reset orientation marker
    if hasattr(slicer.vtkMRMLViewNode(),'SetOrientationMarkerType'): # orientation marker is not available in older Slicer versions
      v1=slicer.util.getNode('View1')
      v1.SetOrientationMarkerType(v1.OrientationMarkerTypeNone)
