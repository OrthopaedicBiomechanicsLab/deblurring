#-----------------------------------------------------------------------------
set(MODULE_NAME DeblurringGuidelet)

#-----------------------------------------------------------------------------
set(MODULE_PYTHON_SCRIPTS
  ${MODULE_NAME}.py
  )

set(MODULE_PYTHON_RESOURCES
  Resources/Icons/DeblurringIcon.png
  Resources/Icons/ExampleGuidelet.png
  Resources/Icons/fullscreen.png
  Resources/Icons/Guidelet.png
  Resources/Icons/minimize.png

  )
#-----------------------------------------------------------------------------
slicerMacroBuildScriptedModule(
  NAME ${MODULE_NAME}
  SCRIPTS ${MODULE_PYTHON_SCRIPTS}
  RESOURCES ${MODULE_PYTHON_RESOURCES}
  )