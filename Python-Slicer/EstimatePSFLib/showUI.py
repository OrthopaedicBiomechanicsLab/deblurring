# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 15:46:55 2016

@author: hardisty
"""
from PythonQt import QtGui
import sys
# import qSlicerestimatePSF1ModuleWidget
import qt
import os
#app = QtGui.QApplication(sys.argv)
'''
MainWindow = QtGui.QMainWindow()
ui = qSlicerestimatePSF1ModuleWidget.Ui_qSlicerestimatePSF1ModuleWidget()
ui.setupUi(MainWindow)
MainWindow.show()
'''

dirPath = os.path.dirname(os.path.realpath(__file__))
lastDirectoryPath = dirPath.rfind('\\')
# pathList = dirPath.split('\\')
#Python-Slicer is the first divergence in file paths. This might change if slicer is built.
qtUIFilePath = dirPath[:] + '\Resources\UI\qSlicerestimatePSF1ModuleWidget.ui'
print "qtUIFilePath from showUI: ", qtUIFilePath
f = qt.QFile(qtUIFilePath)

f.open(qt.QFile.ReadOnly)
loader = qt.QUiLoader()
estimatePSFWidgetDirectUi = loader.load(f)
f.close()
estimatePSFWidgetDirectUi.show()