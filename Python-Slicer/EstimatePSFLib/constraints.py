# Autogenerated with SMOP version 
# main.py C:\Mike\Git\deblurring\Matlab\estimateGaussianPSF\constraints.m --output=C:\Mike\Git\deblurring\Matlab\estimateGaussianPSF\constraints.py

from __future__ import division
#try:
#    from runtime import *
#except ImportError:
#    from smop.runtime import *

import math
import numpy as np
#from scipy.special import erf 

def erf(x):
    # save the sign of x
    sign = x/abs(x)
    x = abs(x)

    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911

    # A&S formula 7.1.26
    t = 1.0/(1.0 + p*x)
    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*np.exp(-x*x)
    return sign*y # erf(-x) = -erf(x)
    

def inequalityConstraints(x=None,profileNumber=None,n=None,nn=None,A_CT=None,t=None,y_b=None,e_1_i=None,e_2_i=None,min_cortthick=None,min_trabthick=None,inplanesig=None,angles=None,inplanemode=None,*args,**kwargs):
    #constraints.inequalityConstraints(x0,0,n,profileCount,A_CT,t,y_b,e_1_i,e_2_i,min_cortthick,min_trabthick,0,0,true)
    #varargin = cellarray(args)
    #nargin = 13-[x,n,nn,A_CT,t,y_b,e_1_i,e_2_i,min_cortthick,min_trabthick,inplanesig,angles,inplanemode].count(None)+len(args)

    k=1
    #c=matlabarray([])
    #ceq=matlabarray([])
    #for j in range(nn):
    j=profileNumber
    if n[j] == 3:
        k=k + 9*j
    else:
        k=k + 5*j
        
    if inplanemode == 1:
        sig=x[0]
    else:
        sig=((((math.cos(angles[j])) ** 2) * inplanesig ** 2) + (((math.sin(angles[j])) ** 2) * x[0] ** 2)) ** 0.5
    if n[j] == 3:
        xsub=x[k:k + 9]
    else:
        xsub=x[k:k + 5]
    
    Y = [xsub[i] for i in range(n[j])]
    X = [xsub[i + n[j]] for i in range(n[j]+1)]
    
    y_0=xsub[-2]
    y_f=xsub[-1]
    
    if n[j] == 3:
        c=[X[0] - X[1] + min_cortthick,X[1] - X[2] + min_trabthick,X[2] - X[3] + min_cortthick]
    else:
        c=X[0] - X[1] + min_cortthick
    return c

def equalityConstraints(x=None, profileNumber=None,n=None,nn=None,A_CT=None,t=None,y_b=None,e_1_i=None,e_2_i=None,min_cortthick=None,min_trabthick=None,inplanesig=None,angles=None,inplanemode=None,*args,**kwargs):            
    #constraints.equalityConstraints(x0,0,n,profileCount,A_CT,t,y_b,e_1_i,e_2_i,min_cortthick,min_trabthick,0,0,true)
    k=1
    
    j=profileNumber
    if n[j] == 3:
        k=k + 9*j
    else:
        k=k + 5*j
        
    if inplanemode == 1:
        sig=x[0]
    else:
        sig=((((math.cos(angles[j])) ** 2) * inplanesig ** 2) + (((math.sin(angles[j])) ** 2) * x[0] ** 2)) ** 0.5
    if n[j] == 3:
        xsub=x[k:k + 9]
    else:
        xsub=x[k:k + 5]
    
    Y = [xsub[i] for i in range(n[j])]
    X = [xsub[i + n[j]] for i in range(n[j]+1)]
    
    y_0=xsub[-2]
    y_f=xsub[-1]
    h_0=0.5 * y_0 * ((erf((t[:,j] - min(t[:,j]) + 10) / (math.sqrt(2) * sig))) - (erf((t[:,j] - X[0]) / (math.sqrt(2) * sig))))
    h_f=0.5 * y_f * ((erf((t[:,j] - X[n[j]]) / (math.sqrt(2) * sig))) - (erf((t[:,j] - max(t[:,j]) - 10) / (math.sqrt(2) * sig))))
    h=h_0 + h_f
    for i in range(n[j]):
        h=h + 0.5 * Y[i] * ((erf((t[:,j] - X[i]) / (math.sqrt(2) * sig))) - (erf((t[:,j] - X[i + 1]) / (math.sqrt(2) * sig))))
    A=np.trapz(h[e_1_i[j]:e_2_i[j]] - y_b[j],t[e_1_i[j]:e_2_i[j],j])
    ceq=A - A_CT[j]
    return ceq
