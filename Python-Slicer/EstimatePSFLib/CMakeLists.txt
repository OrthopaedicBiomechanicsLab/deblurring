#-----------------------------------------------------------------------------
set(MODULE_NAME EstimatePSFLib)
string(TOUPPER ${MODULE_NAME} MODULE_NAME_UPPER)


#-----------------------------------------------------------------------------
set(MODULE_PYTHON_SCRIPTS
  __init__
  constraints
  estimateGaussianPSF
  nlopt
  objective
  showUI
  )

  set(MODULE_PYTHON_RESOURCES
  nlopt.dll
  _nlopt.pyd
  )
#-----------------------------------------------------------------------------
slicerMacroBuildScriptedModule(
  NAME ${MODULE_NAME}
  SCRIPTS ${MODULE_PYTHON_SCRIPTS}
  RESOURCES ${MODULE_PYTHON_RESOURCES}
  WITH_SUBDIR
  )