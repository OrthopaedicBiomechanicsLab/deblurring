import os
from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import logging

#
# GuideletLoadable
#

class GuideletLoadable(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "Guidelet"
    self.parent.categories = ["Guidelet"]
    self.parent.dependencies = []
    self.parent.contributors = [""]


#
# GuideletWidget
#

class GuideletWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent=None):
    ScriptedLoadableModuleWidget.__init__(self, parent)
    self.guideletInstance = None
    self.guideletLogic = self.createGuideletLogic()
    self.selectedConfigurationName = 'Default'

  def setup(self):
    self.developerMode = True
    ScriptedLoadableModuleWidget.setup(self)

    # # Launcher panel
    # launcherCollapsibleButton = ctk.ctkCollapsibleButton()
    # launcherCollapsibleButton.text = "Guidelet launcher"
    # self.layout.addWidget(launcherCollapsibleButton)
    # self.launcherFormLayout = q

    # self.addLauncherWidgets()

    # # Show guidelet button
    # self.launchGuideletButton = qt.QPushButton("Start "+self.moduleName)
    # self.launchGuideletButton.toolTip = "Launch the " + self.moduleName + " application in full screen"
    # self.layout.addWidget(self.launchGuideletButton)
    # self.launchGuideletButton.connect('clicked()', self.onLaunchGuideletButtonClicked)

    print "setup scene"
    logging.debug('setupScene')

    if not self.guideletInstance:
      self.guideletInstance = self.createGuideletInstance()
    self.guideletInstance.setupScene()
    # self.guideletInstance.show()
    # self.guideletInstance.showFullScreen()

    # self.sliceletDockWidget.show()
    # self.showToolbars(True)
    # self.showModulePanel(True)
    # self.showMenuBar(True)
    # slicer.util.mainWindow().showMaximized()

    # Save current state
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'true')

    # self.layout.addStretch(1)


  def cleanup(self):
    # self.launchGuideletButton.disconnect('clicked()', self.onLaunchGuideletButtonClicked)
    if self.guideletLogic:
      self.guideletLogic.cleanup()
    if self.guideletInstance:
      self.guideletInstance.cleanup()

  def onLaunchGuideletButtonClicked(self):
    logging.debug('onLaunchGuideletButtonClicked')

    if not self.guideletInstance:
      self.guideletInstance = self.createGuideletInstance()
    self.guideletInstance.setupScene()
    self.guideletInstance.showFullScreen()

  def createGuideletInstance(self):
    raise NotImplementedError("Abstract method must be overridden!")

  def createGuideletLogic(self):
    raise NotImplementedError("Abstract method must be overridden!")

#
# GuideletLogic
#

class GuideletLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent = None):
    ScriptedLoadableModuleLogic.__init__(self, parent)

    self.setupDefaultConfiguration()

  def createParameterNode(self):
    node = ScriptedLoadableModuleLogic.createParameterNode(self)
    return node

  def cleanup(self):
    pass

  def setupDefaultConfiguration(self):
    # settings = slicer.app.userSettings()
    # settings.beginGroup(self.moduleName + '/Configurations')
    # childs = settings.childGroups()
    # settings.endGroup()
    # if not 'Default' in childs:
    #   self.addValuesToDefaultConfiguration()
    pass

  # Adds a default configurations to Slicer.ini
  def addValuesToDefaultConfiguration(self):
    # moduleDir = os.path.dirname(__file__)
    # defaultSavePath = os.path.join(moduleDir, 'SavedScenes')

    # settingList = {'StyleSheet' : 'DefaultStyle.qss',
    #                'LiveUltrasoundNodeName' : 'Image_Reference',
    #                'LiveUltrasoundNodeName_Needle' : 'Image_Needle',
    #                'PlusServerHostNamePort' : 'localhost:18944',
    #                'RecordingFilenamePrefix' : 'GuideletRecording-',
    #                'RecordingFilenameExtension' : '.mhd',
    #                'SavedScenesDirectory' : defaultSavePath
    #                }
    # self.updateSettings(settingList, 'Default')
    pass


#
# GuideletTest
#

class GuideletTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """
  #TODO add common test utility methods here

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    # self.test_SliceletBase1() # Here the tests should be added that are common for all full screen applets e.g.  # TODO defines tests