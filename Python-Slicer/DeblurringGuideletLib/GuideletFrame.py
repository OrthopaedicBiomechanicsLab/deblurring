import os
from __main__ import vtk, qt, ctk, slicer
import logging
import time

# from UltraSound import UltraSound

class Guidelet(object):

  @staticmethod
  def showToolbars(show):
    for toolbar in slicer.util.mainWindow().findChildren('QToolBar'):
      toolbar.setVisible(show)

  def showModulePanel(self, show):
    modulePanelDockWidget = slicer.util.mainWindow().findChildren('QDockWidget','PanelDockWidget')[0]
    modulePanelDockWidget.setVisible(show)

    if show:
      mainWindow=slicer.util.mainWindow()
      if self.sliceletDockWidgetPosition == qt.Qt.LeftDockWidgetArea:
        mainWindow.tabifyDockWidget(modulePanelDockWidget, self.sliceletDockWidget)
        self.sliceletDockWidget.setFeatures(qt.QDockWidget.DockWidgetClosable+qt.QDockWidget.DockWidgetMovable+qt.QDockWidget.DockWidgetFloatable)
    else:
      if self.sliceletDockWidgetPosition == qt.Qt.LeftDockWidgetArea:
        # Prevent accidental closing or undocking of the slicelet's left panel
        self.sliceletDockWidget.setFeatures(0)

  @staticmethod
  def showMenuBar(show):
    for menubar in slicer.util.mainWindow().findChildren('QMenuBar'):
      menubar.setVisible(show)


  def __init__(self, parent, logic, configurationName='Default', sliceletDockWidgetPosition = qt.Qt.LeftDockWidgetArea):
    logging.debug('Guidelet.__init__')
    self.sliceletDockWidgetPosition = sliceletDockWidgetPosition
    self.parent = parent
    self.logic = logic
    self.configurationName = configurationName
    self.parameterNodeObserver = None
    self.parameterNode = self.logic.getParameterNode()
    self.layoutManager = slicer.app.layoutManager()


    self.sliceletDockWidget = qt.QDockWidget(self.parent)

    self.mainWindow=slicer.util.mainWindow()
    self.sliceletDockWidget.setParent(self.mainWindow)
    self.mainWindow.addDockWidget(self.sliceletDockWidgetPosition, self.sliceletDockWidget)
    self.sliceletPanel = qt.QFrame(self.sliceletDockWidget)
    self.sliceletPanelLayout = qt.QVBoxLayout(self.sliceletPanel)
    self.sliceletDockWidget.setWidget(self.sliceletPanel)

    self.setupPanel()
    self.fullScreenFlag = False
    self.LoadDataButton.connect('clicked(bool)', self.onLoadData)
    self.saveDataButton.connect('clicked(bool)', self.onSaveDataButton)
    self.showGuideletFullscreenButton.connect('clicked(bool)', self.onShowGuideletFullscreenButton)

    # self.hideGuideletFullscreenButton.connect('clicked(bool)', self.onShowFullSlicerInterfaceClicked)

  


  def setupPanel(self):

    deblurringWidget = slicer.modules.deblurringworkflow.createNewWidgetRepresentation() 
    self.sliceletPanelLayout.addWidget(deblurringWidget)

    viewDataCollapsibleButton = ctk.ctkCollapsibleButton()
    viewDataCollapsibleButton.setText("Scene Data")
    viewDataCollapsibleButton.collapsed = True
    treeViewFormLayout = qt.QFormLayout(viewDataCollapsibleButton)
    #set up tree viewer
    treeView = slicer.qMRMLTreeView()
    treeView.headerHidden = True
    treeView.setColumnWidth(0,230)
    treeView.setColumnWidth(1,5)
    treeView.sceneModelType = "ModelHierarchy"
    treeView.nodeTypes = ("vtkMRMLModelNode", "vtkMRMLModelHierarchyNode", "vtkMRMLScalarVolumeNode")
    sceneModel = treeView.sceneModel()
    sceneModel.setMRMLScene(slicer.mrmlScene)
    sceneModel.visibilityColumn = True
    sceneModel.idColumn = True
    treeViewFormLayout.addRow(treeView)
    self.sliceletPanelLayout.addWidget(viewDataCollapsibleButton)

    toolbarWidgetsLayout = qt.QHBoxLayout()
    self.sliceletPanelLayout.addLayout(toolbarWidgetsLayout)
    #make buttons to add
    self.showGuideletFullscreenButton = qt.QPushButton()
    self.saveDataButton = qt.QPushButton()
    self.LoadDataButton = qt.QPushButton()

    self.LoadDataButton.setText(" Load Data")
    self.saveDataButton.setText(" Save Data")
    self.showGuideletFullscreenButton.setText(" Fullscreen View")

    #set appearance of buttons
    #cna see Slicer\Modules\Loadable\SlicerWelcome\Resources\Images\ to see pngs
    moduleDirectoryPath = slicer.modules.deblurringguidelet.path.replace('DeblurringGuidelet.py', '')
    self.LoadDataButton.setIcon(qt.QIcon(":/Icons/Medium/SlicerLoadData.png")) 
    # self.LoadDataButton.setIcon(qt.QIcon(":/Icons/SubjectHierarchy.png")) 
    self.saveDataButton.setIcon(qt.QIcon(":/Icons/Medium/SlicerSave.png"))
    self.showGuideletFullscreenButton.setIcon(qt.QIcon(moduleDirectoryPath + "/Resources/Icons/fullscreen.png"))

    #add widgets to layout
    toolbarWidgetsLayout.addWidget(self.LoadDataButton)
    toolbarWidgetsLayout.addWidget(self.saveDataButton)
    toolbarWidgetsLayout.addWidget(self.showGuideletFullscreenButton)




    # self.hideGuideletFullscreenButton = qt.QPushButton()
    # self.hideGuideletFullscreenButton.setText("Return to Slicer View")
    # self.sliceletPanelLayout.addWidget(self.hideGuideletFullscreenButton)

  def loadStyleSheet(self):
    moduleDir = os.path.dirname(__file__)
    style = self.parameterNode.GetParameter('StyleSheet')
    styleFile = os.path.join(moduleDir, 'Resources', 'StyleSheets', style)
    f = qt.QFile(styleFile)
    if not f.exists():
      logging.debug("Unable to load stylesheet, file not found")
      return ""
    else:
      f.open(qt.QFile.ReadOnly | qt.QFile.Text)
      ts = qt.QTextStream(f)
      stylesheet = ts.readAll()
      return stylesheet


  def showFullScreen(self):

    # We hide all toolbars, etc. which is inconvenient as a default startup setting,
    # therefore disable saving of window setup.
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'false')

    self.showToolbars(False)
    self.showModulePanel(False)
    self.showMenuBar(False)

    self.sliceletDockWidget.show()

    mainWindow=slicer.util.mainWindow()
    mainWindow.showFullScreen()

  def onShowFullSlicerInterfaceClicked(self):
    self.showToolbars(True)
    self.showModulePanel(True)
    self.showMenuBar(True)
    slicer.util.mainWindow().showMaximized()

    # Save current state
    settings = qt.QSettings()
    settings.setValue('MainWindow/RestoreGeometry', 'true')

  def onLoadData(self):
    slicer.util.openAddDataDialog()

  def onSaveDataButton(self):
    slicer.util.openSaveDataDialog()

  def onShowGuideletFullscreenButton(self):
    if self.fullScreenFlag == False:
      self.showGuideletFullscreenButton.setText(" Slicer View")
      self.showFullScreen()
      self.fullScreenFlag = True

    else:
      self.showGuideletFullscreenButton.setText(" Fullscreen View")
      self.onShowFullSlicerInterfaceClicked()
      self.fullScreenFlag = False


