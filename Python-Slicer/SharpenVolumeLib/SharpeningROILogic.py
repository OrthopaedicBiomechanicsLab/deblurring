import os
import unittest
from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *

import math
import PythonQt

import SimpleITK as sitk
import sitkUtils

from vtk.util import numpy_support as VN
import numpy as np

class SharpeningROILogic(ScriptedLoadableModuleLogic):

  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget
  """

  def clipModelWithRoi(self,roiNode, modelNode, outputModelNode):
    bounds = [0,0,0,0,0,0]
    roiNode.GetRASBounds(bounds)
    boundingBox = vtk.vtkPlanes()
    boundingBox.SetBounds(bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5])
    clipper = vtk.vtkClipClosedSurface()
    # clipper.SetGenerateFaces(1)
    clipper.SetInputData(modelNode.GetPolyData())
    planesCollection = vtk.vtkPlaneCollection()

    for i in range(6):
      p = vtk.vtkPlane()
      boundingBox.GetPlane(i,p)
      normal = p.GetNormal()
      p.SetNormal([-1*normal[0],-1*normal[1],-1*normal[2]])
      planesCollection.AddItem(p)

    clipper.SetClippingPlanes(planesCollection)
    # clipper.InsideOutOn()
    clipper.Update()
    outputModelNode.SetAndObservePolyData(clipper.GetOutput())


  def deepCopyModel(self,model, modelName):

    polydata = model.GetPolyData()

    modelClone = slicer.vtkMRMLModelNode()
    modelClone.SetName(modelName)
    polydataClone = vtk.vtkPolyData()
    polydataClone.ShallowCopy(polydata)
    modelClone.SetAndObservePolyData(polydataClone)

    modelDisplayClone = slicer.vtkMRMLModelDisplayNode()
    slicer.mrmlScene.AddNode(modelDisplayClone)

    modelClone.SetAndObserveDisplayNodeID(modelDisplayClone.GetID())

    modelDisplayClone.SetVisibility(True)
    slicer.mrmlScene.AddNode(modelDisplayClone) #repeated

    modelClone.SetAndObserveDisplayNodeID(modelDisplayClone.GetID())

    modelDisplayClone.SetInputPolyDataConnection(modelClone.GetPolyDataConnection())
    slicer.mrmlScene.AddNode(modelClone)

    print "copied!"
    return modelClone

  def deepCopyVolume(self, volume, volumeName):
    volumesLogic = slicer.modules.volumes.logic()
    clonedVolumeNode = volumesLogic.CloneVolume(slicer.mrmlScene, volume, volumeName)
    return clonedVolumeNode

  def deepCopyROI(self, roi):
    radiusXYZ = [0,0,0]
    xyz = [0,0,0]
    roi.GetRadiusXYZ(radiusXYZ)
    roi.GetXYZ(xyz)

    newRoi = slicer.vtkMRMLAnnotationROINode()
    slicer.mrmlScene.AddNode(newRoi)
    newRoi.SetRadiusXYZ(radiusXYZ)
    newRoi.SetXYZ(xyz)

    return newRoi


  def clipVolumeWithRoi(self, volumeNode, roiNode, fillValue, clipOutsideSurface, outputVolume):



    # Create a box implicit function that will be used as a stencil to fill the volume
    roiBox = vtk.vtkBox()

    roiCenter = [0, 0, 0]
    roiNode.GetXYZ( roiCenter )
    roiRadius = [0, 0, 0]
    roiNode.GetRadiusXYZ( roiRadius )
    roiBox.SetBounds(roiCenter[0] - roiRadius[0], roiCenter[0] + roiRadius[0], roiCenter[1] - roiRadius[1], roiCenter[1] + roiRadius[1], roiCenter[2] - roiRadius[2], roiCenter[2] + roiRadius[2])

    # Determine the transform between the box and the image IJK coordinate systems
    rasToBox = vtk.vtkMatrix4x4()    
    if roiNode.GetTransformNodeID() != None:
      roiBoxTransformNode = slicer.mrmlScene.GetNodeByID(roiNode.GetTransformNodeID())
      boxToRas = vtk.vtkMatrix4x4()
      roiBoxTransformNode.GetMatrixTransformToWorld(boxToRas)
      rasToBox.DeepCopy(boxToRas)
      rasToBox.Invert()
      
    ijkToRas = vtk.vtkMatrix4x4()
    volumeNode.GetIJKToRASMatrix( ijkToRas )

    ijkToBox = vtk.vtkMatrix4x4()
    vtk.vtkMatrix4x4.Multiply4x4(rasToBox,ijkToRas,ijkToBox)
    ijkToBoxTransform = vtk.vtkTransform()
    ijkToBoxTransform.SetMatrix(ijkToBox)
    roiBox.SetTransform(ijkToBoxTransform)

    # Use the stencil to fill the volume
    imageData=volumeNode.GetImageData()
    
    # Convert the implicit function to a stencil
    functionToStencil = vtk.vtkImplicitFunctionToImageStencil()
    functionToStencil.SetInput(roiBox)
    functionToStencil.SetOutputOrigin(imageData.GetOrigin())
    functionToStencil.SetOutputSpacing(imageData.GetSpacing())
    functionToStencil.SetOutputWholeExtent(imageData.GetExtent())
    functionToStencil.Update()

    # Apply the stencil to the volume
    stencilToImage=vtk.vtkImageStencil()
    stencilToImage.SetInputData(imageData)
    stencilToImage.SetStencilData(functionToStencil.GetOutput())
    if clipOutsideSurface:
      stencilToImage.ReverseStencilOff()
    else:
      stencilToImage.ReverseStencilOn()
      
    stencilToImage.SetBackgroundValue(fillValue)
    stencilToImage.Update()

    # Update the volume with the stencil operation result
    outputImageData = vtk.vtkImageData()
    outputImageData.DeepCopy(stencilToImage.GetOutput())
    
    outputVolume.SetAndObserveImageData(outputImageData);
    outputVolume.SetIJKToRASMatrix(ijkToRas)

    # Add a default display node to output volume node if it does not exist yet
    if not outputVolume.GetDisplayNode:
      displayNode=slicer.vtkMRMLScalarVolumeDisplayNode()
      displayNode.SetAndObserveColorNodeID("vtkMRMLColorTableNodeGrey")
      slicer.mrmlScene.AddNode(displayNode)
      outputVolume.SetAndObserveDisplayNodeID(displayNode.GetID())  

  #pads given roi
  def padROI(self, roiNode, padding, state):

    roiRadius = [0,0,0]
    roiNode.GetRadiusXYZ(roiRadius)

    if state == True:
      roiNode.SetRadiusXYZ([x+padding for x in roiRadius])

    else:
      roiNode.SetRadiusXYZ([x-padding for x in roiRadius])

  def createAppropriateRoi(self, roiNode, volumeNode):

    # newRoiNode = slicer.vtkMRMLAnnotationROINode()
    radius = [0,0,0]
    roiNode.GetRadiusXYZ(radius)
    xyz = [0,0,0]
    roiNode.GetXYZ(xyz)

    volumeBounds = [0,0,0,0,0,0]
    volumeNode.GetRASBounds(volumeBounds)

    roiBounds = [0,0,0,0,0,0]
    roiNode.GetRASBounds(roiBounds)

    if roiBounds[0] < volumeBounds[0]:
      difference = volumeBounds[0] - roiBounds[0]
      radius[0] -= difference/2
      xyz[0] += difference/2

    if roiBounds[1] > volumeBounds[1]:
      difference = roiBounds[1] - volumeBounds[1]
      radius[0] -= difference/2
      xyz[0] -= difference/2

    if roiBounds[2] < volumeBounds[2]:
      difference = volumeBounds[2] - roiBounds[2]
      radius[1] -= difference/2
      xyz[1] += difference/2

    if roiBounds[3] > volumeBounds[3]:
      difference = roiBounds[3] - volumeBounds[3]
      radius[1] -= difference/2
      xyz[1] -= difference/2

    if roiBounds[4] < volumeBounds[4]:
      difference = volumeBounds[4] - roiBounds[4]
      radius[2] -= difference/2
      xyz[2] += difference/2

    if roiBounds[5] > volumeBounds[5]:
      difference = roiBounds[5] - volumeBounds[5]
      radius[2] -= difference/2
      xyz[2] -= difference/2

    roiNode.GetRASBounds(roiBounds)
    roiNode.SetRadiusXYZ(radius)
    roiNode.SetXYZ(xyz)

  
    
  def showInSliceViewers(self, volumeNode, sliceWidgetNames):
    # Displays volumeNode in the selected slice viewers as background volume
    # Existing background volume is pushed to foreground, existing foreground volume will not be shown anymore
    # sliceWidgetNames is a list of slice view names, such as ["Yellow", "Green"]
    if not volumeNode:
      return
    newVolumeNodeID = volumeNode.GetID()
    for sliceWidgetName in sliceWidgetNames:
      sliceLogic = slicer.app.layoutManager().sliceWidget(sliceWidgetName).sliceLogic()
      foregroundVolumeNodeID = sliceLogic.GetSliceCompositeNode().GetForegroundVolumeID()
      backgroundVolumeNodeID = sliceLogic.GetSliceCompositeNode().GetBackgroundVolumeID()
      if foregroundVolumeNodeID == newVolumeNodeID and backgroundVolumeNodeID == newVolumeNodeID:
        continue
        #   # new volume is already shown as foreground or background
        #   continue
        # if backgroundVolumeNodeID:
        #   # there is a background volume, push it to the foreground because we will replace the background volume
        #   sliceLogic.GetSliceCompositeNode().SetForegroundVolumeID(backgroundVolumeNodeID)
        # show the new volume as background
      else:
        sliceLogic.GetSliceCompositeNode().SetBackgroundVolumeID(newVolumeNodeID)
        sliceLogic.GetSliceCompositeNode().SetForegroundVolumeID(newVolumeNodeID)



  def padVolume(self, outputVolume, volume, clippedExtent):

    originShift= [clippedExtent[0], clippedExtent[2], clippedExtent[4]]
    extentShift = [clippedExtent[0], clippedExtent[2], clippedExtent[4]]
    # originShift = [i*j for i, j in zip(extentShift,spacing)]
    # newOrigin = [i+j for i,j in zip(originShift, origin)]



    padder = vtk.vtkImageConstantPad()

    volumeImageData = volume.GetImageData()
    volumeExtent = volumeImageData.GetExtent()

    translator = vtk.vtkImageChangeInformation()
    translator.SetInputData(outputVolume.GetImageData())
    translator.SetExtentTranslation(extentShift)
    # translator.SetOriginTranslation(originShift)


    padder.SetInputConnection(translator.GetOutputPort())
    # padder.SetInputData(outputVolume.GetImageData())
    padder.SetOutputWholeExtent(volumeExtent)
    padder.SetConstant(outputVolume.GetImageData().GetScalarRange()[0])
    padder.Update()
    outputImageData = vtk.vtkImageData()

    outputImageData.DeepCopy(padder.GetOutput())
    outputImageData.SetExtent( padder.GetOutput().GetExtent())

    outputImageData.AllocateScalars(volumeImageData.GetScalarType(),volumeImageData.GetNumberOfScalarComponents())
    # outputVolume.SetAndObserveImageData(outputImageData)
    outputVolume.SetAndObserveImageData(padder.GetOutput())
    outputVolume.Modified()

    volumeOrigin = volume.GetOrigin()
    outputVolume.SetOrigin(volumeOrigin)


  def cropVolumeWithRoi(self, volume, outputVolume, roi):

    imageDataWorkingCopy = vtk.vtkImageData()

    imageDataWorkingCopy.DeepCopy(volume.GetImageData())
    inputRASToIJK = vtk.vtkMatrix4x4()
    volume.GetRASToIJKMatrix(inputRASToIJK)
    inputIJKToRAS = vtk.vtkMatrix4x4()
    volume.GetIJKToRASMatrix(inputIJKToRAS)
    roiXYZ = [0,0,0]
    roiRadius = [0,0,0]
    roi.GetXYZ(roiXYZ)

    roi.GetRadiusXYZ(roiRadius)

    minXYZRAS = [roiXYZ[0]-roiRadius[0], roiXYZ[1]-roiRadius[1], roiXYZ[2]-roiRadius[2],1]
    maxXYZRAS = [roiXYZ[0]+roiRadius[0], roiXYZ[1]+roiRadius[1], roiXYZ[2]+roiRadius[2],1]
    roiTransform = roi.GetParentTransformNode()
    minXYZIJK = [0,0,0,0]
    maxXYZIJK = [0,0,0,0]
    inputRASToIJK.MultiplyPoint(minXYZRAS, minXYZIJK)
    inputRASToIJK.MultiplyPoint(maxXYZRAS, maxXYZIJK)
    minX = min(minXYZIJK[0], maxXYZIJK[0])
    maxX = max(minXYZIJK[0], maxXYZIJK[0]) + 0.5
    minY = min(minXYZIJK[1], maxXYZIJK[1])
    maxY = max(minXYZIJK[1], maxXYZIJK[1]) + 0.5
    minZ = min(minXYZIJK[2], maxXYZIJK[2])
    maxZ = max(minXYZIJK[2], maxXYZIJK[2]) + 0.5
    originalImageExtents = imageDataWorkingCopy.GetExtent()
    minX = max(minX, 0)
    maxX = min(maxX, originalImageExtents[1])
    minY = max(minY, 0)
    maxY = min(maxY, originalImageExtents[3])
    minZ = max(minZ, 0)
    maxZ = min(maxZ, originalImageExtents[5])
    outputWholeExtent = [minX,maxX,minY,maxY,minZ,maxZ]
    ijkNewOrigin = [outputWholeExtent[0], outputWholeExtent[2], outputWholeExtent[4], 1]
    rasNewOrigin = [0,0,0,0]
    inputIJKToRAS.MultiplyPoint(ijkNewOrigin, rasNewOrigin)
    imageClip = vtk.vtkImageClip()
    imageClip.SetInputData(imageDataWorkingCopy)
    outputWholeExtentInt = [int(x) for x in outputWholeExtent]
    imageClip.SetOutputWholeExtent(outputWholeExtentInt)
    imageClip.SetClipData(True)
    imageClip.Update()
    outputIJKToRAS = vtk.vtkMatrix4x4()
    outputIJKToRAS.DeepCopy(inputIJKToRAS)
    outputIJKToRAS.SetElement(0,3,rasNewOrigin[0])
    outputIJKToRAS.SetElement(1,3,rasNewOrigin[1])
    outputIJKToRAS.SetElement(2,3,rasNewOrigin[2])
    outputVolume.SetOrigin(rasNewOrigin[0], rasNewOrigin[1], rasNewOrigin[2])
    outputRASToIJK = vtk.vtkMatrix4x4()
    outputRASToIJK.DeepCopy(outputIJKToRAS)
    outputRASToIJK.Invert()
    outputImageData = vtk.vtkImageData()
    outputImageData.DeepCopy(imageClip.GetOutput())
    extent = imageClip.GetOutput().GetExtent()
    outputImageData.SetExtent(0,extent[1]-extent[0], 0, extent[3]-extent[2], 0, extent[5]-extent[4])
    outputImageData.AllocateScalars(imageDataWorkingCopy.GetScalarType(),imageDataWorkingCopy.GetNumberOfScalarComponents())
    outputVolume.SetAndObserveImageData(outputImageData)
    outputVolume.SetIJKToRASMatrix(outputIJKToRAS)
    outputVolume.SetRASToIJKMatrix(outputRASToIJK)
    outputVolume.Modified()
    slicer.mrmlScene.AddNode(outputVolume)


  def findClippingExtent(self, volume, roi):

    imageDataWorkingCopy = vtk.vtkImageData()

    imageDataWorkingCopy.DeepCopy(volume.GetImageData())
    inputRASToIJK = vtk.vtkMatrix4x4()
    volume.GetRASToIJKMatrix(inputRASToIJK)
    inputIJKToRAS = vtk.vtkMatrix4x4()
    volume.GetIJKToRASMatrix(inputIJKToRAS)
    roiXYZ = [0,0,0]
    roiRadius = [0,0,0]
    roi.GetXYZ(roiXYZ)

    roi.GetRadiusXYZ(roiRadius)

    minXYZRAS = [roiXYZ[0]-roiRadius[0], roiXYZ[1]-roiRadius[1], roiXYZ[2]-roiRadius[2],1]
    maxXYZRAS = [roiXYZ[0]+roiRadius[0], roiXYZ[1]+roiRadius[1], roiXYZ[2]+roiRadius[2],1]
    roiTransform = roi.GetParentTransformNode()
    minXYZIJK = [0,0,0,0]
    maxXYZIJK = [0,0,0,0]
    inputRASToIJK.MultiplyPoint(minXYZRAS, minXYZIJK)
    inputRASToIJK.MultiplyPoint(maxXYZRAS, maxXYZIJK)
    minX = min(minXYZIJK[0], maxXYZIJK[0])
    maxX = max(minXYZIJK[0], maxXYZIJK[0]) + 0.5
    minY = min(minXYZIJK[1], maxXYZIJK[1])
    maxY = max(minXYZIJK[1], maxXYZIJK[1]) + 0.5
    minZ = min(minXYZIJK[2], maxXYZIJK[2])
    maxZ = max(minXYZIJK[2], maxXYZIJK[2]) + 0.5
    originalImageExtents = imageDataWorkingCopy.GetExtent()
    minX = max(minX, 0)
    maxX = min(maxX, originalImageExtents[1])
    minY = max(minY, 0)
    maxY = min(maxY, originalImageExtents[3])
    minZ = max(minZ, 0)
    maxZ = min(maxZ, originalImageExtents[5])
    outputWholeExtent = [minX,maxX,minY,maxY,minZ,maxZ]
    ijkNewOrigin = [outputWholeExtent[0], outputWholeExtent[2], outputWholeExtent[4], 1]
    rasNewOrigin = [0,0,0,0]
    inputIJKToRAS.MultiplyPoint(ijkNewOrigin, rasNewOrigin)
    imageClip = vtk.vtkImageClip()
    imageClip.SetInputData(imageDataWorkingCopy)
    outputWholeExtentInt = [int(x) for x in outputWholeExtent]
    imageClip.SetOutputWholeExtent(outputWholeExtentInt)
    imageClip.SetClipData(True)
    imageClip.Update()

    extent = imageClip.GetOutput().GetExtent()

    return extent

