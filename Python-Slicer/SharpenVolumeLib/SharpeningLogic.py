import os
import unittest
from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *

import math
import PythonQt

import SimpleITK as sitk
import sitkUtils

from vtk.util import numpy_support as VN
import numpy as np

class SharpeningLogic(ScriptedLoadableModuleLogic):

  def maxScalarVolumes(self,inputVolume1, inputVolume2, volumeName):
    cast = sitk.CastImageFilter()

    inputVolumeName1 = inputVolume1.GetName()
    image1 = sitkUtils.PullFromSlicer(inputVolumeName1)
    image1Cast = cast.Execute(image1)

    inputVolumeName2 = inputVolume2.GetName()
    image2 = sitkUtils.PullFromSlicer(inputVolumeName2)
    image2Cast = cast.Execute(image2)

    maximumFilter = sitk.MaximumImageFilter()
    summedImage = maximumFilter.Execute(image1Cast,image2Cast)
    sitkUtils.PushToSlicer(summedImage,volumeName,True)
    summedVolume = slicer.util.getNode(volumeName)

    return summedVolume


  def deblurHelper(self, inputVolume, inPlaneSigma, outOfPlaneSigma, decimalPlaces):

    scanName = inputVolume.GetName()
    scan = sitkUtils.PullFromSlicer(scanName)


    gis = sitk.GaussianImageSource()
    gis.SetOrigin(scan.GetOrigin())
    gis.SetSpacing(scan.GetSpacing())

    normalizingScale = 1/((2*math.pi)**(3/2)*inPlaneSigma*inPlaneSigma*outOfPlaneSigma)
    gis.SetScale(normalizingScale)

    gis.SetSigma([inPlaneSigma,inPlaneSigma,outOfPlaneSigma])

    #calculate size based on decimal places needed
    inPlanePsfEdgePhyicalUnits =  (math.log(2*math.pi*10**(-decimalPlaces)*inPlaneSigma*inPlaneSigma*outOfPlaneSigma)*inPlaneSigma*inPlaneSigma*-2)**(0.5)
    outOfPlanePsfEdgePhyicalUnits =  (math.log(2*math.pi*10**(-decimalPlaces)*inPlaneSigma*inPlaneSigma*outOfPlaneSigma)*outOfPlaneSigma*outOfPlaneSigma*-2)**(0.5)

    inPlaneSize = 2*(int(inPlanePsfEdgePhyicalUnits/gis.GetSpacing()[0])+1)
    outOfPlaneSize = 2*(int(inPlanePsfEdgePhyicalUnits/gis.GetSpacing()[2])+1)

    maxSize = max(inPlaneSize,outOfPlaneSize)

    gis.SetSize([maxSize,maxSize,maxSize])

    meanLocation = [gis.GetSpacing()[0]*gis.GetSize()[0]/2+gis.GetOrigin()[0],gis.GetSpacing()[1]*gis.GetSize()[1]/2+gis.GetOrigin()[1],gis.GetSpacing()[2]*gis.GetSize()[2]/2+gis.GetOrigin()[2]]
    gis.SetMean(meanLocation)

    gaussianImage = gis.Execute()

    return gaussianImage
  
  def deblurVolume(self,inputVolume, iterations, decimalPlaces, gaussianImage,scan):

    #then deblur the image using RichardsonLucyDeconvolutionImageFilter\

    #there is an issue with having negative value so we will subtract the minimum value run the filter then shift back
    # vtkScan = slicer.mrmlScene.GetNodeByID(pnode.GetParameter('baselineVolumeID')).GetImageData()
    vtkScan = inputVolume.GetImageData()
    vtkHistoStatistics = vtk.vtkImageHistogramStatistics()
    vtkHistoStatistics.SetInputData(vtkScan)
    vtkHistoStatistics.Update()
    minimumPixelIntensity = vtkHistoStatistics.GetMinimum()

    scan = sitk.Subtract(scan,minimumPixelIntensity)
    scanName = inputVolume.GetName()

    # there is probably an issue if the scan is not isotropic

    #SimpleITK seems to have bugs that prevent any spacing other than 1,1,1, an origin other than 0,0,0 and directions other than (1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0)
    scan32 = sitk.Cast(scan,sitk.sitkFloat32)
    scan32.SetSpacing([1,1,1])
    scan32.SetOrigin([0,0,0])
    scan32.SetDirection((1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0))

    gaussianImage.SetSpacing([1,1,1])
    gaussianImage.SetOrigin([0,0,0])


    deconv = sitk.RichardsonLucyDeconvolutionImageFilter()
    deconv.PERIODIC_PAD = 10

    #probably need to have number of iterations as an option
    deconv.SetNumberOfIterations(iterations)

    deconvImage = deconv.Execute(scan32,gaussianImage)

    deconvImage.SetSpacing(scan.GetSpacing())
    deconvImage.SetOrigin(scan.GetOrigin())
    deconvImage.SetDirection(scan.GetDirection())

    deconvImage = sitk.Add(deconvImage,minimumPixelIntensity)

    scanName = scanName + "_Deconv"

    sitkUtils.PushToSlicer(deconvImage,scanName)

    volumeNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLScalarVolumeNode')
    numberOfVolumes = volumeNodes.GetNumberOfItems()
    lastVolume = volumeNodes.GetItemAsObject(numberOfVolumes-1)
    lastVolumeName = lastVolume.GetName()
    print "last volume name to be pushed", lastVolumeName
    return lastVolumeName


  def multiplyVolumesImageFilter(self,inputVolume1, inputVolume2, volumeName):

    cast = sitk.CastImageFilter()

    inputVolumeName1 = inputVolume1.GetName()
    image1 = sitkUtils.PullFromSlicer(inputVolumeName1)
    image1Cast = cast.Execute(image1)

    inputVolumeName2 = inputVolume2.GetName()
    image2 = sitkUtils.PullFromSlicer(inputVolumeName2)
    image2Cast = cast.Execute(image2)

    multiplyFilter = sitk.MultiplyImageFilter()
    productImage = multiplyFilter.Execute(image1Cast,image2Cast)
    sitkUtils.PushToSlicer(productImage,volumeName,True)
    productVolume = slicer.util.getNode(volumeName)

    return productVolume

  def unsharpeningMask(self,inputVolume, sharpness, gaussRadius, gaussVariance, volumeName):
    #returns volume

    #apply Gaussian
    #inputVolume = slicer.mrmlScene.GetNodeByID('vtkMRMLScalarVolumeNode1')
    inputVolumeName = inputVolume.GetName()
    originalImage = sitkUtils.PullFromSlicer(inputVolumeName)
    gaussian = sitk.DiscreteGaussianImageFilter()
    gaussian.SetMaximumKernelWidth(gaussRadius)
    var = gaussian.GetVariance()
    gaussian.SetVariance(gaussVariance)
    # gaussian.SetVariance(0)
    blurredImage = gaussian.Execute(originalImage)
    c = sharpness
    origMultiplier = c/(2*c-1)
    blurredMultiplier = (1-c)/(2*c-1)
    weightedOriginal = sitk.Multiply(originalImage, origMultiplier)
    blurredMultiplier = sitk.Multiply(blurredImage, blurredMultiplier)
    weightedDiff = sitk.Subtract(originalImage, blurredImage)
    weightedDiffVolumeName = volumeName
    sitkUtils.PushToSlicer(weightedDiff,weightedDiffVolumeName,True)
    # scalarVolumes = slicer.mrmlScene.GetNodesByClass('vtkMRMLScalarVolumeNode')
    # numberOfVolumes = scalarVolumes.GetNumberOfItems()
    # unsharpMaskedVolume = scalarVolumes.GetItemAsObject(numberOfVolumes-1)
    unsharpMaskedVolume = slicer.util.getNode(weightedDiffVolumeName)

    return unsharpMaskedVolume

  def makeLabel(self,inputVolume):
    #returns label
    labelName = inputVolume.GetName() + '_label'
    vl = slicer.modules.volumes.logic()
    vl.CreateLabelVolume(inputVolume, labelName)
    volumeLabel = slicer.util.getNode(labelName)

    return volumeLabel


  def thresholdVolume(self,masterVolume, volumeLabel, minThreshold):
    #returns label

    editorWidget = slicer.modules.editor.widgetRepresentation()
    editorWidget = slicer.modules.EditorWidget
    editorWidget.setMasterNode(masterVolume) 
    editorWidget.setMergeNode(volumeLabel) 

    slicer.modules.EditorWidget.toolsBox.selectEffect('ThresholdEffect')
    widg = slicer.modules.editor.widgetRepresentation()
    sbs = widg.findChildren('QDoubleSpinBox')
    minThresh = sbs[0]
    minThresh.setValue(minThreshold)
    pbs = widg.findChildren('QPushButton')
    applyThresholdButton = pbs[7]
    applyThresholdButton.click()  

    return volumeLabel


  def dilateVolume(self, inputVolume, volumeLabel, dilateTimes, eightNeighbors):
    #returns label
    editorWidget = slicer.modules.editor.widgetRepresentation()
    editorWidget = slicer.modules.EditorWidget
    editorWidget.setMasterNode(inputVolume) 
    editorWidget.setMergeNode(volumeLabel) 
    
    slicer.modules.EditorWidget.toolsBox.selectEffect('DilateEffect')
    widg = slicer.modules.editor.widgetRepresentation()
    rbs = widg.findChildren('QRadioButton')
    if eightNeighbors:
      rbs[0].checked = True
    else:
      rbs[1].checked = True

    pbs = widg.findChildren('QPushButton')
    applyDilateButton = pbs[6]
    for instance in range(dilateTimes):
      applyDilateButton.click()

    return volumeLabel

  def erodeVolume(self, inputVolume, volumeLabel, dilateTimes, eightNeighbors):
    #returns label
    editorWidget = slicer.modules.editor.widgetRepresentation()
    editorWidget = slicer.modules.EditorWidget
    editorWidget.setMasterNode(inputVolume) 
    editorWidget.setMergeNode(volumeLabel) 
    
    slicer.modules.EditorWidget.toolsBox.selectEffect('ErodeEffect')
    widg = slicer.modules.editor.widgetRepresentation()
    rbs = widg.findChildren('QRadioButton')
    if eightNeighbors:
      rbs[0].checked = True
    else:
      rbs[1].checked = True

    pbs = widg.findChildren('QPushButton')
    applyErodeButton = pbs[6]
    for instance in range(dilateTimes):
      applyErodeButton.click()

    return volumeLabel



  def runUnsharpMasking(self,inputVolume, sharpness,  gaussRadius, gaussVariance, volumeName):
    sharpenedVolume = self.unsharpeningMask(inputVolume, sharpness, gaussRadius, gaussVariance, volumeName)

    return sharpenedVolume



  def maxScalarVolumes(self,inputVolume1, inputVolume2, volumeName):
    cast = sitk.CastImageFilter()

    inputVolumeName1 = inputVolume1.GetName()
    image1 = sitkUtils.PullFromSlicer(inputVolumeName1)
    image1Cast = cast.Execute(image1)

    inputVolumeName2 = inputVolume2.GetName()
    image2 = sitkUtils.PullFromSlicer(inputVolumeName2)
    image2Cast = cast.Execute(image2)

    maximumFilter = sitk.MaximumImageFilter()
    summedImage = maximumFilter.Execute(image1Cast,image2Cast)
    sitkUtils.PushToSlicer(summedImage,volumeName,True)
    summedVolume = slicer.util.getNode(volumeName)

    return summedVolume
