from __future__ import print_function
import os
import unittest
from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin
from DICOMLib import DICOMUtils 
import DICOM
import unittest
import cProfile
import time

#
# LoadData
#

class LoadData(ScriptedLoadableModule):
  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "Data Loader" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Calavera"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
    This is an example of scripted loadable module bundled in an extension.
    """
    self.parent.acknowledgementText = """
    This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
    and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
    """ # replace with organization, grant and thanks.


class LoadDataWidget(ScriptedLoadableModuleWidget, VTKObservationMixin):

  def __init__(self, parent):
    ScriptedLoadableModuleWidget.__init__(self, parent)
    VTKObservationMixin.__init__(self)

    self.volumesAddedByLoadDataFlag = False
    self.seedVolume = None
    # self.logic = DICOMUtilsLogic()


  def setup(self, tabLayout = None):
    if tabLayout == None:
      self.developerMode = True
      ScriptedLoadableModuleWidget.setup(self)
      tab = qt.QTabWidget()
      tabLayout = qt.QFormLayout(tab)
      self.layout.addWidget(tab)


    self.descriptions = None
    self.seriesFile = None
    self.numberOfVolumeInScene = self.getNumberOfVolumesInScene()
    self.dicomTagsDict = {}
    rowPad1 = qt.QLabel("\n\n")
    tabLayout.addRow(rowPad1)

    self.dicomButton = qt.QPushButton("Load DICOM")
    self.dicomButton.enabled = True
    tabLayout.addRow(self.dicomButton)
    # self.dicomButton.connect('clicked(bool)', self.logic.onDicomButton)
    self.dicomButton.connect('clicked(bool)', self.onDicomButton)



    self.dataButton = qt.QPushButton("Load Other Volume Format")
    self.dataButton.enabled = True
    tabLayout.addRow(self.dataButton)
    # self.dataButton.connect('clicked(bool)', self.logic.onDataButton)
    self.dataButton.connect('clicked(bool)', self.onDataButton)

    rowPad2 = qt.QLabel("\n\n\n\n\n")
    tabLayout.addRow(rowPad2)

    self.activeText = qt.QLabel("Active Volume Data:")
    tabLayout.addRow(self.activeText)

    #select volume
    #creates combobox and populates it with all vtkMRMLScalarVolumeNodes in the scene
    self.inputVolumeSelector = slicer.qMRMLNodeComboBox()
    self.inputVolumeSelector.nodeTypes = ( ("vtkMRMLScalarVolumeNode"), "" )
    self.inputVolumeSelector.addEnabled = False
    self.inputVolumeSelector.removeEnabled = False
    self.inputVolumeSelector.setMRMLScene( slicer.mrmlScene )
    self.inputVolumeSelector.selectNodeUponCreation = False 
    tabLayout.addRow(self.inputVolumeSelector)
    # slicer.mrmlScene.AddObserver('ModifiedEvent', self.updateActiveVolume)


    rowPad3 = qt.QLabel("\n\n\n")
    self.statusText = qt.QLabel("")
    tabLayout.addRow(rowPad3)
    tabLayout.addRow(self.statusText)
    #connections
    # self.addObserver(slicer.mrmlScene, slicer.mrmlScene.NodeAddedEvent, self.updateActiveVolume)
    self.inputVolumeSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onInputNodeChanged)
    slicer.mrmlScene.AddObserver(slicer.mrmlScene.NodeAddedEvent, self.onNodeAddedEvent)
    slicer.mrmlScene.AddObserver(slicer.mrmlScene.NodeRemovedEvent, self.onNodeRemovedEvent)

  def onInputNodeChanged(self):
    volumeNode = self.inputVolumeSelector.currentNode()
    if volumeNode != None:
      # self.logic.showInSliceViewers(self.inputVolumeSelector.currentNode(), ["Red", "Yellow", "Green"])
      self.showInSliceViewers(self.inputVolumeSelector.currentNode(), ["Red", "Yellow", "Green"])
      # print("current ndoe after ndoe change", self.inputVolumeSelector.currentNode())
      # self.storeMetadataTags(self.inputVolumeSelector.currentNode(), self.dicomTagsDict)
      if self.seriesFile != None:
        self.storeMetadataTags(self.inputVolumeSelector.currentNode(), self.seriesFile, self.dicomTagsDict)
        # if volumeNode.GetID() in self.dicomTagsDict.keys():
        nameTag = str(self.dicomTagsDict[volumeNode.GetID()]["patientName"])
        description = str(self.dicomTagsDict[volumeNode.GetID()]["seriesDescription"]) 
        if description != "":
          nameTag = nameTag + "_" + description
        else:
          nameTag = nameTag + '_Series'
        volumeNode.SetName(nameTag)
        self.seriesFile = None
       

  def updateActiveVolume(self):
    currentInputVolume = self.inputVolumeSelector.currentNode()

    volumes = slicer.mrmlScene.GetNodesByClass('vtkMRMLScalarVolumeNode')
    numberOfVolumes = volumes.GetNumberOfItems()
    lastVolume = volumes.GetItemAsObject(numberOfVolumes-1)
    lastVolume.GetName()
    # self.inputVolumeSelector.setCurrentNode(lastVolume)

    if self.volumesAddedByLoadDataFlag == True:
      self.seedVolume = lastVolume
      print("\n\n NEW SEED  \n\n")
      
      print(self.volumesAddedByLoadDataFlag)
    else:
      print("NOT TRUE")
    self.inputVolumeSelector.setCurrentNode(self.seedVolume)
    self.onInputNodeChanged()
    self.volumesAddedByLoadDataFlag = False

    # return lastVolume

    # if currentInputVolume == None or self.descriptions == None:
    #   return
    # # currentInputNodeName = self.inputVolumeSelector.currentNode().GetName()
    # # index = currentInputNodeName.index(":")
    # # currentInputNodeName = currentInputNodeName[index+2:]
    # # print([x == currentInputNodeName for x in descriptions])
    # print("descriptopns: ", self.descriptions)
    # volumes = slicer.mrmlScene.GetNodesByClass('vtkMRMLScalarVolumeNode')
    # numberOfVolumes = volumes.GetNumberOfItems()
    # lastVolume = volumes.GetItemAsObject(numberOfVolumes-1)
    # print(lastVolume.GetName())
    # instUids = lastVolume.GetAttribute('DICOM.instanceUIDs').split()
    # filename = slicer.dicomDatabase.fileForInstance(instUids[0])
    # seriesDescription = slicer.dicomDatabase.fileValue(filename, '0008,103E')
    # for element in self.descriptions:
    #   if seriesDescription == self.descriptions:
    #     self.inputVolumeSelector.setCurrentNode(lastVolume)
    #     break

  # class DICOMUtilsLogic(ScriptedLoadableModuleTest):

    # def onDicomButton(self):
    #   cProfile.runctx('self.logic.onDicomButton()', globals(), locals())



  def onDicomButton(self):
    beforeLoadNumberOfVolumes = self.getNumberOfVolumesInScene()
    now = time.time()
    #extract button that launches DICOM browser from widget
    dw = slicer.modules.dicom.widgetRepresentation()

    self.dicomWidget = DICOM.DICOMWidget(dw)
    self.dicomWidget.setup()

    dicomFilesDirectory = qt.QFileDialog.getExistingDirectory(None, 'Select DCMTK database folder')
    if dicomFilesDirectory == "":
      return
    	
    self.directoryChosen = True
    self.statusText.setText("Attempting to open DICOM in directory: " + str(dicomFilesDirectory))
    self.statusText.repaint()

    try:
      self.volumesAddedByLoadDataFlag = True
      print("TRUE FROM DICOM")
      originalDatabaseDirectory = DICOMUtils.openTemporaryDatabase('tempDICOMDatbase')
      indexer = ctk.ctkDICOMIndexer()
      # self.assertIsNotNone( indexer )

      beforeTime = time.time()
      # Import study to database
      ########
      #the following line takes 28 seconds to execute
      indexer.addDirectory( slicer.dicomDatabase, dicomFilesDirectory )
      indexer.waitForImportFinished()


      # Choose first patient from the patient list
      dicomBrowserFlag = False
      if len(slicer.dicomDatabase.patients()) <= 1:
        patient = slicer.dicomDatabase.patients()[0]
        studies = slicer.dicomDatabase.studiesForPatient(patient)
        series = [slicer.dicomDatabase.seriesForStudy(study) for study in studies]
        seriesUIDs = [uid for uidList in series for uid in uidList]
        self.seriesFile = slicer.dicomDatabase.filesForSeries(series[0][0])[0]


        if len(studies) <= 1 and len(series) <=1 and len(seriesUIDs) <=1:

          self.dicomWidget.detailsPopup.offerLoadables(seriesUIDs, 'SeriesUIDList')

          beforeTime = time.time()
          #######
          #following line takes 6 seconds to execute
          # self.dicomWidget.detailsPopup.examineForLoading()


          loadables = self.dicomWidget.detailsPopup.loadableTable.loadables


          beforeTime = time.time()
          #####
          #this step takes 14 seconds to execute
          self.dicomWidget.detailsPopup.loadCheckedLoadables()
          # self.delayDisplay("Restoring original database directory")
          DICOMUtils.closeTemporaryDatabase(originalDatabaseDirectory)
          self.statusText.setText("")
        else:
          dicomBrowserFlag = True

      else:
        dicomBrowserFlag = True

      if dicomBrowserFlag == True:
        dw = slicer.modules.dicom.widgetRepresentation()
        dwCast = DICOM.DICOMWidget(dw)
        dwCast.setup()
        for patient in slicer.dicomDatabase.patients():
          studies = slicer.dicomDatabase.studiesForPatient(patient)
          series = [slicer.dicomDatabase.seriesForStudy(study) for study in studies]
          seriesUIDs = [uid for uidList in series for uid in uidList]
          self.seriesFile = slicer.dicomDatabase.filesForSeries(series[0][0])[0]
          # print("self.seriesFile before entering: ", self.seriesFile)
          # self.storeMetadataTags(self.seriesFile, self.dicomTagsDict)
          descriptions = [slicer.dicomDatabase.descriptionForSeries(singleSeries) for singleSeries in seriesUIDs]
          dwCast.detailsPopup.offerLoadables(seriesUIDs, 'SeriesUIDList')
          dwCast.detailsPopup.examineForLoading()
          dwCast.detailsPopup.open()
          self.descriptions = descriptions        
          self.statusText.setText("")



      
    except Exception, e:
      self.volumesAddedByLoadDataFlag = False
      # import traceback
      # traceback.print_exc()
      # self.delayDisplay('Test caused exception!\n' + str(e))
      # self.delayDisplay("Restoring original database directory")
      DICOMUtils.closeTemporaryDatabase(originalDatabaseDirectory)
      messageBox = qt.QMessageBox()
      messageBox.setText("This is not a DICOM Directory. Please choose an appropriate DICOM directory.")
      messageBox.exec_()

    

  def onDataButton(self):
    
    beforeLoadNumberOfVolumes = self.getNumberOfVolumesInScene()
    slicer.util.openAddDataDialog()
    afterLoadNumberOfVolumes = self.getNumberOfVolumesInScene()
    if afterLoadNumberOfVolumes > beforeLoadNumberOfVolumes:
      self.volumesAddedByLoadDataFlag = True
      print("\nTRUE")
      self.updateActiveVolume()

  def getNumberOfVolumesInScene(self):
    volumeNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLScalarVolumeNode')
    return volumeNodes.GetNumberOfItems()

  def onNodeAddedEvent(self, observer, event):
    print("Node added event")
    numberOfVolumes = self.getNumberOfVolumesInScene()
    if numberOfVolumes > self.numberOfVolumeInScene:
      self.numberOfVolumeInScene = numberOfVolumes
      print("updated ACTIVE TO BE CALLED")
      self.updateActiveVolume()
     

  def onNodeRemovedEvent(self, observer, event):
    numberOfVolumes = self.getNumberOfVolumesInScene()
    if numberOfVolumes < self.numberOfVolumeInScene:
      self.numberOfVolumeInScene = numberOfVolumes

  def storeMetadataTags(self, volume, filename, tagsDict):
    tags = {}

    if volume == None:
      return

    # for series in seriesUIDsList:
    #   parsedSeries = str(series)
    #   # instUids = volume.GetAttribute('DICOM.instanceUIDs').split()
    # #   print("\n\ninstUIDS: " + instUids)
    #   filename = slicer.dicomDatabase.fileForInstance(parsedSeries)
    #   print("PARSED UID", parsedSeries)
    #   print("FILENAME IS: ", filename)

    try:
      slicer.dicomDatabase.fileValue(filename,'0010,0010')
    except:
      return tags


    tags["patientName"] = slicer.dicomDatabase.fileValue(filename,'0010,0010')
    tags["patientID"] = slicer.dicomDatabase.fileValue(filename,'0010,0020') + '_deconv'
    tags["patientComments"] = slicer.dicomDatabase.fileValue(filename,'0010,4000')
    tags["patientBirthDate"] = slicer.dicomDatabase.fileValue(filename,'0010,0030')
    tags["patientSex"] = slicer.dicomDatabase.fileValue(filename,'0010,0040')
    tags["studyID"] = slicer.dicomDatabase.fileValue(filename,'0020,0010')
    tags["studyDate"] = slicer.dicomDatabase.fileValue(filename,'0008,0020')
    tags["studyTime"] = slicer.dicomDatabase.fileValue(filename,'0008,0030')
    tags["studyDescription"] = slicer.dicomDatabase.fileValue(filename,'0008,1030') 
    tags["seriesDescription"] = slicer.dicomDatabase.fileValue(filename, '0008, 103e')   
    tags["seriesNumber"] = slicer.dicomDatabase.fileValue(filename,'0020,0011')
    tags["modality"] = slicer.dicomDatabase.fileValue(filename,'0008,0060')
    tags["model"] = slicer.dicomDatabase.fileValue(filename,'0008,1090')
    tags["manufacturer"] = slicer.dicomDatabase.fileValue(filename,'0008,0070') 
    # x = slicer.dicomDatabase.fileValue(filename,'0008,0021')
    tagsDict[volume.GetID()] = tags
    # print("TAGSDICT", tagsDict)


  def storeMetadataTags2(self, volume, tagsDict):
    tags = {}
    if volume == None:
      return tags
    # try:
    #   print("\ntrying")
    instUids = volume.GetAttribute('DICOM.instanceUIDs').split()
    #   print("\n\ninstUIDS: " + instUids)
    filename = slicer.dicomDatabase.fileForInstance(instUids[0])
    #   slicer.dicomDatabase.fileValue(filename,'0010,0010')
    # except:
    #   print("\nexcepting")
    #   return tags


    tags["patientName"] = slicer.dicomDatabase.fileValue(filename,'0010,0010')
    tags["patientID"] = slicer.dicomDatabase.fileValue(filename,'0010,0020') + '_deconv'
    tags["patientComments"] = slicer.dicomDatabase.fileValue(filename,'0010,4000')
    tags["patientBirthDate"] = slicer.dicomDatabase.fileValue(filename,'0010,0030')
    tags["patientSex"] = slicer.dicomDatabase.fileValue(filename,'0010,0040')
    tags["studyID"] = slicer.dicomDatabase.fileValue(filename,'0020,0010')
    tags["studyDate"] = slicer.dicomDatabase.fileValue(filename,'0008,0020')
    tags["studyTime"] = slicer.dicomDatabase.fileValue(filename,'0008,0030')
    tags["studyDescription"] = slicer.dicomDatabase.fileValue(filename,'0008,1030')    
    tags["seriesNumber"] = slicer.dicomDatabase.fileValue(filename,'0020,0011')
    tags["modality"] = slicer.dicomDatabase.fileValue(filename,'0008,0060')
    tags["model"] = slicer.dicomDatabase.fileValue(filename,'0008,1090')
    tags["manufacturer"] = slicer.dicomDatabase.fileValue(filename,'0008,0070') 
    # x = slicer.dicomDatabase.fileValue(filename,'0008,0021')
    tagsDict[volume.GetID()] = tags
    # print("TAGSDICT", tagsDict)


  def showInSliceViewers(self, volumeNode, sliceWidgetNames):
    # Displays volumeNode in the selected slice viewers as background volume
    # Existing background volume is pushed to foreground, existing foreground volume will not be shown anymore
    # sliceWidgetNames is a list of slice view names, such as ["Yellow", "Green"]
    if not volumeNode:
      return
    newVolumeNodeID = volumeNode.GetID()
    for sliceWidgetName in sliceWidgetNames:
      sliceLogic = slicer.app.layoutManager().sliceWidget(sliceWidgetName).sliceLogic()
      foregroundVolumeNodeID = sliceLogic.GetSliceCompositeNode().GetForegroundVolumeID()
      backgroundVolumeNodeID = sliceLogic.GetSliceCompositeNode().GetBackgroundVolumeID()
      if foregroundVolumeNodeID == newVolumeNodeID and backgroundVolumeNodeID == newVolumeNodeID:
        continue
        #   # new volume is already shown as foreground or background
        #   continue
        # if backgroundVolumeNodeID:
        #   # there is a background volume, push it to the foreground because we will replace the background volume
        #   sliceLogic.GetSliceCompositeNode().SetForegroundVolumeID(backgroundVolumeNodeID)
        # show the new volume as background
      else:
        sliceLogic.GetSliceCompositeNode().SetBackgroundVolumeID(newVolumeNodeID)
        sliceLogic.GetSliceCompositeNode().SetForegroundVolumeID(newVolumeNodeID)


