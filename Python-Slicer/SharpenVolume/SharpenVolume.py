import os
import unittest
from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *

import math
import PythonQt

import SimpleITK as sitk
import sitkUtils

from vtk.util import numpy_support as VN
import numpy as np
import time
import SharpenVolumeLib
#

#
# Deblur
#

class SharpenVolume(ScriptedLoadableModule):
  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent) 
    self.parent.title = "Sharpen Volume" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
    This is an example of scripted loadable module bundled in an extension.
    """
    self.parent.acknowledgementText = """
    This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
    and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# DeblurTestsWidget
#

class SharpenVolumeWidget(ScriptedLoadableModuleWidget):

  def __init__(self, parent):
    ScriptedLoadableModuleWidget.__init__(self, parent)
    self.outputSharpenedVolume = None
    self.outputVolume = None
    self.finalOutputVolume = None
    #self.outputVolumeCombined = None
    self.sharpeningLogic = SharpenVolumeLib.SharpeningLogic.SharpeningLogic()
    self.roiLogic = SharpenVolumeLib.SharpeningROILogic.SharpeningROILogic()
    self.parameterNode = None
    self.parameterNodeObserver = None
    self.roiNodeObserver = None
    self.roiNode = None
    self.inputVolume = None

  def setup(self, tabLayout=None, inPlaneSigmaValue=None, outPlaneSigmaValue=None, psfVolume=None):
    print "deblur logic passed"
    if tabLayout == None:
      self.developerMode = True
      ScriptedLoadableModuleWidget.setup(self)
      tab = qt.QTabWidget()
      tabLayout = qt.QFormLayout(tab)
      self.layout.addWidget(tab)

    # Instantiate and connect widgets ...imn
    

    #
    # Parameters Area

    volumeLabels = qt.QVBoxLayout()
    volumeRow = qt.QHBoxLayout()

    #user chooses volume node
    self.inputVolumeSelector = slicer.qMRMLNodeComboBox()
    self.inputVolumeSelector.nodeTypes = (("vtkMRMLScalarVolumeNode"),"")
    self.inputVolumeSelector.addEnabled = False
    self.inputVolumeSelector.removeEnabled = False
    self.inputVolumeSelector.noneEnabled = True
    self.inputVolumeSelector.showHidden = False
    self.inputVolumeSelector.renameEnabled = False
    self.inputVolumeSelector.selectNodeUponCreation = False
    self.inputVolumeSelector.showChildNodeTypes = False
    self.inputVolumeSelector.setMRMLScene(slicer.mrmlScene)
    self.inputVolumeSelector.selectNodeUponCreation = False
    self.inputVolumeLabel = qt.QLabel("Select Volume to Sharpen:")

    volumeRow.addWidget(self.inputVolumeLabel)
    volumeRow.addWidget(self.inputVolumeSelector)
    if psfVolume != None:
      print "resetting volume in sharpen volume!!"
      self.inputVolumeSelector.setCurrentNode(psfVolume)

    self.clipUsingRoiCheckBox = qt.QCheckBox()
    self.clipUsingRoiCheckBox.setText("Create ROI to Sharpen")
    self.clipUsingRoiCheckBox.setEnabled(False)
    volumeRow.addWidget(self.clipUsingRoiCheckBox)
    volumeRow.margin = 5 #can set stretch on tabLayout maybe
    tabLayout.addRow(volumeRow)

    self.endClippingButton = qt.QPushButton("Clip to ROI")
    self.endClippingButton.toolTip = "Clip Volume with ROI"
    self.endClippingButton.setStyleSheet("border-height: 20px;")
    self.endClippingButton.enabled = True
    self.endClippingButton.visible = False
    tabLayout.addRow(self.endClippingButton)

    self.mergeRoiWithVolumeCheckBox = qt.QCheckBox()
    self.mergeRoiWithVolumeCheckBox.setText("Create merged volume with sharpened ROI and original Volume")
    self.mergeRoiWithVolumeCheckBox.setEnabled(True)
    self.mergeRoiWithVolumeCheckBox.checked = True
    self.mergeRoiWithVolumeCheckBox.visible = False
    tabLayout.addRow(self.mergeRoiWithVolumeCheckBox)

    self.sharpeningMethodsHBox = qt.QHBoxLayout()
    self.sharpeningMethodsLabel = qt.QLabel("Sharpen using:")
    self.sharpeningMethodsLabel.setStyleSheet("padding-top: 50px; padding-bottom: 20px; color: grey;")

    self.sharpenUsingRLCheckBox = qt.QCheckBox()
    self.sharpenUsingRLCheckBox.setText("Richardson-Lucy Deconvolution")
    self.sharpenUsingRLCheckBox.setEnabled(False)
    self.sharpenUsingRLCheckBox.setStyleSheet("padding-top: 50px; padding-right: 15px; padding-bottom: 20px;")

    self.sharpenUsingUnsharpMaskingCheckBox = qt.QCheckBox()
    self.sharpenUsingUnsharpMaskingCheckBox.setText("Unsharp Masking")
    self.sharpenUsingUnsharpMaskingCheckBox.setEnabled(False)
    self.sharpenUsingUnsharpMaskingCheckBox.setStyleSheet("padding-top: 50px; padding-bottom: 20px;")

    self.sharpeningMethodsHBox.addWidget(self.sharpeningMethodsLabel)
    self.sharpeningMethodsHBox.addWidget(self.sharpenUsingRLCheckBox)
    self.sharpeningMethodsHBox.addWidget(self.sharpenUsingUnsharpMaskingCheckBox)

    tabLayout.addRow(self.sharpeningMethodsHBox)


    self.deblurringCollapsibleButton = ctk.ctkCollapsibleButton()
    self.deblurringCollapsibleButton.setText("Richardson-Lucy Deconvolution")
    self.deblurringCollapsibleButton.collapsed = True
    self.deblurringCollapsibleButton.enabled = True
    tabLayout.addRow(self.deblurringCollapsibleButton)
    deblurringFormLayout = qt.QFormLayout(self.deblurringCollapsibleButton)


    self.inPlaneSigmaSelector = ctk.ctkDoubleSpinBox()
    self.inPlaneSigmaSelector.minimum = 0.01
    self.inPlaneSigmaSelector.maximum = 0.99
    self.inPlaneSigmaSelector.singleStep = 0.01
    self.inPlaneSigmaSelector.value = 0.5
    self.inPlaneSigmaSelector.setFixedWidth(70)
    deblurringFormLayout.addRow("In Plane Sigma: ", self.inPlaneSigmaSelector)
    if inPlaneSigmaValue != None and inPlaneSigmaValue != 0:
      self.inPlaneSigmaSelector.value = inPlaneSigmaValue

    self.outPlaneSigmaSelector = ctk.ctkDoubleSpinBox()
    self.outPlaneSigmaSelector.minimum = 0.01
    self.outPlaneSigmaSelector.maximum = 0.99
    self.outPlaneSigmaSelector.singleStep = 0.01
    self.outPlaneSigmaSelector.value = 0.5
    self.outPlaneSigmaSelector.setMaximumWidth(70)
    deblurringFormLayout.addRow("Out of Plane Sigma:", self.outPlaneSigmaSelector)
    if outPlaneSigmaValue !=None and outPlaneSigmaValue != 0:
      self.outPlaneSigmaSelector.value = outPlaneSigmaValue 

    #iterations slicer
    self.iterationsSlider = ctk.ctkSliderWidget()

    self.iterationsSlider.decimals = 0
    self.iterationsSlider.minimum = 1
    self.iterationsSlider.maximum = 150
    self.iterationsSlider.value = 80
    deblurringFormLayout.addRow("Iterations:", self.iterationsSlider)

    # Frame skip slider
    self.psfDecimalSlicer = ctk.ctkSliderWidget()
    self.psfDecimalSlicer.decimals = 0
    self.psfDecimalSlicer.minimum = 1
    self.psfDecimalSlicer.maximum = 15
    self.psfDecimalSlicer.value = 6
    deblurringFormLayout.addRow("Decimal Places in PSF:", self.psfDecimalSlicer)

    self.unsharpMaskingCollapsibleButton = ctk.ctkCollapsibleButton()
    self.unsharpMaskingCollapsibleButton.setText("Unsharp Masking")
    self.unsharpMaskingCollapsibleButton.collapsed = True
    self.unsharpMaskingCollapsibleButton.enabled = True
    tabLayout.addRow(self.unsharpMaskingCollapsibleButton)
    unsharpMaskingFormLayout = qt.QFormLayout(self.unsharpMaskingCollapsibleButton)
    tabLayout.addRow(unsharpMaskingFormLayout)


    self.gaussRadiusSpinBox = ctk.ctkDoubleSpinBox()
    self.gaussRadiusSpinBox.setValue(5)
    self.gaussRadiusSpinBox.decimals = 0
    self.singleStep = 1
    unsharpMaskingFormLayout.addRow("Gaussian Radius: ", self.gaussRadiusSpinBox)

    self.gaussVarianceSpinBox = ctk.ctkDoubleSpinBox()
    self.gaussVarianceSpinBox.setValue(2.56)
    self.gaussVarianceSpinBox.decimals = 2
    self.gaussVarianceSpinBox.singleStep = 0.01
    unsharpMaskingFormLayout.addRow("Gaussian Variance: ", self.gaussVarianceSpinBox)

    self.maskSharpnessSpinBox = ctk.ctkDoubleSpinBox()
    self.maskSharpnessSpinBox.setValue(0.535)
    self.maskSharpnessSpinBox.decimals = 3
    self.maskSharpnessSpinBox.singleStep = .001
    unsharpMaskingFormLayout.addRow("Mask Sharpness: ", self.maskSharpnessSpinBox)



    padding1 = qt.QLabel("\n\n")
    tabLayout.addRow(padding1)
    self.sharpenButton = qt.QPushButton("Sharpen Volume")
    self.sharpenButton.enabled = False
    tabLayout.addRow(self.sharpenButton)


    self.statusText = qt.QLabel('')
    self.statusText.setStyleSheet("padding-top: 20px; font-style: italic;")
    self.statusText.hide()
    tabLayout.addRow(self.statusText)

    self.setAndObserveRoiNode(self.roiNode)


    self.endClippingButton.connect('clicked(bool)', self.onEndClippingButtonPressed)
    self.inputVolumeSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onInputNodeChanged)
    self.clipUsingRoiCheckBox.connect("clicked()", self.onClipUsingRoiNodeChecked)
    self.sharpenUsingRLCheckBox.connect("clicked()", self.onSharpeningMethodChecked)
    self.sharpenUsingUnsharpMaskingCheckBox.connect("clicked()", self.onSharpeningMethodChecked)

    #connections #Note: should always be at the bottom of setup
    self.sharpenButton.connect('clicked(bool)', self.onSharpenButtonPressed)

    self.onInputNodeChanged()

  def onSharpeningMethodChecked(self):

    self.sharpenButton.enabled = self.sharpenUsingUnsharpMaskingCheckBox.checked or self.sharpenUsingRLCheckBox.checked

    if self.sharpenUsingUnsharpMaskingCheckBox.checked and self.sharpenUsingRLCheckBox.checked:
      self.sharpenButton.setText("Sharpen Volume using Richardson-Lucy Deconvolution and Unsharp Masking")

    elif self.sharpenUsingUnsharpMaskingCheckBox.checked:
      self.sharpenButton.setText("Sharpen Volume using Unsharp Masking")

    elif self.sharpenUsingRLCheckBox.checked:
      self.sharpenButton.setText("Sharpen Volume using Richardson-Lucy Deconvolution")

    elif self.sharpenUsingUnsharpMaskingCheckBox.checked == False and self.sharpenUsingRLCheckBox.checked == False:
      self.sharpenButton.setText("Sharpen Volume")



  def onClipUsingRoiNodeChecked(self):
    checkBoxState = self.clipUsingRoiCheckBox.isChecked()
    self.sharpenButton.enabled = not checkBoxState
    self.sharpenUsingRLCheckBox.setEnabled(not checkBoxState)
    self.sharpenUsingUnsharpMaskingCheckBox.setEnabled(not checkBoxState)
    self.endClippingButton.visible = checkBoxState
    self.mergeRoiWithVolumeCheckBox.visible = checkBoxState
    self.deblurringCollapsibleButton.enabled = not checkBoxState
    self.unsharpMaskingCollapsibleButton.enabled = not checkBoxState

    if self.clipUsingRoiCheckBox.isChecked():
      self.sharpenButton.setText("Sharpen ROI")
      self.sharpeningMethodsLabel.setStyleSheet("padding-top: 50px; padding-bottom: 20px; color: grey;")
      self.statusText.setText("Encapsulate desired area by ROI")

      self.roiNode = slicer.vtkMRMLAnnotationROINode()
      self.setAndObserveRoiNode(self.roiNode)
      self.roiNode.SetDisplayVisibility(1)
      self.roiNode.Initialize(slicer.mrmlScene)
      roiRadius = [40,60,95]
      self.roiNode.SetRadiusXYZ(roiRadius)
      inputVolume = self.inputVolumeSelector.currentNode()
      volumeBounds = [0,0,0,0,0,0]
      inputVolume.GetRASBounds(volumeBounds)
      roiCenterInit = [0,0,0,0,0,0]
      for i in range(0,5,2):
        roiCenterInit[i] = (volumeBounds[i+1] + volumeBounds[i])/2

      self.roiNode.SetXYZ(roiCenterInit[0], roiCenterInit[2], roiCenterInit[4])
      self.roiNode.SetDisplayVisibility(1)

    else:
      slicer.mrmlScene.RemoveNode(self.roiNode)
      self.sharpenButton.setText("Sharpen Volume")
      self.sharpeningMethodsLabel.setStyleSheet("padding-top: 50px; padding-bottom: 20px; color: black;")
      self.statusText.setText("")
      self.mergeRoiWithVolumeCheckBox.checked = False

    slicer.app.layoutManager().setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView)


  def setAndObserveRoiNode(self, roiNode):
    if roiNode == self.roiNode and self.roiNodeObserver:
      # no change and node is already observed
      return
    # Remove observer to old parameter node
    if self.roiNode and self.roiNodeObserver:
      self.roiNode.RemoveObserver(self.roiNodeObserver)
      self.roiNodeObserver = Nonebank
    # Set and observe new parameter node
    self.roiNode = roiNode
    if self.roiNode != None:
      self.roiNode.InteractiveModeOff()

  def onInputNodeChanged(self):
    if self.roiNode:
      self.clipUsingRoiCheckBox.checked = False

    self.statusText.setText("")
    currentNodeState = self.inputVolumeSelector.currentNode() != None
    self.endClippingButton.enabled = currentNodeState 
    self.roiLogic.showInSliceViewers(self.inputVolumeSelector.currentNode(), ["Red", "Yellow", "Green"])
    slicer.app.processEvents()

    self.clipUsingRoiCheckBox.setEnabled(currentNodeState)
    self.sharpenUsingRLCheckBox.setEnabled(currentNodeState)
    self.sharpenUsingUnsharpMaskingCheckBox.setEnabled(currentNodeState)
    self.deblurringCollapsibleButton.enabled = currentNodeState
    self.unsharpMaskingCollapsibleButton.enabled = currentNodeState
    self.statusText.visible = currentNodeState

    if currentNodeState:
      self.sharpeningMethodsLabel.setStyleSheet("padding-top: 50px; padding-bottom: 20px; color: black;")

      if (self.sharpenUsingUnsharpMaskingCheckBox.isChecked() or self.sharpenUsingRLCheckBox.isChecked()):
        self.sharpenButton.enabled = True
        self.statusText.setText("Ready")
    else:
      self.sharpeningMethodsLabel.setStyleSheet("padding-top: 50px; padding-bottom: 20px; color: grey;")
  
  def onRoiSelect(self, node):
    self.setAndObserveRoiNode(self.roiNode)


  def onEndClippingButtonPressed(self):
    self.endClippingButton.visible = False
    self.sharpenUsingRLCheckBox.setEnabled(True)
    self.sharpenUsingUnsharpMaskingCheckBox.setEnabled(True)
    self.sharpeningMethodsLabel.setStyleSheet("padding-top: 50px; padding-bottom: 20px; color: black;")
    self.deblurringCollapsibleButton.enabled = True
    self.unsharpMaskingCollapsibleButton.enabled = True
    self.clipUsingRoiCheckBox.checked = False
    slicer.app.processEvents()

    self.inputVolume = self.inputVolumeSelector.currentNode()
    inputVolume = self.inputVolume
    #create copy of roi to pad with and then crop with the original later
    radius = [0,0,0,0,0,0]
    self.roiNode.GetRASBounds(radius)
    self.roiLogic.createAppropriateRoi(self.roiNode, inputVolume)
    self.roiNode.GetRASBounds(radius)
    paddedRoi = self.roiLogic.deepCopyROI(self.roiNode)
    self.roiLogic.padROI(paddedRoi, 10, True)
    paddedRoi.GetRASBounds(radius)
    self.roiLogic.createAppropriateRoi(paddedRoi, inputVolume)
    paddedRoi.GetRASBounds(radius)
    self.clippingExtent = self.roiLogic.findClippingExtent(inputVolume, self.roiNode)

    self.outputVolume = slicer.vtkMRMLScalarVolumeNode()
    self.roiLogic.cropVolumeWithRoi(inputVolume, self.outputVolume, paddedRoi)

    self.outputVolume.SetName(inputVolume.GetName() + '_ClippedAndPadded')

    #pad the roi before deblurring, so that it can be de-padded after deblurring
    #this removes a potentially sharpened parameter which frames the roi
    self.inputVolumeSelector.setCurrentNode(self.outputVolume) 
    self.roiLogic.showInSliceViewers(self.outputVolume, ["Red", "Yellow", "Green"])
    slicer.mrmlScene.RemoveNode(paddedRoi)
    self.statusText.setText("Ready")
    self.roiNode.SetDisplayVisibility(0)


  def onSharpenButtonPressed(self):
    unsharpState = self.sharpenUsingUnsharpMaskingCheckBox.checked and not self.sharpenUsingRLCheckBox.checked
    deblurState = self.sharpenUsingRLCheckBox.checked and not self.sharpenUsingUnsharpMaskingCheckBox.checked
    deblurUnsharpState = self.sharpenUsingUnsharpMaskingCheckBox.checked and self.sharpenUsingRLCheckBox.checked
    

    if self.inputVolume == None:
      self.inputVolume = self.inputVolumeSelector.currentNode()
      print "self.inputVolume is None so name is", self.inputVolume.GetName()
      
    volumeToSharpen = self.inputVolumeSelector.currentNode()

    iterations = int(self.iterationsSlider.value)
    decimalPlaces = int(self.psfDecimalSlicer.value)
    inPlaneSigma = self.inPlaneSigmaSelector.value
    outOfPlaneSigma = self.outPlaneSigmaSelector.value

    sharpness = self.maskSharpnessSpinBox.value
    gaussRadius = int(self.gaussRadiusSpinBox.value)
    gaussVariance= self.gaussVarianceSpinBox.value

    self.statusText.setStyleSheet("padding-top: 20px;")
    self.statusText.visible = True

    timeStamp = str(time.localtime()[0])+str(time.localtime()[3])+str(time.localtime()[4])+str(time.localtime()[5])
    intermediateVolumeName = "intermediateVolume" + timeStamp
    if unsharpState:
      self.statusText.setText("Unsharp Masking...")
      self.statusText.repaint()
      self.outputSharpenedVolume = self.onUnsharpMaskChecked(volumeToSharpen, sharpness, gaussRadius, gaussVariance, intermediateVolumeName)
      fileNameSuffix = 'Sharpened'

    elif deblurState:
      self.statusText.setText("Deblurring...")
      self.statusText.repaint()
      self.outputSharpenedVolume = self.onDeblurChecked(volumeToSharpen, iterations, decimalPlaces, inPlaneSigma, outOfPlaneSigma)      
      fileNameSuffix = 'Deblurred'

    else:
      self.statusText.setText("Deblurring and Unsharp Masking...")
      self.statusText.repaint()
      self.outputSharpenedVolume = self.onDeblurChecked(volumeToSharpen, iterations, decimalPlaces, inPlaneSigma, outOfPlaneSigma)      
      self.outputSharpenedVolume = self.onUnsharpMaskChecked(self.outputSharpenedVolume, sharpness, gaussRadius, gaussVariance, intermediateVolumeName)
      self.statusText.setText("Finished Richardson-Lucy Deconvolution and Unsharp Masking")
      fileNameSuffix = 'DeblurredSharpened'

    

    if self.outputVolume:
      self.newVolume = slicer.vtkMRMLScalarVolumeNode()
      self.roiLogic.cropVolumeWithRoi(self.outputSharpenedVolume, self.newVolume, self.roiNode)
      self.roiLogic.padVolume(self.newVolume, self.inputVolume, self.clippingExtent)
      self.newVolume.SetName(self.inputVolume.GetName() + '_Clipped' + fileNameSuffix)
      slicer.mrmlScene.RemoveNode(self.outputVolume)
      slicer.mrmlScene.RemoveNode(self.outputSharpenedVolume)
      slicer.mrmlScene.RemoveNode(self.roiNode)
      slicer.mrmlScene.AddNode(self.newVolume)
      self.finalOutputVolume = self.newVolume

    else:
      print self.inputVolume.GetName()
      print fileNameSuffix
      self.outputSharpenedVolume.SetName(self.inputVolume.GetName() + '_' + fileNameSuffix)
      self.finalOutputVolume = self.outputSharpenedVolume

      
    if self.mergeRoiWithVolumeCheckBox.checked and self.outputVolume != None:
      self.mergedVolume = self.sharpeningLogic.maxScalarVolumes(self.inputVolume, self.newVolume, self.inputVolume.GetName() + "_SharpenedAndMerged")
      slicer.mrmlScene.RemoveNode(self.newVolume)
      slicer.mrmlScene.RemoveNode(self.outputVolume)
      self.finalOutputVolume = self.mergedVolume

    self.roiLogic.showInSliceViewers(self.finalOutputVolume, ["Red", "Yellow", "Green"])
    ("Sharpened Output Volume: " + self.finalOutputVolume.GetName())
    self.mergeRoiWithVolumeCheckBox.checked = False
    self.mergeRoiWithVolumeCheckBox.visible = False
    self.sharpenUsingUnsharpMaskingCheckBox.checked = False
    self.sharpenUsingRLCheckBox.checked = False
    self.sharpenButton.setText("Sharpen Volume")
    self.sharpenButton.enabled = False
    self.inputVolume = None

  def onUnsharpMaskChecked(self, inputVolume, sharpness, gaussRadius, gaussVariance, volumeName):
      sharpenedVolume= self.sharpeningLogic.runUnsharpMasking(inputVolume, sharpness,  gaussRadius, gaussVariance, volumeName)
      self.statusText.setText("Finished Unsharp-Masking")

      return sharpenedVolume

  def onDeblurChecked(self, inputVolume, iterations, decimalPlaces, inPlaneSigma, outOfPlaneSigma):

      gaussianImage =  self.sharpeningLogic.deblurHelper(inputVolume, inPlaneSigma, outOfPlaneSigma, decimalPlaces)
      scanName = inputVolume.GetName()
      scan = sitkUtils.PullFromSlicer(scanName)
      scanNameDeconv = self.sharpeningLogic.deblurVolume(inputVolume, iterations, decimalPlaces, gaussianImage,scan)

      outputSharpenedVolume = slicer.util.getNode(scanNameDeconv)

      self.statusText.setText("Finished Richardson-Lucy Deconvolution")

      return outputSharpenedVolume 

