from __future__ import division

import os
import unittest
from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *

import SimpleITK as sitk
import sitkUtils as su
import math
import PythonQt
import sys
import numpy as np


#import qSlicerestimatePSF1ModuleWidget

from vtk.util import numpy_support as VN

from EstimatePSFLib.estimateGaussianPSF import estimateGaussianPSF_


#
# EstimatePSF
#

class EstimatePSF(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):

    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "EstimatePSFModule" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
    This is an example of scripted loadable module bundled in an extension.
    """
    self.parent.acknowledgementText = """
    This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
    and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
    """ # replace with organization, grant and thanks.


class EstimatePSFWidget(ScriptedLoadableModuleWidget):


  def setup(self, tabLayout = None):

    slicer.mrmlScene.AddObserver(vtk.vtkCommand.ModifiedEvent, self.sceneChangeObserver)
    self.lm = slicer.app.layoutManager()
    # self.lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpQuantitativeView)
    self.startCount = 0
    self.buttonNumber = None

    #variables for keeping track of rulers
    self.allRulersDict = {}

    self.rulers = []
    currentRulers = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationRulerNode')
    self.rulerNumber = currentRulers.GetNumberOfItems()
    
    self.iterations = 1
    self.psfDecimals = 6



    if tabLayout == None:
        ScriptedLoadableModuleWidget.setup(self)
        tab = qt.QScrollArea()
        tab.setWidget(qt.QWidget())
        self.layout.addWidget(tab)
        self.tabLayout = qt.QFormLayout(tab.widget())
        tab.setWidgetResizable(True)
    else:
        self.tabLayout = tabLayout



    classNodes = slicer.mrmlScene.GetNodesByClass("vtkMRMLAnnotationHierarchyNode")
    itemCount = classNodes.GetNumberOfItems()
    if itemCount > 0:
      AnnotationsSetupFlag = True
    else:
      AnnotationsSetupFlag = False

    rowPad1 = qt.QLabel("")
    self.tabLayout.addRow(rowPad1)

    self.volumeSelectorLabel = qt.QLabel("Select volume for PSF estimate: ")
    self.volumeSelector = slicer.qMRMLNodeComboBox()
    self.volumeSelector.nodeTypes = ( ("vtkMRMLScalarVolumeNode"), "" )
    self.volumeSelector.addEnabled = False
    self.volumeSelector.removeEnabled = False
    self.volumeSelector.noneEnabled = False
    self.volumeSelector.showHidden = False
    self.volumeSelector.renameEnabled = False
    self.volumeSelector.selectNodeUponCreation = True
    self.volumeSelector.showChildNodeTypes = False
    self.volumeSelector.setMRMLScene(slicer.mrmlScene)
    volumeSelectorLayout = qt.QHBoxLayout()
    volumeSelectorLayout.addWidget(self.volumeSelectorLabel)
    volumeSelectorLayout.addWidget(self.volumeSelector)
    self.tabLayout.addRow(volumeSelectorLayout)

    volumesModule = slicer.modules.volumes.widgetRepresentation()
    volumesModuleComboBoxes = volumesModule.findChildren('qMRMLNodeComboBox')
    self.volumesModuleVolumeComboBox = volumesModuleComboBoxes[0]

    rowPad2 = qt.QLabel("")
    self.tabLayout.addRow(rowPad2)

    self.rulers = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationRulerNode')

    
    #This is not called when rulers are deleted programmatically
    # slicer.mrmlScene.AddObserver(vtk.vtkCommand.DeleteEvent, self.sceneElementDeleted)
    slicer.mrmlScene.AddObserver(slicer.mrmlScene.EndCloseEvent, self.sceneCloseObserver)


    self.annotationsWidget = slicer.modules.annotations.createNewWidgetRepresentation()
    self.annotationsWidgetCollapsibleButton = self.annotationsWidget.findChild('ctkCollapsibleButton')
    self.annotationsLogic = slicer.modules.annotations.logic()


    if AnnotationsSetupFlag == False:
      self.addHierarchyNodes()

    else:
      self.topLevelHierarchy = slicer.mrmlScene.GetNthNodeByClass(0,"vtkMRMLAnnotationHierarchyNode")
      self.inPlaneHierarchy = slicer.mrmlScene.GetNthNodeByClass(1,"vtkMRMLAnnotationHierarchyNode")
      self.outPlaneHierarchy = slicer.mrmlScene.GetNthNodeByClass(2,"vtkMRMLAnnotationHierarchyNode")
      self.topLevelHierarchyID = self.topLevelHierarchy.GetID()
      self.inPlaneHierarchyID = self.inPlaneHierarchy.GetID()
      self.outPlaneHierarchyID = self.outPlaneHierarchy.GetID()

    
    annotationsCollapsibleButton = ctk.ctkCollapsibleButton()
    annotationsCollapsibleButton.setText("Place Rulers")

    self.tabLayout.addRow(annotationsCollapsibleButton)
    annotationsFormLayout = qt.QFormLayout(annotationsCollapsibleButton)


    self.tabLayout.addRow(qt.QLabel(""))

    self.startMeasurementsInPlane = qt.QPushButton("Start Placing In-Plane")
    self.startMeasurementsInPlane.enabled = True
    self.startMeasurementsInPlane.connect('clicked(bool)', self.addRulersInPlane)

    self.startMeasurementsOutPlane = qt.QPushButton("Start Placing Out-of-Plane")
    self.startMeasurementsOutPlane.enabled = True
    self.startMeasurementsOutPlane.connect('clicked(bool)', self.addRulersOutPlane)


    buttonLayout = qt.QHBoxLayout()
    buttonLayout.addWidget(self.startMeasurementsInPlane)
    buttonLayout.addWidget(self.startMeasurementsOutPlane)

    annotationsFormLayout.addRow(qt.QLabel(""))

    annotationsFormLayout.addRow(buttonLayout)
    annotationsFormLayout.addRow(qt.QLabel(""))

    rulersFrame = qt.QFrame()
    rulersFrame.setStyleSheet('background-color: whiteSmoke;')
    self.rulersFrameLayout = qt.QGridLayout()
    rulersFrame.setLayout(self.rulersFrameLayout)
    annotationsFormLayout.addRow(rulersFrame)

    rulersInPlaneLabelTitle = qt.QLabel("In-Plane Rulers")
    rulersInPlaneLabelTitle.setStyleSheet('text-decoration: underline; qproperty-alignment: AlignCenter;')
    self.rulersFrameLayout.addWidget(rulersInPlaneLabelTitle, 0, 0)
    self.inPlaneRulersLayout = qt.QVBoxLayout()
    self.outPlaneRulersLayout = qt.QVBoxLayout()
    self.rulersFrameLayout.addLayout(self.inPlaneRulersLayout,1,0)
    self.rulersFrameLayout.addLayout(self.outPlaneRulersLayout, 1, 1)
    rulersOutPlaneLabelTitle = qt.QLabel("Out-of-Plane Rulers")
    rulersOutPlaneLabelTitle.setStyleSheet('text-decoration: underline; qproperty-alignment: AlignCenter;')
    self.rulersFrameLayout.addWidget(rulersOutPlaneLabelTitle, 0, 1)

    annotationsFormLayout.addRow(qt.QLabel("\n"))

    self.deleteRulerPushButton = qt.QPushButton("Delete Selected Ruler")
    self.deleteRulerPushButton.setIcon(qt.QIcon(":/Icons/MarkupsDelete.png"))
    self.deleteRulerPushButton.setMaximumWidth(135)
    self.deleteRulerPushButton.setEnabled(False)

    self.loadRulersPushButton = qt.QPushButton("")
    self.loadRulersPushButton.setIcon(qt.QIcon(":/Icons/Data.png"))
    self.loadRulersPushButton.setMaximumWidth(40)
    # annotationsFormLayout.addRow(self.deleteRulerPushButton)

    self.hideRulersPushButton = qt.QPushButton("")
    self.hideRulersPushButton.setIcon(qt.QIcon(":/Icons/AnnotationVisibility.png"))
    self.hideRulersPushButton.setMaximumWidth(40)
    self.hideRulersPushButton.setEnabled(False)
    self.hideRulersVisibleStatus = True

    deleteButtonLayout = qt.QHBoxLayout()  
    deleteButtonLayout.addWidget(qt.QLabel(""))
    deleteButtonLayout.addWidget(self.loadRulersPushButton)
    deleteButtonLayout.addWidget(self.deleteRulerPushButton)
    deleteButtonLayout.addWidget(self.hideRulersPushButton)
    deleteButtonLayout.addWidget(qt.QLabel(""))
    annotationsFormLayout.addRow(deleteButtonLayout)  

    dirPath = os.path.dirname(os.path.realpath(__file__))
   
    qtUIFilePath = dirPath + '\Resources\UI\qSlicerestimatePSF1ModuleWidget.ui'
    print "qtUIFilePath from EstimatePSF: ", qtUIFilePath

    f = qt.QFile(qtUIFilePath)
    f.open(qt.QFile.ReadOnly)
    loader = qt.QUiLoader()
    self.estimatePSFWidgetDirectUi = loader.load(f)
    f.close()    

    #findchild should be gotten rid of
    self.estimatePSFWidget = self.estimatePSFWidgetDirectUi


    self.estimatePSFWidget.setMRMLScene(slicer.mrmlScene)
    self.estimatePSFWidgetCollapsibleButton = self.estimatePSFWidget.findChild('ctkCollapsibleButton')
    self.estimatePSFWidgetCollapsibleButton.collapsed = True
    self.estimatePSFWidgetCollapsibleButton.connect('contentsCollapsed(bool)',self.onEstimatePSFWidgetOpen)
    self.tabLayout.addRow(self.estimatePSFWidget)
    # self.verticalLayoutScroll.addWidget(self.estimatePSFWidget)
    comboBoxes = self.estimatePSFWidgetCollapsibleButton.findChildren('qMRMLNodeComboBox')
    #can set this more intelligently by seeing if the nodeType matches, instead of just using the child we assume to be correct
    ## e.g. if self.psfVolumeNodeComboBox1.nodeTypes == (u'vtkMRMLScalarVolumeNode',): # then we know that we are dealing with the volume selector 
    ## whereas if  self.psfVolumeNodeComboBox2.nodeTypes == (u'vtkMRMLScalarVolumeNode',): # then we know that we are dealing with the annotation selector
    self.psfVolumeSelectorComboBox = comboBoxes[1]
    # self.psfVolumeSelectorComboBox.hide()
    self.psfHierarchySelectorComboBox = comboBoxes[0]

    self.inPlaneSigmaComboBox = self.estimatePSFWidget.findChild('ctkDoubleSpinBox','InPlaneSigmaSpinBox')
    self.outPlaneSigmaComboBox = self.estimatePSFWidget.findChild('ctkDoubleSpinBox','OutOfPlaneSigmaSpinBox')
    # self.inPlaneSigmaComboBox.value = updatedInPlanePsf
    # self.outPlaneSigmaComboBox.value = updatedOutPlanePsf

    
    self.corticalThicknessMinSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.CorticalThicknessMinSpinBox
    self.trabecularThicknessMinSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.TrabecularThicknessMinSpinBox
    self.edgeDetectionSlackFactorSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.EdgeDetectionSlackFactorSpinBox
    self.trabecularIntensityMinSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.TrabecularIntensityMinSpinBox
    self.trabecularIntensityMaxSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.TrabecularIntensityMaxSpinBox
    self.corticalIntensityMinSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.CorticalIntensityMinSpinBox
    self.corticalIntensityMaxSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.CorticalIntensityMaxSpinBox    
    self.sigmaMaxSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.SigmaMaxSpinBox
    self.sigmaMinSpinBox = self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.SigmaMinSpinBox
    self.estimatePSFWidget.ParametersCollapsibleButton.ProfilEstimationParametersGroupBox_2.CorticalThicknessMinSpinBox

    radioButtons = self.estimatePSFWidgetCollapsibleButton.findChildren('QRadioButton')
    self.radioButtonInPlane = radioButtons[0]
    self.radioButtonOutPlane = radioButtons[1]

    labels = self.estimatePSFWidgetCollapsibleButton.findChildren('QLabel')
    self.psfVolumeLabel = labels[3]
    self.psfVolumeLabel.hide()
    
    self.estimateSigmaButton = self.estimatePSFWidgetCollapsibleButton.EstimateButton
    self.estimateSigmaButton.connect('clicked(bool)',self.onEstimateButtonPress)


    self.logic = EstimatePSFLogic()

    #spacing
    self.spacingLabel = qt.QLabel("  Image Spacing: ")
    self.spacingLabel.setVisible(False)
    collapsableButtons = volumesModule.findChildren('ctkCollapsibleButton')
    volumeInformation = collapsableButtons[1]
    infoWidget = volumeInformation.findChild('qMRMLVolumeInfoWidget')
    self.infoSpinBoxes = infoWidget.findChildren('ctkDoubleSpinBox')

    self.xSpacing = qt.QLabel('')
    self.ySpacing = qt.QLabel('')
    self.zSpacing = qt.QLabel('')

    spacingLayout = qt.QHBoxLayout()
    spacingLayout.addWidget(self.spacingLabel)
    spacingLayout.addWidget(self.xSpacing)
    spacingLayout.addWidget(self.ySpacing)
    spacingLayout.addWidget(self.zSpacing)
    self.tabLayout.addRow(spacingLayout)


    #connections
    self.psfHierarchySelectorComboBox.setCurrentNode(self.inPlaneHierarchy)
    self.psfHierarchySelectorComboBox.connect("currentNodeChanged(vtkMRMLNode*)", self.onHierarchyChanged)
    self.radioButtonInPlane.connect('toggled(bool)',self.onToggled)
    self.radioButtonOutPlane.connect('toggled()',self.onToggled)
    self.volumeSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onVolumeSelect)
    self.deleteRulerPushButton.connect("clicked(bool)", self.onDeleteRulerPushButtonPressed)
    self.loadRulersPushButton.connect("clicked(bool)", self.onLoadRulersPushButtonPressed)
    self.hideRulersPushButton.connect("clicked(bool)", self.onHideRulersPushButtonPressed)

    self.onVolumeSelect()


  def addHierarchyNodes(self):
    self.annotationsLogic.AddHierarchy()
    self.inPlaneHierarchy = self.annotationsLogic.GetActiveHierarchyNode()
    self.inPlaneHierarchyID = self.annotationsLogic.GetActiveHierarchyNodeID()
    self.inPlaneHierarchy.SetName("In-Plane Rulers")
    self.topLevelHierarchyID = self.annotationsLogic.GetTopLevelHierarchyNodeID()
    self.annotationsLogic.SetActiveHierarchyNodeID(self.topLevelHierarchyID)
    self.topLevelHierarchy = self.annotationsLogic.GetActiveHierarchyNode()
    self.annotationsLogic.AddHierarchy()
    self.outPlaneHierarchy = self.annotationsLogic.GetActiveHierarchyNode()
    self.outPlaneHierarchyID = self.annotationsLogic.GetActiveHierarchyNodeID()
    self.outPlaneHierarchy.SetName("Out-of-Plane Rulers") 

  def onHideRulersPushButtonPressed(self):
    if self.hideRulersVisibleStatus == True:
      self.hideRulersVisibleStatus = False
      self.hideRulersPushButton.setIcon(qt.QIcon(":/Icons/AnnotationInvisible.png"))
      for key in self.allRulersDict.keys():
        self.allRulersDict[key]['rulerObject'].SetDisplayVisibility(0)

    else:
      self.hideRulersVisibleStatus = True
      self.hideRulersPushButton.setIcon(qt.QIcon(":/Icons/AnnotationVisibility.png"))
      for key in self.allRulersDict.keys():
        self.allRulersDict[key]['rulerObject'].SetDisplayVisibility(1)


  def onLoadRulersPushButtonPressed(self):

    # annotationHierarchyNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationHierarchyNode')
    rulers = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationRulerNode')
    # numberOfAnnotationHierarchyNodes = annotationHierarchyNodes.GetNumberOfItems()
    numberOfRulers = rulers.GetNumberOfItems()
    # dictionaryOfAnnotationNodes = {}
    dictionaryOfInSceneRulers = {}

    msgBox = qt.QMessageBox()
    msgBox.setStandardButtons(qt.QMessageBox.Yes | qt.QMessageBox.No)
    buttonYes = msgBox.button(qt.QMessageBox.Yes)
    buttonYes.setText("Add")
    buttonNo = msgBox.button(qt.QMessageBox.No)
    buttonNo.setText("Cancel")
    layout = msgBox.layout()
    gridLayout = qt.QGridLayout()
    inPlane = True
    row = 0
    col = 0
    gridLayout.addWidget(qt.QLabel("Rulers in Scene:"), row, col, 1, 2)
    row+=1

    for rulerIndex in range(numberOfRulers):
      #check that it is not part of inPlane or outPlane rulers already
      rulerNode = rulers.GetItemAsObject(rulerIndex)
      if rulerNode.GetID() not in self.allRulersDict.keys():
        dictionaryOfInSceneRulers[rulerNode.GetID()] = {'rulerObject': rulerNode}

    for key in dictionaryOfInSceneRulers:
      rulerNode = dictionaryOfInSceneRulers[key]['rulerObject']
      rulerNodeName = rulerNode.GetName()
      checkBox = qt.QCheckBox(rulerNodeName)
      dictionaryOfInSceneRulers[key]['msgBoxCheckBox'] = checkBox
      gridLayout.addWidget(checkBox, row, col)
      row+=1

    if not len(dictionaryOfInSceneRulers) == 0:
      gridLayout.addWidget(qt.QLabel("\nLoad selected rulers as:"), row, col,1,2)
      row+=1
      inPlaneRadioButton = qt.QRadioButton("In-Plane Rulers")
      inPlaneRadioButton.checked = True
      # inPlaneRadioButton.setMaximumWidth(100)
      outPlaneRadioButton = qt.QRadioButton("Out-of-Plane Rulers")
      # outPlaneRadioButton.setMaximumWidth(100)
      gridLayout.addWidget(inPlaneRadioButton, row, col)
      gridLayout.addWidget(outPlaneRadioButton, row, col+1)
    else:
      gridLayout = qt.QGridLayout()
      gridLayout.addWidget(qt.QLabel("\n   No new rulers in scene to load\n"), row, col)
      buttonNo.setText("Okay")
      msgBox.removeButton(buttonYes)
    layout.addLayout(gridLayout,0,0)
    value = msgBox.exec_()

    if value == qt.QMessageBox.Yes:
      if outPlaneRadioButton.checked == True:
        inPlane = False
      self.addRulersToRulerPlanes(inPlane, dictionaryOfInSceneRulers)


  def addRulersToRulerPlanes(self, inPlane, dictionaryOfInSceneRulers):
    currentInputVolume = self.volumeSelector.currentNode()
    if inPlane == True:
      buttonNumber = 1
    else:
      buttonNumber = 2

    for key in dictionaryOfInSceneRulers.keys():
      rulerDict = dictionaryOfInSceneRulers[key]
      if rulerDict['msgBoxCheckBox'].checked == True:
        rulerNode = rulerDict['rulerObject']
        self.addRulerToScene(buttonNumber, rulerNode, self.volumeSelector.currentNode())
        self.updatePlot(currentInputVolume, rulerNode, numPoints=100)
 

  def onDeleteRulerPushButtonPressed(self):

    msgBox = qt.QMessageBox()
    #remove ruler, radioButton, associated charts, arrays, and delete the key from dictionary
    if len(self.allRulersDict) == 1:
      rulerKey = self.allRulersDict.iterkeys().next()
      if self.allRulersDict[rulerKey]['radioButton'].checked:
        self.activeRulerKey = rulerKey

    key = self.activeRulerKey
    if key == None:
      msgBox.setText("\nNo ruler selected.       \n")
      msgBox.setStandardButtons(qt.QMessageBox.Yes)
      buttonYes = msgBox.button(qt.QMessageBox.Yes)
      buttonYes.setText("Ok")
      msgBox.exec_()
      return

    # try:
    rulerName = self.allRulersDict[key]['rulerObject'].GetName()
    # except: #no new ruler selected
    #   return 
    msgBox.setText("\nAre you sure you want to delete Ruler " + rulerName + "?       \n")
    msgBox.setStandardButtons(qt.QMessageBox.Yes | qt.QMessageBox.No)
    value = msgBox.exec_()
    if value != qt.QMessageBox.Yes:
      return
    #THIS SHOULD NO WORK REVISE
    if self.allRulersDict[key]['buttonNumber'] == 1:
      self.inPlaneRulersLayout.removeWidget(self.allRulersDict[key]['radioButton'])
      print "\ndeleting button in plane button 1"
    else:
      # self.allRulersDict[key]['radioButton'].visible = False #this line should not be necessary, but qt is being buggy and displaying the rb
      self.outPlaneRulersLayout.removeWidget(self.allRulersDict[key]['radioButton'])
      print "\rdeleting button out plane button 2"

    slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['rulerObject'])
    self.allRulersDict[key]['radioButton'].setParent(0)

    slicer.mrmlScene.RemoveNode(slicer.mrmlScene.GetNodeByID(self.allRulersDict[key]['chartNodeID']))
    slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['chartNodeArrayOriginal'])
    slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['chartNodeArrayFitted'])
    slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['chartNodeArrayPieceWise'])
    del self.allRulersDict[key]
    self.activeRulerKey = None

  def onHierarchyChanged(self):
    if self.psfHierarchySelectorComboBox.currentNode() == self.inPlaneHierarchy:
      self.radioButtonInPlane.checked = True
    elif self.psfHierarchySelectorComboBox.currentNode() == self.outPlaneHierarchy:
      self.radioButtonOutPlane.checked = True


  def onToggled(self):
    if self.radioButtonInPlane.isChecked():
      self.psfHierarchySelectorComboBox.setCurrentNode(self.inPlaneHierarchy)
    if self.radioButtonOutPlane.checked:
      self.psfHierarchySelectorComboBox.setCurrentNode(self.outPlaneHierarchy)


  def onVolumeSelect(self):
    if self.volumeSelector.currentNode() == None:

      self.spacingLabel.setVisible(False)
      self.xSpacing.setText('')
      self.ySpacing.setText('')
      self.zSpacing.setText('')

      self.startMeasurementsInPlane.enabled = False
      self.startMeasurementsOutPlane.enabled = False

    else:
      currentInputVolume = self.volumeSelector.currentNode()
      self.psfVolumeSelectorComboBox.setCurrentNode(currentInputVolume)
      self.startMeasurementsInPlane.enabled = True
      self.startMeasurementsOutPlane.enabled = True
      self.volumeID = self.volumeSelector.currentNodeID
      print "ID so vol exist", self.volumeID
      #below is image spacing information
      self.xSpacing.setText(str(round(self.infoSpinBoxes[3].value,2)))
      self.ySpacing.setText(str(round(self.infoSpinBoxes[4].value,2)))
      self.zSpacing.setText(str(round(self.infoSpinBoxes[5].value,2)))
      self.spacingLabel.setVisible(True)
      self.volumesModuleVolumeComboBox.setCurrentNode(currentInputVolume) 
      print "\n*volume changed: ", self.volumeID

      self.showInSliceViewers(currentInputVolume, ["Red", "Yellow", "Green"])
      for rulerID in self.allRulersDict.keys():
        rulerNode = self.allRulersDict[rulerID]['rulerObject'] 
        self.updatePlot(currentInputVolume, rulerNode, numPoints=100)


  def onEstimatePSFWidgetOpen(self,collapsedState):
    if collapsedState == False:
      self.radioButtonInPlane.checked = True #start with InPlane Sigma estimation

      #should not be necessary. But volume does not seem to update sometimes, so this ensures it updates
      self.psfVolumeSelectorComboBox.setCurrentNode(self.volumeSelector.currentNode())
      self.volumesModuleVolumeComboBox.setCurrentNode(self.volumeSelector.currentNode()) 
      if self.volumeSelector.currentNode() != None:
        self.spacingLabel.setVisible(True)
        self.xSpacing.setText(str(round(self.infoSpinBoxes[3].value,2)))
        self.ySpacing.setText(str(round(self.infoSpinBoxes[4].value,2)))
        self.zSpacing.setText(str(round(self.infoSpinBoxes[5].value,2)))


  def erf(self,x):
    # save the sign of x
    sign = x/abs(x)
    x = abs(x)

    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911

    # A&S formula 7.1.26
    t = 1.0/(1.0 + p*x)
    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*np.exp(-x*x)
    return sign*y # erf(-x) = -erf(x)
      
        
  def onEstimateButtonPress(self):

    if len(self.allRulersDict) == 0:
        return

    progressBar = qt.QProgressBar()
    progressBar.setMinimum(0)
    progressBar.setMaximum(0)
    progressBar.show()
    slicer.app.processEvents()

    inputVolumeNode = self.psfVolumeSelectorComboBox.currentNode()
    inputRulerListNode = self.psfHierarchySelectorComboBox.currentNode()
    corticalIntensityMin = self.corticalIntensityMinSpinBox.value
    corticalIntensityMax = self.corticalIntensityMaxSpinBox.value
    trabecularIntensityMin = self.trabecularIntensityMinSpinBox.value
    trabecularIntensityMax = self.trabecularIntensityMaxSpinBox.value
    sigmaMax = self.sigmaMaxSpinBox.value
    sigmaMin = self.sigmaMinSpinBox.value
    slackFactor = self.edgeDetectionSlackFactorSpinBox.value
    
    minCortThickness = self.corticalThicknessMinSpinBox.value
    minTrabThickness = self.trabecularThicknessMinSpinBox.value
    
    imageSpacing = inputVolumeNode.GetSpacing()
    sliceThickness = imageSpacing[0]
    pixelSpacing = imageSpacing[2]
    inPlaneFlag = self.radioButtonInPlane.checked
    inPlaneSigmaInput = self.inPlaneSigmaComboBox.value

    
    #This is where something can be done with sigma,cort_thicknesses,edgePositions,profileValuesAtEdges to plot the result 
    #with the ruler plots.  The intesities along the rulers are already plotted in the quantitative window by this module
    #For each profile/ruler a model plot needs to be created and plotted along with the intensities from the scan, so that the 
    #user has some idea of how well the model fits the local area.  If the model doesn't fit the profile will hopefully be removed and
    #the estimation rerun
    #The equation that describes the model for each profile is described in equation 6 and plotted in fig2 of Pakdel, A., J. G. Mainprize, N. Robert, J. Fialkov, and C. M. Whyne. Model-based PSF and MTF estimation and validation from skeletal clinical CT images. Med. Phys. 41:011906, 2014.
    #
    #
    
    #added a return for lists fo profile position beginnings and ends
    parameters = self.logic.run(inputVolumeNode, inputRulerListNode,corticalIntensityMin, corticalIntensityMax, trabecularIntensityMin, trabecularIntensityMax, minCortThickness, minTrabThickness, sigmaMax, sigmaMin, slackFactor, sliceThickness, pixelSpacing, inPlaneFlag, inPlaneSigmaInput)
    if parameters == -1:
        return
    else:
        sigma, cortIntensityResult_Y2, layerPositionResult_X1, layerPositionResult_X2, adjacentTissueIntensityResult_Y1, adjacentTissueIntensityResult_Y3,  profilePositionsBeginning, profilePositionsEnd, profilesIntensity = parameters

    #for each ruler, get the number of points, then apply this to make a new vector g. Then add each ruler's new vector array to the chart

    numberOfProfiles = len(cortIntensityResult_Y2)
    allRulersGaussPoints = range(numberOfProfiles)

    rulerCollection = vtk.vtkCollection()
    inputRulerListNode.GetAllChildren(rulerCollection)
    for rulerIndex in xrange(numberOfProfiles):

      X0 = -10 #profilePositionsBeginning[rulerIndex, 0]-10
      X1 = layerPositionResult_X1[rulerIndex]
      X2 = layerPositionResult_X2[rulerIndex]
      Xf = 10 #profilePositionsEnd[rulerIndex, 0]+10
      Y1 = adjacentTissueIntensityResult_Y1[rulerIndex]
      Y2 = cortIntensityResult_Y2[rulerIndex]
      Y3 = adjacentTissueIntensityResult_Y3[rulerIndex]
      currentProfileIntensities = profilesIntensity[rulerIndex]
      numPoints = len(currentProfileIntensities)
      print "numPoints: ", numPoints 
      

      #get the current ruler to determine if dn is already defined

      currentRulerNode = rulerCollection.GetItemAsObject(rulerIndex)
      if currentRulerNode.GetClassName() != 'vtkMRMLAnnotationRulerNode': 
        continue

      rulerLength = currentRulerNode.GetDistanceMeasurement()
      gaussPoints = range(numPoints)
      for i in xrange(numPoints): # values of this
        gaussPoints[i] = \
          (Y1*0.5) * (math.erf((i*rulerLength/numPoints - X0) / (sigma * math.sqrt(2))) - math.erf((i*rulerLength/numPoints - X1) / (sigma * math.sqrt(2)))) \
        + (Y3*0.5) * (math.erf((i*rulerLength/numPoints - X2) / (sigma * math.sqrt(2))) - math.erf((i*rulerLength/numPoints - Xf) / (sigma * math.sqrt(2)))) \
        + (Y2*0.5) * (math.erf((i*rulerLength/numPoints - X1) / (sigma * math.sqrt(2))) - math.erf((i*rulerLength/numPoints - X2) / (sigma * math.sqrt(2)))) 

      allRulersGaussPoints[rulerIndex] = gaussPoints

      #define piecewise graph
      xVals = np.linspace(X0,Xf, numPoints)
      firstLargerThanX1 = np.argmax(xVals>X1)
      firstLargerThanX2 = np.argmax(xVals>X2)

      piecewiseIntensityCurve = np.piecewise(xVals, [xVals <= X1, (xVals >= X1) & (xVals <= X2), xVals >= X2], [Y1, Y2, Y3])

      # Get the Chart View Node
      cvns = slicer.mrmlScene.GetNodesByClass('vtkMRMLChartViewNode')
      cvns.InitTraversal()
      cvn = cvns.GetNextItemAsObject()

      if self.allRulersDict[currentRulerNode.GetID()]['chartNodeArrayFitted'] == None:
        dn1 = slicer.mrmlScene.AddNode(slicer.vtkMRMLDoubleArrayNode())
      else:
        dn1 = self.allRulersDict[self.allRulersDict[currentRulerNode.GetID()]]['chartNodeArrayFitted']

      a1 = dn1.GetArray()
      a1.SetNumberOfTuples(numPoints)
      x = range(0, numPoints)
      for p in range(len(x)):
        a1.SetComponent(p, 0, x[p])
        a1.SetComponent(p, 1, gaussPoints[p])


      if self.allRulersDict[currentRulerNode.GetID()]['chartNodeArrayPieceWise'] == None:
        dn2 = slicer.mrmlScene.AddNode(slicer.vtkMRMLDoubleArrayNode())
      else:
        dn2 = self.allRulersDict[self.allRulersDict[currentRulerNode.GetID()]]['chartNodeArrayPieceWise']


      #second array which requires some tweaking to make piecewise

      a2 = dn2.GetArray()
      a2.SetNumberOfTuples(numPoints+2)
      x = range(0, numPoints)
      for p in range(len(x)):
        a2.SetComponent(p, 0, x[p])
        a2.SetComponent(p, 1, piecewiseIntensityCurve[p])
        if p == firstLargerThanX1-1:
          a2.SetComponent(len(x), 0, x[p] + 0.1)
          a2.SetComponent(len(x), 1, Y2)
          gh1 = x[p]
        if p == firstLargerThanX2-1:
          a2.SetComponent(len(x)+1, 0, x[p] + 0.1)
          a2.SetComponent(len(x)+1, 1, Y3)
          gh2 = x[p]



      cn = slicer.mrmlScene.GetNodeByID(self.allRulersDict[currentRulerNode.GetID()]['chartNodeID'])
      cn.AddArray("Fitted", dn1.GetID())
      cn.AddArray("Predicted", dn2.GetID())

    if inPlaneFlag:
        self.inPlaneSigmaComboBox.value = sigma
    else:
        self.outPlaneSigmaComboBox.value = sigma

    progressBar.close()

    #set to top level hierarchy in case new annotations are added outside of the module
    self.startMeasurementsInPlane.setText("Start Placing In-Plane")
    self.startMeasurementsOutPlane.setText("Start Placing Out-of-Plane")
    self.startMeasurementsOutPlane.enabled = True
    self.startMeasurementsInPlane.enabled = True
    
    interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    #placeModePersistance determines whether one or more annotations will be dropped
    placeModePersistence = 0
    interactionNode.SetPlaceModePersistence(placeModePersistence)
    # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
    # this causes annotations' creation to be haulted/"unselected"
    interactionNode.SetCurrentInteractionMode(2)
    self.startCount = 0
    self.annotationsLogic.SetActiveHierarchyNodeID(self.topLevelHierarchyID)


  def onNodeRemoved(self):

    if self.startMeasurementsOutPlane.enabled == False:
      self.annotationsLogic.SetActiveHierarchyNodeID(self.inPlaneHierarchyID)
    elif self.startMeasurementsInPlane.enabled == False:
      self.annotationsLogic.SetActiveHierarchyNodeID(self.outPlaneHierarchyID)
    return -1


  def begin(self, buttonNumber):
    selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
    print "\nbutton number is", buttonNumber
    if buttonNumber == 1:
      # self.outPlaneHierarchy.HideFromEditorsOn()
      self.annotationsLogic.SetActiveHierarchyNodeID(self.inPlaneHierarchyID)
      selectionNode.SetReferenceActivePlaceNodeID(self.inPlaneHierarchyID)
      print "\nin plane hierarchy id is", self.inPlaneHierarchyID
      print "current active hierarchy node is ", self.annotationsLogic.GetActiveHierarchyNodeID()
      # self.outPlaneHierarchy.SetName("Out-of-Plane Rulers-hidden")

    elif buttonNumber == 2:
      # self.inPlaneHierarchy.HideFromEditorsOn()
      self.annotationsLogic.SetActiveHierarchyNodeID(self.outPlaneHierarchyID)
      selectionNode.SetReferenceActivePlaceNodeID(self.outPlaneHierarchyID)
      print "\nout plane hierachy id is", self.outPlaneHierarchyID
      print "current active hierachy node is ", self.annotationsLogic.GetActiveHierarchyNodeID()
      # self.inPlaneHierarchy.SetName("In-Plane Rulers-hidden")

    # place rulers

    selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLAnnotationRulerNode")
    # self.annotationsLogic.SetActiveHierarchyNodeID(self.inPlaneHierarchyID)

    # to place ROIs use the class name vtkMRMLAnnotationROINode
    interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    placeModePersistence = 1
    interactionNode.SetPlaceModePersistence(placeModePersistence)
    # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
    interactionNode.SetCurrentInteractionMode(1)


  def stop(self, buttonNumber):
    selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
    # place rulers
    selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLAnnotationRulerNode")

    if buttonNumber == 1:
      selectionNode.SetReferenceActivePlaceNodeID(self.inPlaneHierarchyID)
      self.annotationsLogic.SetActiveHierarchyNodeID(self.inPlaneHierarchyID)
      print "unhid outplane rulers"

    elif buttonNumber == 2:
      selectionNode.SetReferenceActivePlaceNodeID(self.outPlaneHierarchyID)
      self.annotationsLogic.SetActiveHierarchyNodeID(self.outPlaneHierarchyID)

    # to place ROIs use the class name vtkMRMLAnnotationROINode
    interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
    placeModePersistence = 1
    interactionNode.SetPlaceModePersistence(placeModePersistence)
    # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
    interactionNode.SetCurrentInteractionMode(2)



  def iterationSliderValueChanged(self, newValue):
    self.iterations = newValue

  def decimalSliderValueChanged(self, newValue):
    self.psfDecimals = newValue

  def addRulersInPlane(self):
    self.buttonNumber = 1
    if self.startCount == 0:
      print "\nstarting in-plane"
      self.startMeasurementsOutPlane.enabled = False
      self.begin(self.buttonNumber)
      self.startCount = 1
      self.startMeasurementsInPlane.setText("Stop Placing In-Plane")
    elif self.startCount == 1:
      print "\nstopping in-plane"
      self.startMeasurementsOutPlane.enabled = True
      self.stop(self.buttonNumber)
      self.startCount = 0
      self.startMeasurementsInPlane.setText("Start Placing In-Plane")
      self.buttonNumber = None

  def addRulersOutPlane(self):
    self.buttonNumber = 2
    if self.startCount == 0:
      self.startMeasurementsInPlane.enabled = False
      self.begin(self.buttonNumber)
      self.startCount = 1
      self.startMeasurementsOutPlane.setText("Stop Placing Out-of-Plane")
    elif self.startCount == 1:
      self.startMeasurementsInPlane.enabled = True
      self.stop(self.buttonNumber)
      self.startCount = 0
      self.startMeasurementsOutPlane.setText("Start Placing Out-of-Plane")
      self.buttonNumber = None


  def rulerObservingFunction(self,observer,event):
    #anytime a ruler changes the function is called

    inputRuler = observer

    currentPosition1 = [0,0,0]
    currentPosition2 = [0,0,0]
    inputRuler.GetPosition1(currentPosition1)
    inputRuler.GetPosition2(currentPosition2)
    oldPosition1 = self.allRulersDict[inputRuler.GetID()]['endPositions'][0]
    oldPosition2 = self.allRulersDict[inputRuler.GetID()]['endPositions'][1]
    oldInputVolume = self.allRulersDict[inputRuler.GetID()]['inputVolume']

    inputVolume = slicer.mrmlScene.GetNodeByID(self.volumeID)
    if oldPosition1 == currentPosition1 and oldPosition2 == currentPosition2 and inputVolume == oldInputVolume:
      return

    self.allRulersDict[inputRuler.GetID()]['endPositions'] = [currentPosition1, currentPosition2]
    self.allRulersDict[inputRuler.GetID()]['inputVolume'] = inputVolume

    print "*!ruler observing function"
    self.allRulersDict[inputRuler.GetID()]['radioButton'].checked = True
    self.updatePlot(inputVolume, inputRuler, 100)
 

  
  def onRadioButtonChanged(self):
    for key in self.allRulersDict:
      radioButton = self.allRulersDict[key]['radioButton']
      if radioButton.isChecked() == True:
        self.activeRulerKey = key
        activeRuler = self.allRulersDict[key]['rulerObject']
        activeRuler.SetSelected(1)
        activeRuler.RestoreView()
        cvns = slicer.mrmlScene.GetNodesByClass('vtkMRMLChartViewNode')
        cvns.InitTraversal()
        cvn = cvns.GetNextItemAsObject()
        cvn.SetChartNodeID(self.allRulersDict[key]['chartNodeID'])
        print "radio chart id", self.allRulersDict[key]['chartNodeID']
        #Make appropriate plot show
      else:
        self.allRulersDict[key]['rulerObject'].SetSelected(0)


  def addRulerToScene(self, buttonNumber, plottedRuler, inputVolume):

    self.hideRulersPushButton.enabled = True
    self.deleteRulerPushButton.enabled = True
    print "*sceneChangeObserver: ruler node added"

    self.lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpQuantitativeView)
    #add ruler to the gui
    rulerRadioButton = qt.QRadioButton(plottedRuler.GetName())

    if buttonNumber == 1:
      
      self.inPlaneRulersLayout.addWidget(rulerRadioButton)
      self.inPlaneRulersLayout.setAlignment(qt.Qt.AlignHCenter)
    else:
      
      self.outPlaneRulersLayout.addWidget(rulerRadioButton)
      self.outPlaneRulersLayout.setAlignment(qt.Qt.AlignHCenter)

    rulerDict = {
    'rulerObject': plottedRuler,
    'radioButton': rulerRadioButton,
    'chartNodeID': None,
    'endPositions': [None,None],
    'chartNodeArrayOriginal': None,
    'chartNodeArrayFitted': None,
    'chartNodeArrayPieceWise': None,
    'inputVolume': inputVolume,
    'buttonNumber':buttonNumber
    }

    self.allRulersDict[plottedRuler.GetID()] = rulerDict

    #check newest ruler; plot the new Ruler and update; add observer to each new ruler
    plottedRuler.AddObserver('ModifiedEvent', self.rulerObservingFunction)
    chartNodeID = self.plotProfile(inputVolume, plottedRuler,100 )
    #set the chartNodeID from None
    self.allRulersDict[plottedRuler.GetID()]['chartNodeID'] = chartNodeID

    if self.allRulersDict != {}:
        for key in self.allRulersDict:
          self.allRulersDict[key]['radioButton'].checked = False

    rulerRadioButton.checked = True
    plottedRuler.SetSelected(1)
    rulerRadioButton.connect('toggled(bool)', self.onRadioButtonChanged)

  def sceneCloseObserver(self, observer, event):
    print "\nscene close"
    for key in self.allRulersDict:
      # slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['rulerObject'])
      if self.allRulersDict[key]['buttonNumber'] == 1:
        self.inPlaneRulersLayout.removeWidget(self.allRulersDict[key]['radioButton'])
        print "\nremovng ruler form in plane"
      else:
        self.outPlaneRulersLayout.removeWidget(self.allRulersDict[key]['radioButton'])
        print "\nremoving ruler form out of plane"
      self.allRulersDict[key]['radioButton'].setParent(0)

      slicer.mrmlScene.RemoveNode(slicer.mrmlScene.GetNodeByID(self.allRulersDict[key]['chartNodeID']))
      slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['chartNodeArrayOriginal'])
      slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['chartNodeArrayFitted'])
      slicer.mrmlScene.RemoveNode(self.allRulersDict[key]['chartNodeArrayPieceWise'])
      self.activeRulerKey = None
    self.allRulersDict.clear()
    print "all ruler radiobuttons deleted"
    self.addHierarchyNodes()
    self.estimatePSFWidgetCollapsibleButton.collapsed = True


  def sceneElementDeleted(self,observer,event):
    print "deleted"


  def sceneChangeObserver(self,observer,event):
    #self.ruler = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationRulerNode1')
    # self.lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpQuantitativeView)
    currentRulers = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationRulerNode')
    currentRulerNumber = currentRulers.GetNumberOfItems()

    if self.rulerNumber != currentRulerNumber:
      inputVolume = self.volumeSelector.currentNode()

      if currentRulerNumber > self.rulerNumber:
        self.rulers = currentRulers
        self.rulerNumber = currentRulerNumber

        if self.buttonNumber == None:
          return

        self.plottedRuler = currentRulers.GetItemAsObject(currentRulerNumber-1)

        if self.buttonNumber == 1:
          
          self.addRulerToScene(1, self.plottedRuler, inputVolume)
        else:
          
          self.addRulerToScene(2, self.plottedRuler, inputVolume)


      else: # self.rulerNumber > currentRulerNumber:
  
        #a ruler was deleted was it the one being plotted, if so change the plot otherwise just update
        deletedFlag = 1
        self.onNodeRemoved()
        for rulerIndex in range(0,currentRulerNumber):
          if currentRulers.GetItemAsObject(rulerIndex).GetDisplayNodeID() ==self.plottedRuler.GetDisplayNodeID():
              deletedFlag = 0
        if deletedFlag == 1 and currentRulerNumber != 0:
            self.rulers = currentRulers
            self.rulerNumber = currentRulerNumber
            self.plottedRuler = self.rulers.GetItemAsObject(self.rulerNumber-1)
            self.plottedRuler.AddObserver('ModifiedEvent', self.rulerObservingFunction)
            self.plotProfile(inputVolume, self.plottedRuler,100 )
        else:
            self.rulers = currentRulers
            self.rulerNumber = currentRulerNumber

        if self.rulerNumber < 1:
          self.deleteRulerPushButton.enabled = False
          self.hideRulersPushButton.enabled = False


  def updatePlot(self, inputVolume, inputRuler, numPoints=100):
    print "updateplot"
    if not (inputVolume and inputRuler):
      print "no plot needed"
      return


    #Define the data for vtk filter
    vtkdata = inputVolume.GetImageData()

    #Set the points between which the line is constructed.
    p1=[0.0,-0.1,0.0]
    p2=[0.0,-0.1,1.0]

    inputRuler.GetPosition1(p1)
    inputRuler.GetPosition2(p2)

    p1.append(1)
    p2.append(1)

    #transforming p1,p2 to the ijk space
    rasToijkMatrix = vtk.vtkMatrix4x4() #= np.arange(16).reshape(4,4)
    inputVolume.GetRASToIJKMatrix(rasToijkMatrix)
    p1_ijk = rasToijkMatrix.MultiplyPoint(p1)
    p2_ijk = rasToijkMatrix.MultiplyPoint(p2)

    p1_ijk = p1_ijk[0:3]
    p2_ijk = p2_ijk[0:3]

    #Define the number of interpolation points

    line=createLine(p1_ijk,p2_ijk,numPoints) # Create the line
    points,U,vtkArray =  probeOverLine(line,vtkdata) # interpolate the data over the line

    dn = self.allRulersDict[inputRuler.GetID()]['chartNodeArrayOriginal']

    a = dn.GetArray()
    a.SetNumberOfTuples(numPoints)
    x = range(0, numPoints)
    for i in range(len(x)):
        a.SetComponent(i, 0, x[i])
        a.SetComponent(i, 1, U[i])
    a.Modified()
    dn.Modified()
    cn = slicer.mrmlScene.GetNodeByID(self.allRulersDict[inputRuler.GetID()]['chartNodeID'])
    cn.Modified()


  def showInSliceViewers(self, volumeNode, sliceWidgetNames):
    # Displays volumeNode in the selected slice viewers as background volume
    # Existing background volume is pushed to foreground, existing foreground volume will not be shown anymore
    # sliceWidgetNames is a list of slice view names, such as ["Yellow", "Green"]
    if not volumeNode:
      return
    newVolumeNodeID = volumeNode.GetID()
    for sliceWidgetName in sliceWidgetNames:
      sliceLogic = slicer.app.layoutManager().sliceWidget(sliceWidgetName).sliceLogic()
      foregroundVolumeNodeID = sliceLogic.GetSliceCompositeNode().GetForegroundVolumeID()
      backgroundVolumeNodeID = sliceLogic.GetSliceCompositeNode().GetBackgroundVolumeID()
      if foregroundVolumeNodeID == newVolumeNodeID and backgroundVolumeNodeID == newVolumeNodeID:
        return
      else:
        sliceLogic.GetSliceCompositeNode().SetBackgroundVolumeID(newVolumeNodeID)
        sliceLogic.GetSliceCompositeNode().SetForegroundVolumeID(newVolumeNodeID)

  def plotProfile( self, inputVolume, inputRuler, numPoints=100 ):


    if not (inputVolume and inputRuler):
      print "no plot needed"
      return


    #Define the data for vtk filter
    vtkdata = inputVolume.GetImageData()

    #Set the points between which the line is constructed.
    p1=[0.0,-0.1,0.0]
    p2=[0.0,-0.1,1.0]

    inputRuler.GetPosition1(p1)
    inputRuler.GetPosition2(p2)

    p1.append(1)
    p2.append(1)

    #transforming p1,p2 to the ijk space
    rasToijkMatrix = vtk.vtkMatrix4x4() #= np.arange(16).reshape(4,4)
    inputVolume.GetRASToIJKMatrix(rasToijkMatrix)
    p1_ijk = rasToijkMatrix.MultiplyPoint(p1)
    p2_ijk = rasToijkMatrix.MultiplyPoint(p2)

    p1_ijk = p1_ijk[0:3]
    p2_ijk = p2_ijk[0:3]

    #Define the number of interpolation points

    line=createLine(p1_ijk,p2_ijk,numPoints) # Create the line
    #print line
    points,U,vtkArray =  probeOverLine(line,vtkdata) # interpolate the data over the line

    #Plotting

    #new plotting method using charting within Slicer
    # Switch to a layout (24) that contains a Chart View to initiate the construction of the widget and Chart View Node
    lns = slicer.mrmlScene.GetNodesByClass('vtkMRMLLayoutNode')
    lns.InitTraversal()
    ln = lns.GetNextItemAsObject()

    # Get the Chart View Node
    cvns = slicer.mrmlScene.GetNodesByClass('vtkMRMLChartViewNode')
    cvns.InitTraversal()
    cvn = cvns.GetNextItemAsObject()

    if self.allRulersDict[inputRuler.GetID()]['chartNodeArrayOriginal'] == None:
      dn = slicer.mrmlScene.AddNode(slicer.vtkMRMLDoubleArrayNode())
      self.allRulersDict[inputRuler.GetID()]['chartNodeArrayOriginal'] = dn
    else:
      dn = self.allRulersDict[inputRuler.GetID()]['chartNodeArrayOriginal']

    a = dn.GetArray()
    a.SetNumberOfTuples(numPoints)
    x = range(0, numPoints)
    for i in range(len(x)):
        a.SetComponent(i, 0, x[i])
        a.SetComponent(i, 1, U[i])
        # a.SetComponent(i, 2, 0)
    a.Modified()
    # Create a Chart Node.

    if self.allRulersDict[inputRuler.GetID()]['chartNodeID'] != None: 
      cn = slicer.mrmlScene.GetNodeByID(self.allRulersDict[inputRuler.GetID()]['chartNodeID'])
    else:
      cn = slicer.mrmlScene.AddNode(slicer.vtkMRMLChartNode())
      cn.AddArray('Actual', dn.GetID())
      cn.SetProperty('default', 'title', 'Intensity Profile of ' + inputRuler.GetName())
      cn.SetProperty('default', 'xAxisLabel', '% along ruler')
      cn.SetProperty('default', 'yAxisLabel', 'Image Intensity(HU)')

    # Set a few properties on the Chart. The first argument is a string identifying which Array to assign the property.
    # 'default' is used to assign a property to the Chart itself (as opposed to an Array Node).


    # Tell the Chart View which Chart to display
    cvn.SetChartNodeID(cn.GetID())
    print "chart node updated"
    return cn.GetID()


  # Helper Functions from Newfarmer on Stack Overflow: http://stackoverflow.com/questions/21630987/probing-sampling-interpolating-vtk-data-using-python-tvtk-or-mayavi
def createLine(p1,p2,numPoints):
  # Create the line along which you want to sample
  line = vtk.vtkLineSource()
  line.SetResolution(numPoints)
  line.SetPoint1(p1)
  line.SetPoint2(p2)
  line.Update()
  return line

def probeOverLine(line,vtkdata):
  #Interpolate the data from the VTK-file on the created line.
  data = vtkdata
  # vtkProbeFilter, the probe line is the input, and the underlying dataset is the source.
  probe = vtk.vtkProbeFilter()
  probe.SetInputConnection(line.GetOutputPort())
  if hasattr(probe, 'SetSourceData'):
      probe.SetSourceData(data)
  else:
      probe.SetSource(data)
  probe.Update()
  #get the data from the VTK-object (probe) to an numpy array
  t = probe.GetOutput().GetPointData().GetArray(0)
  q=VN.vtk_to_numpy(probe.GetOutput().GetPointData().GetArray(0))
  numPoints = probe.GetOutput().GetNumberOfPoints() # get the number of points on the line
  #intialise the points on the line
  x = np.zeros(numPoints)
  y = np.zeros(numPoints)
  z = np.zeros(numPoints)
  points = np.zeros((numPoints , 3))
  #get the coordinates of the points on the line
  for i in range(numPoints):
      x[i],y[i],z[i] = probe.GetOutput().GetPoint(i)
      points[i,0]=x[i]
      points[i,1]=y[i]
      points[i,2]=z[i]
  vtkArray = t
  return points,q,vtkArray


def setZeroToNaN(array):
    # In case zero-values in the data, these are set to NaN.
    array[array==0]=np.nan
    return array


#
# SurfaceRegistrationLogic
#

class EstimatePSFLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget
  """


  def run(self, inputVolumeNode, inputRulerListNode,corticalIntensityMin, corticalIntensityMax, trabecularIntensityMin, trabecularIntensityMax, minCortThickness,minTrabThickness,sigmaMax, sigmaMin, slackFactor, sliceThickness, pixelSpacing, inPlaneFlag, inPlaneSigmaInput):

    numPoint = 100
    
    rulerCollection = vtk.vtkCollection()
    inputRulerListNode.GetAllChildren(rulerCollection)
    numberOfPotentialRulers = rulerCollection.GetNumberOfItems()
    print "numberOfPotentialRulers: ", numberOfPotentialRulers

    
    if(inputVolumeNode ==0 or inputRulerListNode ==0):
        sys.stderr.write("Failed to look up input volume and/or Ruler List Hierarchy Node!\n")
        return -1
    
    if(numberOfPotentialRulers==0):
        sys.stderr.write("No Rulers in List!\n")
        return -1
    

    inputVolumeImage = inputVolumeNode.GetImageData()    
    numberOfProfiles  = numberOfPotentialRulers  
    
    #note that in the C++ version of this code I was checking that the all the
    #input rulers were in fact rulers.  However python doesn't allow easy checking
    #of types so I will omit this check for now
     
    edges = np.zeros((2,numberOfProfiles))
       
    profilePositionsBeginning =  np.zeros((numberOfProfiles,3))
    profilePositionsEnd =  np.zeros((numberOfProfiles,3))
    
    
    
    profilesX = np.zeros((numberOfProfiles,numPoint))
    profilesIntensity = np.zeros((numberOfProfiles,numPoint))

    currentProfileIndex = 0

    for rulerIndex in range(numberOfPotentialRulers):
        print "rulerIndex", rulerIndex
        currentRulerNode = rulerCollection.GetItemAsObject(rulerIndex) #inputRulerListNode.GetNthChildNode(rulerIndex).GetAssociatedNode()
        print "currentRulerName", currentRulerNode.GetName()

        # if currentRulerNode.GetClassName() != 'vtkMRMLAnnotationRulerNode':
        #     print "returning"
        #     return

        if currentRulerNode!=0 and currentRulerNode.GetClassName() == 'vtkMRMLAnnotationRulerNode':
            linePosition1= np.zeros(3)
            currentRulerNode.GetPosition1(linePosition1)
            linePosition2= np.zeros(3)
            currentRulerNode.GetPosition2(linePosition2)

           #transforming p1,p2 to the ijk space
            rasToijkMatrix = vtk.vtkMatrix4x4()
            inputVolumeNode.GetRASToIJKMatrix(rasToijkMatrix)
            
            fltLinePostion1 = np.array((linePosition1[0],linePosition1[1],linePosition1[2],1.0))
            fltLinePostion2 = np.array((linePosition2[0],linePosition2[1],linePosition2[2],1.0))
            linePosition1_ijk = np.array((0.0,0.0,0.0,1.0))
            linePosition2_ijk = np.array((0.0,0.0,0.0,1.0))
            
            rasToijkMatrix.MultiplyPoint(fltLinePostion1,linePosition1_ijk);
            rasToijkMatrix.MultiplyPoint(fltLinePostion2,linePosition2_ijk);
            
            currentLine = createLine(linePosition1_ijk[0:-1],linePosition2_ijk[0:-1],numPoint);
            a,b,imageIntensityAlongLine = probeOverLine(currentLine,inputVolumeImage);
            lineLength = pow(pow(linePosition1[0]-linePosition2[0],2)+pow(linePosition1[1]-linePosition2[1],2)+pow(linePosition1[2]-linePosition2[2],2),(0.5))
            lineLengthIncrement = lineLength/(numPoint-1);
            for pointIndex in range(numPoint):
                profilesX[currentProfileIndex,pointIndex] = lineLengthIncrement*pointIndex;
                currentIntensityValue=[0.0]
                imageIntensityAlongLine.GetTuple(pointIndex,currentIntensityValue)
                profilesIntensity[currentProfileIndex,pointIndex] = currentIntensityValue[0];
                
            edges[0,currentProfileIndex] = 0;
            edges[1,currentProfileIndex] = lineLength;
                
            profilePositionsBeginning[currentProfileIndex,0] = linePosition1[0];
            profilePositionsBeginning[currentProfileIndex,1] = linePosition1[1];
            profilePositionsBeginning[currentProfileIndex,2] = linePosition1[2];
            profilePositionsEnd[currentProfileIndex,0] = linePosition2[0];
            profilePositionsEnd[currentProfileIndex,1] = linePosition2[1];
            profilePositionsEnd[currentProfileIndex,2] = linePosition2[2];

            currentProfileIndex = currentProfileIndex + 1;
        
    
    
    
    sigma, cortIntensityResult_Y2, layerPositionResult_X1, layerPositionResult_X2, adjacentTissueIntensityResult_Y1, adjacentTissueIntensityResult_Y3=estimateGaussianPSF_(inPlaneFlag,profilesX,profilesIntensity,edges,corticalIntensityMin,corticalIntensityMax,trabecularIntensityMin,trabecularIntensityMax,minCortThickness,minTrabThickness,sigmaMax,sigmaMin,slackFactor,sliceThickness,pixelSpacing,profilePositionsBeginning,profilePositionsEnd,inPlaneSigmaInput,nargout=4)

    return sigma, cortIntensityResult_Y2, layerPositionResult_X1, layerPositionResult_X2, adjacentTissueIntensityResult_Y1, adjacentTissueIntensityResult_Y3, profilePositionsBeginning, profilePositionsEnd, profilesIntensity



