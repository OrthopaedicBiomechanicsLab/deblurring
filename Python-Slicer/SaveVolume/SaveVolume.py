import os
import unittest
from __main__ import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *

import SimpleITK as sitk
import sitkUtils as su
import math
import PythonQt

from vtk.util import numpy_support as VN
import numpy as np
import datetime
import DICOM
import DICOMLib.DICOMProcesses
import DICOMLib.DICOMWidgets

#
# SaveVolume
#
class SaveVolume(ScriptedLoadableModule):
  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SaveVolume" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
    This is an example of scripted loadable module bundled in an extension.
    """
    self.parent.acknowledgementText = """
    This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
    and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# SaveVolumeWidget
#

class SaveVolumeWidget(ScriptedLoadableModuleWidget):

  def setup(self, tabLayout=None, metaDataVolume = None, deblurredVolume = None, dicomTagsDict = None):

    if tabLayout == None:
      self.developerMode = True
      tab = qt.QTabWidget()
      tabLayout = qt.QFormLayout(tab)
      self.layout.addWidget(tab)


    # Instantiate and connect widgets ...
    ScriptedLoadableModuleWidget.setup(self)
    gridLayout = qt.QGridLayout()      
    gridLayout.setHorizontalSpacing(0)


    self.logic = SaveVolumeLogic()
    self.directory = None
    self.directoryChosen = False
    self.noFilenameMsgBox = qt.QMessageBox()
    self.noVolumeMsgBox = qt.QMessageBox()
    self.readyLabel = qt.QLabel()
    row = 0
    col = 0

    newVolumeSelectorLayout = qt.QGridLayout()
    self.newVolumeSelector = slicer.qMRMLNodeComboBox()
    self.newVolumeSelector.nodeTypes = (("vtkMRMLScalarVolumeNode"), "")
    self.newVolumeSelector.addEnabled = False
    self.newVolumeSelector.removeEnabled = False
    self.newVolumeSelector.noneEnabled = True
    self.newVolumeSelector.showHidden = False
    self.newVolumeSelector.renameEnabled = False
    self.newVolumeSelector.selectNodeUponCreation = False
    self.newVolumeSelector.showChildNodeTypes = False
    self.newVolumeSelector.setMRMLScene(slicer.mrmlScene)
    self.newVolumeSelectorLabel = qt.QLabel("Volume to export as DICOM: ")
    self.newVolumeSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)
    self.newVolumeSelectorLabel.setStyleSheet('padding-left: 5px; padding-top: 23px')
    self.newVolumeSelector.setStyleSheet('padding-right: 5px; padding-left:5px; margin-top: 23px; margin-right: 5px;')
    gridLayout.addWidget(self.newVolumeSelectorLabel,row,col,1,1)
    gridLayout.addWidget(self.newVolumeSelector,row,col+1,1,1)
    if deblurredVolume != None:
        self.newVolumeSelector.setCurrentNode(deblurredVolume)
    if dicomTagsDict == None:
      self.dicomTagsDict = {}
    else:
      self.dicomTagsDict = dicomTagsDict
    # added one row and one col
    row += 1


    #add spacing between the two
    # rowPad2 = qt.QLabel('')
    # gridLayout.addWidget(rowPad2, row, col, 1,1)
    # row +=1



    volumeMetadataSelectorLayout = qt.QGridLayout()
    self.volumeMetadataSelector = slicer.qMRMLNodeComboBox()
    self.volumeMetadataSelector.nodeTypes = (("vtkMRMLScalarVolumeNode"), "")
    self.volumeMetadataSelector.addEnabled = False
    self.volumeMetadataSelector.removeEnabled = False
    self.volumeMetadataSelector.noneEnabled = True
    self.volumeMetadataSelector.showHidden = False
    self.volumeMetadataSelector.renameEnabled = False
    self.volumeMetadataSelector.selectNodeUponCreation = False
    self.volumeMetadataSelector.showChildNodeTypes = False
    self.volumeMetadataSelector.setMRMLScene(slicer.mrmlScene)
    # tabLayout.addRow("Select input DICOM volume to pull metadata:", self.volumeMetadataSelector)
    self.volumeMetadataSelectorLabel = qt.QLabel("Volume used to auto-fill DICOM metadata: ")
    self.volumeMetadataSelectorLabel.setMinimumWidth(300)
    self.optionalLabel = qt.QLabel("(Optional)")
    self.optionalLabel.setStyleSheet("font-style: italic; padding-left: 5px; padding-top: 20px;")
    self.volumeMetadataSelectorLabel.setStyleSheet('padding-left: 5px;')
    self.volumeMetadataSelector.setStyleSheet('padding-right: 5px; padding-left:5px; margin-right: 5px;')
    gridLayout.addWidget(self.optionalLabel,row,col,1,1)
    gridLayout.addWidget(self.volumeMetadataSelectorLabel,row+1,col,1,1)
    gridLayout.addWidget(self.volumeMetadataSelector,row+1,col+1,1,1)
    if metaDataVolume != None:
        self.volumeMetadataSelector.setCurrentNode(metaDataVolume)
    row +=2

    # rowPad3 = qt.QLabel('\n')
    # gridLayout.addWidget(rowPad3,row,col,1,1)
    # row+=1

    self.directoryLabel = qt.QLabel("Export directory:")
    self.directoryButton = qt.QPushButton("Select a Local Directory")
    self.directoryButton.enabled = True
    self.directoryButton.connect('clicked(bool)', self.onDirectoryButton)

    self.directoryLabel.setStyleSheet('padding-left: 5px; padding-top: 33px;')
    self.directoryButton.setStyleSheet('padding-right: 5px; padding-left:10px; margin-right: 5px; maring-left:5px; margin-top: 34px; height: 16px;')

    gridLayout.addWidget(self.directoryLabel, row, col, 1,1)
    gridLayout.addWidget(self.directoryButton, row, col+1, 1,1)
    row+=1

    self.currentDirectoryLabel = qt.QLabel("Current Directory: None")
    self.currentDirectoryLabel.setStyleSheet('padding-left: 5px; padding-top: 8px; color: Darkgray')
    gridLayout.addWidget(self.currentDirectoryLabel, row, col, 1,2)
    row+=1

    rowPad4 = qt.QLabel("\n\n")
    gridLayout.addWidget(rowPad4,row,col,2,2)
    row+=2



    self.applyButton = qt.QPushButton("Export Volume as DICOM")
    self.applyButton.enabled = False
    tabLayout.addRow(self.applyButton)
    self.applyButton.connect('clicked(bool)', self.onApplyButton)
    self.applyButton.setStyleSheet('margin-left: 5px; margin-right: 5px; height:17px')
    gridLayout.addWidget(self.applyButton,row,col,1,2)
    tabLayout.addRow(gridLayout)
    row+=1
    self.readyLabel.visible = False
    tabLayout.addRow(self.readyLabel)


  def onSelect(self):
    self.applyButton.enabled = self.newVolumeSelector.currentNode() and self.directoryChosen
    self.readyLabel.visible = self.applyButton.enabled
    self.readyLabel.setText("Ready to export")
    self.readyLabel.setStyleSheet('padding-left: 5px; padding-top: 2px;')


  def onDirectoryButton(self):
    dicomFilesDirectory = qt.QFileDialog.getExistingDirectory(None, 'Select DCMTK database folder')
    
    # configFilePath = dicomFilesDirectory + '/dcmrscp.cfg'
    if dicomFilesDirectory != "":
      self.directoryChosen = True
      self.directory = dicomFilesDirectory
      self.currentDirectoryLabel.setText("Current Directory: " + str(self.directory))
      self.currentDirectoryLabel.setStyleSheet('padding-left: 5px; padding-top: 8px; color: Navy')
      self.onSelect()


  def onApplyButton(self):
    metadataVolume = self.volumeMetadataSelector.currentNode()
    # tags = self.logic.grabMetaDataFromVolume(self.volumeMetadataSelector.currentNode())
    if self.volumeMetadataSelector.currentNode() == None:
      self.noVolumeMsgBox.setText("No DICOM volume was selected. Therefore, no metadata will be set. Continue with export?")
      self.noVolumeMsgBox.setStandardButtons(qt.QMessageBox.Yes | qt.QMessageBox.No)
      value = self.noVolumeMsgBox.exec_()
      if value != qt.QMessageBox.Yes:
        return
      else:
        tags = {}


    elif metadataVolume.GetID() not in self.dicomTagsDict.keys():
        self.noFilenameMsgBox.setText("The DICOM volume you have chosen is not loaded in the DICOM database. Therefore, no metadata will be set. Continue with export?")
        self.noFilenameMsgBox.setStandardButtons(qt.QMessageBox.Yes | qt.QMessageBox.No)
        value = self.noFilenameMsgBox.exec_()
        if value != qt.QMessageBox.Yes:
          return
        else:
          tags = {}

    else:
      tags = self.dicomTagsDict[metadataVolume.GetID()]

    self.logic.export(self.newVolumeSelector.currentNode(), tags, self.directory)
    self.applyButton.enabled = False
    self.readyLabel.visible = True
    self.readyLabel.setText("Exported volume to " + str(self.directory))
    self.readyLabel.setStyleSheet('padding-left: 5px; padding-top: 2px; color: green; font-style: italic')
    # self.newVolumeSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)
    # self.volumeMetadataSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)


  def promptForDatabaseDirectory(self):
    """Ask the user to pick a database directory.
    But, if the application is in testing mode, just pick
    a temp directory
    """
    commandOptions = slicer.app.commandOptions()
    settings = qt.QSettings()
    databaseDirectory = settings.value('DatabaseDirectory')

    documentsLocation = qt.QDesktopServices.DocumentsLocation
    documents = qt.QDesktopServices.storageLocation(documentsLocation)
    databaseDirectory = documents + "/SlicerDICOMDatabase"
    message = "DICOM Database will be stored in\n\n"
    message += databaseDirectory
    message += "\n\nUse the Local Database button in the DICOM Browser "
    message += "to pick a different location."
    qt.QMessageBox.information(slicer.util.mainWindow(),
                    'DICOM', message, qt.QMessageBox.Ok)
    if not os.path.exists(databaseDirectory):
      os.mkdir(databaseDirectory)
    self.onDatabaseDirectoryChanged(databaseDirectory)

# SaveVolumeLogic
#

class SaveVolumeLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget
  """

  def grabMetaDataFromVolume(self, volume):
    tags = {}

    if volume == None:
      return tags

    try:
      instUids = volume.GetAttribute('DICOM.instanceUIDs').split()
      filename = slicer.dicomDatabase.fileForInstance(instUids[0])
      tags["patientName"] = slicer.dicomDatabase.fileValue(filename,'0010,0010')
    except:
      return tags

    tags["patientName"] = slicer.dicomDatabase.fileValue(filename,'0010,0010')
    tags["patientID"] = slicer.dicomDatabase.fileValue(filename,'0010,0020') + '_deconv'
    tags["patientComments"] = slicer.dicomDatabase.fileValue(filename,'0010,4000')
    tags["patientBirthDate"] = slicer.dicomDatabase.fileValue(filename,'0010,0030')
    tags["patientSex"] = slicer.dicomDatabase.fileValue(filename,'0010,0040')
    tags["studyID"] = slicer.dicomDatabase.fileValue(filename,'0020,0010')
    tags["studyDate"] = slicer.dicomDatabase.fileValue(filename,'0008,0020')
    tags["studyTime"] = slicer.dicomDatabase.fileValue(filename,'0008,0030')
    tags["studyDescription"] = slicer.dicomDatabase.fileValue(filename,'0008,1030')    
    tags["seriesNumber"] = slicer.dicomDatabase.fileValue(filename,'0020,0011')
    tags["modality"] = slicer.dicomDatabase.fileValue(filename,'0008,0060')
    tags["model"] = slicer.dicomDatabase.fileValue(filename,'0008,1090')
    tags["manufacturer"] = slicer.dicomDatabase.fileValue(filename,'0008,0070') 
    x = slicer.dicomDatabase.fileValue(filename,'0008,0021')

    return tags

  def export(self, newVolume, tags, directory):

    dObj = datetime.date.today()
    year = dObj.year
    month = dObj.month
    day = dObj.day

    if month < 10:
      month = '0' + str(month)

    if day < 10:
      day = '0' + str(day)

    #Cannot get current date to show as a tag. 
    #It is put in unicode as needed.
    date = (str(year) + str(month) + str(day))

    cliparameters = tags
    cliparameters['inputVolume'] = newVolume.GetID()
    cliparameters['dicomDirectory'] = directory
    cliparameters["seriesDate"] = unicode(date)
    cliparameters["seriesDescription"] = u'Sharpened'

  
    # use a GUI to provide progress feedback
    
    if not hasattr(slicer.modules, 'createdicomseries'):
      return False
    dicomWrite = slicer.modules.createdicomseries
    cliNode = slicer.cli.run(dicomWrite, None, cliparameters, wait_for_completion=False)
    return cliNode != None

